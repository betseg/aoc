tris = []
with open("inputs/day03.txt") as file:
    for line in file:
        line = line.split()
        tris.append([int(line[0]),int(line[1]),int(line[2])])

cnt = 0
for tri in tris:
    if tri[0]+tri[1]>tri[2] and tri[1]+tri[2]>tri[0] and tri[0]+tri[2]>tri[1]:
        cnt += 1

print(cnt)

cnt = 0
for i in range(int(len(tris)/3)):
    if tris[i*3+0][0]+tris[i*3+1][0]>tris[i*3+2][0] and tris[i*3+1][0]+tris[i*3+2][0]>tris[i*3+0][0] and tris[i*3+0][0]+tris[i*3+2][0]>tris[i*3+1][0]:
        cnt += 1
    if tris[i*3+0][1]+tris[i*3+1][1]>tris[i*3+2][1] and tris[i*3+1][1]+tris[i*3+2][1]>tris[i*3+0][1] and tris[i*3+0][1]+tris[i*3+2][1]>tris[i*3+1][1]:
        cnt += 1
    if tris[i*3+0][2]+tris[i*3+1][2]>tris[i*3+2][2] and tris[i*3+1][2]+tris[i*3+2][2]>tris[i*3+0][2] and tris[i*3+0][2]+tris[i*3+2][2]>tris[i*3+1][2]:
        cnt += 1

print(cnt)