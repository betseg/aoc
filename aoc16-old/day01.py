import math

dirs = []
with open("inputs/day01.txt") as file:
    dirs = file.read().split(', ')

x,y = 0,0
ang = math.pi / 2

visited = set()
first = None

for instr in dirs:
    match instr[0]:
        case 'L':
            ang += math.pi/2
        case 'R':
            ang -= math.pi/2
    val = int(instr[1:])
    for _ in range(val):
        x += math.cos(ang)
        y += math.sin(ang)
        if first is None:
            if (x,y) in visited:
                first = x,y
            visited.add((x,y))

print(int(round(abs(x) + abs(y))), int(round(abs(first[0])+abs(first[1]))))