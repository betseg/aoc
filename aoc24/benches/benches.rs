#![feature(str_from_raw_parts)]

use aoc24::utils::DayImpl;
use aoc24::*;

use criterion::{BatchSize, Criterion, criterion_group, criterion_main};

macro_rules! bench_part {
    ( $c:expr, $input:expr, $day:ident, $part:ident ) => {
        $c.bench_function(
            &format!("{} - {}", stringify!($day), stringify!($part)),
            |b| {
                b.iter_batched(
                    || $input.clone(),
                    |i| <$day::Day as DayImpl>::$part(i),
                    BatchSize::SmallInput,
                );
            },
        );
    };
}

macro_rules! create_benches {
    ( $( $day:ident ),* ) => {
        $(
            fn $day(c: &mut Criterion) {
                let (input_str, cap) = <$day::Day as DayImpl>::get_input();
                let len = input_str.len();
                let constructed = unsafe { std::str::from_raw_parts(input_str.as_ptr(), len) };

                c.bench_function(&format!("{} - parse", &stringify!($day::Day)[..5]), |b| {
                    b.iter_batched(
                        || constructed.clone(),
                        |i| <$day::Day as DayImpl>::parse(i),
                        BatchSize::SmallInput,
                    );
                });

                let input = <$day::Day as DayImpl>::parse(constructed);

                bench_part!(c, input, $day, solve_part1);
                bench_part!(c, input, $day, solve_part2);

                drop(unsafe { String::from_raw_parts(input_str.as_mut_ptr(), len, cap) } );
            }
        )*

        criterion_group!(benches, $( $day, )* );
    };
}

create_benches!(
    day01, day02, day03, day04, day05, day06, day07, day08, day09, day10, day11, day12, day13,
    day14, day15, day16, day17, day18, day19, day20, day21, day22, day23, day24, day25
);

criterion_main!(benches);
