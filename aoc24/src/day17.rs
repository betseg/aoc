use itertools::Itertools;

use crate::utils::*;

pub struct Day;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Instruction {
    Adv,
    Bxl,
    Bst,
    Jnz,
    Bxc,
    Out,
    Bdv,
    Cdv,
}

#[derive(Debug, Clone, Default)]
pub struct Computer {
    a: usize,
    b: usize,
    c: usize,
    ip: usize,
}

impl DayImpl for Day {
    type Input = (Computer, Vec<Instruction>);
    const DAY: u8 = 17;

    fn parse(input: &str) -> Self::Input {
        let mut a = 0;
        let mut prog = vec![];
        for (i, line) in input.lines().enumerate() {
            if i == 0 {
                a = line[12..].parse().unwrap();
            } else if i == 4 {
                prog = line[9..]
                    .chars()
                    .step_by(2)
                    .map(|c| c.to_digit(10).unwrap().into())
                    .collect();
            }
        }
        (
            Computer {
                a,
                ..Default::default()
            },
            prog,
        )
    }

    fn solve_part1((computer, prog): Self::Input) -> Option<PartResult> {
        let mut computer = computer.to_owned();
        let mut res = vec![];
        while computer.a != 0 {
            res.push(computer.step(&prog));
        }
        Some(res.iter().join(",").into())
    }

    fn solve_part2((_, prog): Self::Input) -> Option<PartResult> {
        Some(
            prog.iter()
                .map(|&i| i as usize)
                .rev()
                .fold(vec![0], |poss, instr| {
                    poss.iter()
                        .flat_map(|a| {
                            (a * 8..a * 8 + 8).filter(|a| {
                                Computer {
                                    a: *a,
                                    ..Default::default()
                                }
                                .step(&prog)
                                    == instr
                            })
                        })
                        .collect()
                })[0]
                .into(),
        )
    }
}

impl Computer {
    fn step(&mut self, prog: &[Instruction]) -> usize {
        while self.ip < prog.len() {
            match prog[self.ip] {
                Instruction::Adv => self.a >>= self.combo(prog),
                Instruction::Bxl => self.b ^= self.operand(prog),
                Instruction::Bst => self.b = self.combo(prog) % 8,
                Instruction::Jnz => {
                    if self.a != 0 {
                        self.ip = self.operand(prog).wrapping_add_signed(-2);
                    }
                }
                Instruction::Bxc => self.b ^= self.c,
                Instruction::Out => {
                    let res = self.combo(prog) % 8;
                    self.ip += 2;
                    return res;
                }
                Instruction::Bdv => self.b = self.a >> self.combo(prog),
                Instruction::Cdv => self.c = self.a >> self.combo(prog),
            }
            self.ip = self.ip.wrapping_add(2);
        }
        unreachable!()
    }

    fn operand(&self, prog: &[Instruction]) -> usize {
        prog[self.ip + 1] as usize
    }

    fn combo(&self, prog: &[Instruction]) -> usize {
        match prog[self.ip + 1] as usize {
            val @ 0..=3 => val,
            4 => self.a,
            5 => self.b,
            6 => self.c,
            _ => unreachable!(),
        }
    }
}

impl From<u32> for Instruction {
    fn from(value: u32) -> Self {
        match value {
            0 => Self::Adv,
            1 => Self::Bxl,
            2 => Self::Bst,
            3 => Self::Jnz,
            4 => Self::Bxc,
            5 => Self::Out,
            6 => Self::Bdv,
            7 => Self::Cdv,
            _ => unreachable!(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_my_input() {
        assert_eq!(
            Day.run(None),
            (
                Some(PartResult::String(String::from("2,0,1,3,4,0,2,1,7"))),
                Some(PartResult::Int(236580836040301))
            )
        );
    }
}
