use std::collections::HashMap;

use rayon::prelude::*;

use crate::utils::*;

pub struct Day;

impl DayImpl for Day {
    type Input = (HashMap<(usize, usize), usize>, usize);
    const DAY: u8 = 20;

    fn parse(input: &str) -> Self::Input {
        let width = input.find('\n').unwrap() + 1;
        let start = input.find('S').unwrap();
        let mut cur = (start / width, start % width);
        let mut map = HashMap::new();
        let mut dist = 0;
        map.insert(cur, dist);
        while input.as_bytes()[cur.0 * width + cur.1] != b'E' {
            for d in [(-1, 0), (0, -1), (0, 1), (1, 0)] {
                let dcur = (
                    cur.0.wrapping_add_signed(d.0),
                    cur.1.wrapping_add_signed(d.1),
                );
                if !map.contains_key(&dcur)
                    && [b'.', b'E'].contains(&input.as_bytes()[dcur.0 * width + dcur.1])
                {
                    cur = dcur;
                    dist += 1;
                    map.insert(cur, dist);
                    break;
                }
            }
        }
        (map, dist)
    }

    fn solve_part1((map, max): Self::Input) -> Option<PartResult> {
        calculate((&map, &max), 2)
    }

    fn solve_part2((map, max): Self::Input) -> Option<PartResult> {
        calculate((&map, &max), 20)
    }
}

fn calculate(
    (map, max): (&HashMap<(usize, usize), usize>, &usize),
    size: isize,
) -> Option<PartResult> {
    Some(
        map.par_iter()
            .map(|cell| {
                (-size..=size)
                    .par_bridge()
                    .map(|dy| {
                        (-size..=size)
                            .par_bridge()
                            .map(|dx| {
                                let mut res = 0;
                                let cheat = dy.abs() + dx.abs();
                                if cheat <= size {
                                    let dcur = (
                                        cell.0.0.wrapping_add_signed(dy),
                                        cell.0.1.wrapping_add_signed(dx),
                                    );
                                    if let Some(new) = map.get(&dcur) {
                                        let new_dist = max.wrapping_sub(new.wrapping_sub(*cell.1))
                                            + cheat as usize;
                                        if new_dist <= max - 100 {
                                            res += 1;
                                        }
                                    }
                                }
                                res
                            })
                            .sum::<usize>()
                    })
                    .sum::<usize>()
            })
            .sum::<usize>()
            .into(),
    )
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_my_input() {
        assert_eq!(
            Day.run(None),
            (Some(PartResult::Int(1375)), Some(PartResult::Int(983054)))
        );
    }
}
