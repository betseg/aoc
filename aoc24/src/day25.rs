use itertools::Itertools;

use crate::utils::*;

pub struct Day;

impl DayImpl for Day {
    type Input = (Vec<[u8; 5]>, Vec<[u8; 5]>);
    const DAY: u8 = 25;

    fn parse(input: &str) -> Self::Input {
        let mut keys = vec![];
        let mut locks = vec![];
        for c in input.split("\n\n") {
            let comp_type = c.as_bytes()[0];
            let mut comp = [0; 5];
            (0..5).for_each(|i| {
                let mut val = 0;
                for j in 0..7 {
                    if c.as_bytes()[i + j * 6] == comp_type {
                        val += 1;
                    }
                }
                comp[i] = if comp_type == b'#' { val - 1 } else { 6 - val };
            });
            if comp_type == b'#' {
                locks.push(comp);
            } else {
                keys.push(comp);
            }
        }
        (keys, locks)
    }

    fn solve_part1((keys, locks): Self::Input) -> Option<PartResult> {
        Some(
            keys.iter()
                .cartesian_product(locks.iter())
                .filter(|(a, b)| a.iter().zip(b.iter()).all(|(a, b)| a + b <= 5))
                .count()
                .into(),
        )
    }

    fn solve_part2(_: Self::Input) -> Option<PartResult> {
        None
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::default_tests;

    default_tests!(
        Day,
        Day,
        Some(
            "\
#####
.####
.####
.####
.#.#.
.#...
.....

#####
##.##
.#.##
...##
...#.
...#.
.....

.....
#....
#....
#...#
#.#.#
#.###
#####

.....
.....
#.#..
###..
###.#
###.#
#####

.....
.....
.....
#....
#.#..
#.#.#
#####"
        ),
        (Some(PartResult::Int(3)), None),
        (Some(PartResult::Int(3619)), None),
    );
}
