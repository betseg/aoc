use crate::utils::*;

pub struct Day;

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum BlockType {
    File(usize),
    Empty(Option<Vec<Block>>),
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Block {
    size: u8,
    typ: BlockType,
}

impl DayImpl for Day {
    type Input = &'static str;
    const DAY: u8 = 9;

    fn parse(input: &'static str) -> Self::Input {
        input
    }

    fn solve_part1(input: Self::Input) -> Option<PartResult> {
        let mut disk = input
            .bytes()
            .enumerate()
            .flat_map(|(i, c)| {
                std::iter::repeat_n(
                    if i % 2 == 0 { i / 2 } else { usize::MAX },
                    (c - b'0') as usize,
                )
            })
            .collect::<Vec<_>>();
        let mut head = 0;
        let mut tail = disk.len() - 1;
        while head < tail {
            if disk[head] != usize::MAX {
                head += 1;
            } else if disk[tail] == usize::MAX {
                tail -= 1;
            } else {
                disk.swap(head, tail);
            }
        }
        Some(
            disk.iter()
                .enumerate()
                .take_while(|&(_, &n)| n != usize::MAX)
                .map(|(i, &n)| i * n)
                .sum::<usize>()
                .into(),
        )
    }

    fn solve_part2(input: Self::Input) -> Option<PartResult> {
        let mut disk = input
            .bytes()
            .enumerate()
            .map(|(i, c)| Block {
                size: c - b'0',
                typ: if i % 2 == 0 {
                    BlockType::File(i / 2)
                } else {
                    BlockType::Empty(None)
                },
            })
            .collect::<Vec<_>>();
        for tail in (0..disk.len()).rev() {
            let (head_blocks, tail_blocks) = disk.split_at_mut(tail);

            if let Block {
                size: file_size,
                typ: BlockType::File(..),
            } = tail_blocks[0]
            {
                'head: for head in head_blocks.iter_mut() {
                    if let Block {
                        size: empty_size,
                        typ: BlockType::Empty(files),
                    } = head
                        && *empty_size >= file_size
                    {
                        let files = files.get_or_insert(Vec::new());
                        files.push(tail_blocks[0].clone());
                        tail_blocks[0].typ = BlockType::Empty(None);
                        *empty_size -= file_size;
                        break 'head;
                    }
                }
            }
        }
        Some(p2_checksum(0, &disk).1.into())
    }
}

fn p2_checksum(ix: usize, input: &[Block]) -> (usize, usize) {
    input.iter().fold((ix, 0), |(ix, sum), b| match b.typ {
        BlockType::File(id) => {
            let size = b.size as usize;
            (ix + size, sum + id * size * (size + 2 * ix - 1) / 2)
        }
        BlockType::Empty(Some(ref vec)) => {
            let res = p2_checksum(ix, vec);
            (res.0 + b.size as usize, sum + res.1)
        }
        _ => (ix + b.size as usize, sum),
    })
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::default_tests;

    default_tests!(
        Day,
        Day,
        Some("2333133121414131402"),
        (Some(PartResult::Int(1928)), Some(PartResult::Int(2858))),
        (
            Some(PartResult::Int(6359213660505)),
            Some(PartResult::Int(6381624803796))
        ),
    );
}
