use std::collections::HashMap;

use crate::utils::*;

pub struct Day;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Cell {
    Robot,
    Wall,
    BoxLeft,
    BoxRight,
    Empty,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Move {
    Up,
    Right,
    Down,
    Left,
}

impl DayImpl for Day {
    type Input = ((usize, usize), Vec<Vec<Cell>>, Vec<Move>);
    const DAY: u8 = 15;

    fn parse(input: &str) -> Self::Input {
        let (grid, moves) = input.split_once("\n\n").unwrap();
        let mut robot = None;
        let grid = grid
            .lines()
            .enumerate()
            .map(|(i, l)| {
                l.bytes()
                    .enumerate()
                    .map(|(j, c)| match c {
                        b'@' => {
                            robot = Some((i, j));
                            Cell::Robot
                        }
                        b'O' => Cell::BoxLeft,
                        b'.' => Cell::Empty,
                        b'#' => Cell::Wall,
                        _ => unreachable!(),
                    })
                    .collect()
            })
            .collect();
        let moves = moves
            .bytes()
            .filter_map(|c| match c {
                b'^' => Some(Move::Up),
                b'>' => Some(Move::Right),
                b'<' => Some(Move::Left),
                b'v' => Some(Move::Down),
                b'\n' => None,
                _ => unreachable!(),
            })
            .collect();
        (robot.unwrap(), grid, moves)
    }

    fn solve_part1((robot, grid, moves): Self::Input) -> Option<PartResult> {
        let mut grid = grid.to_owned();
        let mut robot = robot;
        for r#move in moves {
            try_move_p1(&mut robot, &mut grid, r#move);
        }
        Some(score(&grid).into())
    }

    fn solve_part2((robot, grid, moves): Self::Input) -> Option<PartResult> {
        let mut robot = (robot.0, robot.1 * 2);
        let mut grid = grid
            .iter()
            .map(|l| {
                l.iter()
                    .flat_map(|c| match c {
                        Cell::Robot => [Cell::Robot, Cell::Empty],
                        Cell::Wall => [Cell::Wall, Cell::Wall],
                        Cell::BoxLeft => [Cell::BoxLeft, Cell::BoxRight],
                        Cell::Empty => [Cell::Empty, Cell::Empty],
                        Cell::BoxRight => unreachable!(),
                    })
                    .collect::<Vec<_>>()
            })
            .collect::<Vec<_>>();
        for r#move in moves {
            try_move_p2(&mut robot, &mut grid, r#move);
        }
        Some(score(&grid).into())
    }
}

fn try_move_p1(robot: &mut (usize, usize), grid: &mut [Vec<Cell>], r#move: Move) {
    let dirs = r#move.as_dirs();
    let one_over = (
        robot.0.wrapping_add_signed(dirs.0),
        robot.1.wrapping_add_signed(dirs.1),
    );
    let mut slide = one_over;
    while grid[slide.0][slide.1] == Cell::BoxLeft {
        slide.0 = slide.0.wrapping_add_signed(dirs.0);
        slide.1 = slide.1.wrapping_add_signed(dirs.1);
    }
    match grid[slide.0][slide.1] {
        Cell::Wall => (),
        Cell::Empty => {
            grid[slide.0][slide.1] = Cell::BoxLeft;
            grid[robot.0][robot.1] = Cell::Empty;
            grid[one_over.0][one_over.1] = Cell::Robot;
            robot.0 = one_over.0;
            robot.1 = one_over.1;
        }
        _ => unreachable!(),
    }
}

fn try_move_p2(robot: &mut (usize, usize), grid: &mut [Vec<Cell>], r#move: Move) {
    let dirs = r#move.as_dirs();
    let one_over = (
        robot.0.wrapping_add_signed(dirs.0),
        robot.1.wrapping_add_signed(dirs.1),
    );
    match grid[one_over.0][one_over.1] {
        Cell::Wall => (),
        Cell::Empty => {
            grid[robot.0][robot.1] = Cell::Empty;
            grid[one_over.0][one_over.1] = Cell::Robot;
            robot.0 = one_over.0;
            robot.1 = one_over.1;
        }
        Cell::BoxLeft | Cell::BoxRight => {
            if [Move::Left, Move::Right].contains(&r#move) {
                if p2_move_horizontal(*robot, grid, r#move) {
                    robot.0 = one_over.0;
                    robot.1 = one_over.1;
                }
            } else {
                let mut seen = HashMap::new();
                if cascade_movable(*robot, grid, r#move, &mut seen) {
                    seen.insert(*robot, Cell::Robot);
                    p2_move_vertical(grid, r#move, seen);
                    robot.0 = one_over.0;
                    robot.1 = one_over.1;
                }
            }
        }
        _ => unreachable!(),
    }
}

fn p2_move_horizontal(robot: (usize, usize), grid: &mut [Vec<Cell>], r#move: Move) -> bool {
    let dirs = r#move.as_dirs();
    let one_over = (
        robot.0.wrapping_add_signed(dirs.0),
        robot.1.wrapping_add_signed(dirs.1),
    );
    let mut slide = one_over;
    while [Cell::BoxLeft, Cell::BoxRight].contains(&grid[slide.0][slide.1]) {
        slide.1 = slide.1.wrapping_add_signed(dirs.1);
    }
    if grid[robot.0][slide.1] == Cell::Empty {
        if r#move == Move::Right {
            grid[robot.0][robot.1..=slide.1].rotate_right(1);
        } else {
            grid[robot.0][slide.1..=robot.1].rotate_left(1);
        }
        true
    } else {
        false
    }
}

fn cascade_movable(
    cell: (usize, usize),
    grid: &[Vec<Cell>],
    r#move: Move,
    seen: &mut HashMap<(usize, usize), Cell>,
) -> bool {
    let dirs = r#move.as_dirs();
    let one_over = (
        cell.0.wrapping_add_signed(dirs.0),
        cell.1.wrapping_add_signed(dirs.1),
    );
    match grid[one_over.0][one_over.1] {
        Cell::Wall => false,
        Cell::Empty => {
            seen.insert(one_over, Cell::Empty);
            true
        }
        c @ (Cell::BoxLeft | Cell::BoxRight) => {
            let one_over_side = (
                one_over.0,
                one_over
                    .1
                    .wrapping_add_signed(if c == Cell::BoxLeft { 1 } else { -1 }),
            );
            seen.insert(one_over, grid[one_over.0][one_over.1]);
            seen.insert(one_over_side, grid[one_over_side.0][one_over_side.1]);
            cascade_movable(one_over, grid, r#move, seen)
                && cascade_movable(one_over_side, grid, r#move, seen)
        }
        _ => unreachable!(),
    }
}

fn p2_move_vertical(grid: &mut [Vec<Cell>], r#move: Move, seen: HashMap<(usize, usize), Cell>) {
    let dirs = r#move.as_dirs();
    for cell in seen.keys() {
        let before = (cell.0.wrapping_add_signed(-dirs.0), cell.1);
        grid[cell.0][cell.1] = *seen.get(&before).unwrap_or(&Cell::Empty);
    }
}

fn score(grid: &[Vec<Cell>]) -> usize {
    grid.iter()
        .enumerate()
        .map(|(i, l)| {
            l.iter()
                .enumerate()
                .filter_map(|(j, c)| {
                    if *c == Cell::BoxLeft {
                        Some(i * 100 + j)
                    } else {
                        None
                    }
                })
                .sum::<usize>()
        })
        .sum()
}

impl Move {
    fn as_dirs(&self) -> (isize, isize) {
        match self {
            Move::Up => (-1, 0),
            Move::Right => (0, 1),
            Move::Down => (1, 0),
            Move::Left => (0, -1),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_test() {
        assert_eq!(
            Day.run(Some(
                "\
########
#..O.O.#
##@.O..#
#...O..#
#.#.O..#
#...O..#
#......#
########

<^^>>>vv<v>>v<<"
            ))
            .0,
            Some(PartResult::Int(2028))
        );
        assert_eq!(
            Day.run(Some(
                "\
#######
#...#.#
#.....#
#..OO@#
#..O..#
#.....#
#######

<vv<<^^<<^^"
            ))
            .1,
            Some(PartResult::Int(618))
        );

        assert_eq!(
            Day.run(Some(
                "\
##########
#..O..O.O#
#......O.#
#.OO..O.O#
#..O@..O.#
#O#..O...#
#O..O..O.#
#.OO.O.OO#
#....O...#
##########

<vv>^<v^>v>^vv^v>v<>v^v<v<^vv<<<^><<><>>v<vvv<>^v^>^<<<><<v<<<v^vv^v>^
vvv<<^>^v^^><<>>><>^<<><^vv^^<>vvv<>><^^v>^>vv<>v<<<<v<^v>^<^^>>>^<v<v
><>vv>v^v^<>><>>>><^^>vv>v<^^^>>v^v^<^^>v^^>v^<^v>v<>>v^v^<v>v^^<^^vv<
<<v<^>>^^^^>>>v^<>vvv^><v<<<>^^^vv^<vvv>^>v<^^^^v<>^>vvvv><>>v^<<^^^^^
^><^><>>><>^^<<^^v>>><^<v>^<vv>>v>>>^v><>^v><<<<v>>v<v<v>vvv>^<><<>^><
^>><>^v<><^vvv<^^<><v<<<<<><^v<<<><<<^^<v<^^^><^>>^<v^><<<^>>^v<v^v<v^
>^>>^v>vv>^<<^v<>><<><<v<<v><>v<^vv<<<>^^v^>^^>>><<^v>>v^v><^^>>^<>vv^
<><^^>^^^<><vvvvv^v<v<<>^v<v>v<<^><<><<><<<^^<<<^<<>><<><^^^>^^<>^>v<>
^^>vv<^v^v<vv>^<><v<^v>^^^>>>^^vvv^>vvv<>>>^<^>>>>>^<<^v>^vvv<>^<><<v>
v^^>>><<^^<>>^v^<v^vv<>v^<<>^<^v^v><^<<<><<^<v><v<>vv>>v><v^<vv<>v^<<^"
            )),
            (Some(PartResult::Int(10092)), Some(PartResult::Int(9021)))
        );
    }

    #[test]
    fn test_my_input() {
        assert_eq!(
            Day.run(None),
            (
                Some(PartResult::Int(1429911)),
                Some(PartResult::Int(1453087))
            )
        );
    }
}
