use std::collections::{HashMap, HashSet};

use itertools::Itertools;

use crate::utils::*;

pub struct Day;

#[derive(Debug, Hash, PartialEq, Eq, Clone, Copy)]
pub struct Pos(i8, i8);

impl DayImpl for Day {
    type Input = (HashMap<u8, Vec<Pos>>, Pos);
    const DAY: u8 = 8;

    fn parse(input: &str) -> Self::Input {
        let mut map = HashMap::<u8, Vec<Pos>>::new();
        for (i, line) in input.lines().enumerate() {
            for (j, c) in line.bytes().enumerate() {
                if c != b'.' {
                    map.entry(c).or_default().push(Pos(i as i8, j as i8));
                }
            }
        }
        (
            map,
            Pos(
                input.lines().count() as i8,
                input.bytes().position(|b| b == b'\n').unwrap() as i8,
            ),
        )
    }

    fn solve_part1((input, size): Self::Input) -> Option<PartResult> {
        let mut points = HashSet::<Pos>::new();
        for (_, antennae) in input.iter() {
            for (a, b) in antennae.iter().tuple_combinations() {
                for possible in [
                    Pos(a.0 * 2 - b.0, a.1 * 2 - b.1),
                    Pos(b.0 * 2 - a.0, b.1 * 2 - a.1),
                ] {
                    if (0..size.0).contains(&possible.0) && (0..size.1).contains(&possible.1) {
                        points.insert(possible);
                    }
                }
            }
        }
        Some(points.len().into())
    }

    fn solve_part2((input, size): Self::Input) -> Option<PartResult> {
        let mut points = HashSet::<Pos>::new();
        for (_, antennae) in input.iter() {
            for (a, b) in antennae.iter().tuple_combinations() {
                for i in 0.. {
                    let new = Pos(a.0 + (a.0 - b.0) * i, a.1 + (a.1 - b.1) * i);
                    if (0..size.0).contains(&new.0) && (0..size.1).contains(&new.1) {
                        points.insert(new);
                    } else {
                        break;
                    }
                }
                for i in 0.. {
                    let new = Pos(b.0 + (b.0 - a.0) * i, b.1 + (b.1 - a.1) * i);
                    if (0..size.0).contains(&new.0) && (0..size.1).contains(&new.1) {
                        points.insert(new);
                    } else {
                        break;
                    }
                }
            }
        }
        Some(points.len().into())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::default_tests;

    default_tests!(
        Day,
        Day,
        Some(
            "\
............
........0...
.....0......
.......0....
....0.......
......A.....
............
............
........A...
.........A..
............
............"
        ),
        (Some(PartResult::Int(14)), Some(PartResult::Int(34))),
        (Some(PartResult::Int(341)), Some(PartResult::Int(1134))),
    );
}
