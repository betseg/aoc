#![feature(
    array_windows,
    binary_heap_into_iter_sorted,
    iter_map_windows,
    let_chains,
    mixed_integer_ops_unsigned_sub,
    str_from_raw_parts
)]

use clap::Parser;

mod day01;
mod day02;
mod day03;
mod day04;
mod day05;
mod day06;
mod day07;
mod day08;
mod day09;
mod day10;
mod day11;
mod day12;
mod day13;
mod day14;
mod day15;
mod day16;
mod day17;
mod day18;
mod day19;
mod day20;
mod day21;
mod day22;
mod day23;
mod day24;
mod day25;
#[macro_use]
mod utils;

use utils::*;

#[derive(Parser)]
struct Opts {
    #[arg(short, long)]
    day: Option<usize>,
}

const SOLUTIONS: [&dyn Day; 25] = [
    &day01::Day,
    &day02::Day,
    &day03::Day,
    &day04::Day,
    &day05::Day,
    &day06::Day,
    &day07::Day,
    &day08::Day,
    &day09::Day,
    &day10::Day,
    &day11::Day,
    &day12::Day,
    &day13::Day,
    &day14::Day,
    &day15::Day,
    &day16::Day,
    &day17::Day,
    &day18::Day,
    &day19::Day,
    &day20::Day,
    &day21::Day,
    &day22::Day,
    &day23::Day,
    &day24::Day,
    &day25::Day,
];

fn main() {
    let opts = Opts::parse();

    match opts.day {
        Some(day) => println!("Day {:02}: {:?}", day, SOLUTIONS[day - 1].run()),
        None => SOLUTIONS
            .iter()
            .enumerate()
            .for_each(|(i, d)| println!("Day {:02}: {:?}", i + 1, d.run())),
    }
}
