use crate::utils::*;

pub struct Day;

impl DayImpl for Day {
    type Input = ();
    const DAY: u8 = 16;

    fn parse(_: &str) -> Self::Input {}

    fn solve_part1(_: Self::Input) -> Option<PartResult> {
        None
    }

    fn solve_part2(_: Self::Input) -> Option<PartResult> {
        None
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::default_tests;

    default_tests!(Day, Day, Some(""), (None, None), (None, None),);
}
