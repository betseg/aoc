use pathfinding::prelude::dijkstra;

use crate::utils::*;

pub struct Day;

impl DayImpl for Day {
    type Input = Vec<(u16, u16)>;
    const DAY: u8 = 18;

    fn parse(input: &str) -> Self::Input {
        input
            .lines()
            .map(|l| {
                let (x, y) = l.split_once(',').unwrap();
                (x.parse().unwrap(), y.parse().unwrap())
            })
            .collect()
    }

    fn solve_part1(input: Self::Input) -> Option<PartResult> {
        let mut grid = vec![vec![true; 71]; 71];
        for (x, y) in input.iter().take(1024) {
            grid[*x as usize][*y as usize] = false;
        }
        Some(
            (dijkstra(
                &(0, 0),
                |&(x, y)| {
                    [
                        if x != 0 && grid[x as usize - 1][y as usize] {
                            Some((x - 1, y))
                        } else {
                            None
                        },
                        if y != 0 && grid[x as usize][y as usize - 1] {
                            Some((x, y - 1))
                        } else {
                            None
                        },
                        if y != 70 && grid[x as usize][y as usize + 1] {
                            Some((x, y + 1))
                        } else {
                            None
                        },
                        if x != 70 && grid[x as usize + 1][y as usize] {
                            Some((x + 1, y))
                        } else {
                            None
                        },
                    ]
                    .into_iter()
                    .flatten()
                    .map(|s| (s, 1))
                },
                |&(x, y)| x == 70 && y == 70,
            )
            .unwrap()
            .0
            .len()
                - 1)
            .into(),
        )
    }

    fn solve_part2(input: Self::Input) -> Option<PartResult> {
        let mut grid = vec![vec![true; 71]; 71];
        for (x, y) in input.iter().take(1024) {
            grid[*x as usize][*y as usize] = false;
        }
        for (x, y) in input.iter().skip(1024) {
            grid[*x as usize][*y as usize] = false;
            if dijkstra(
                &(0, 0),
                |&(x, y)| {
                    [
                        if x != 0 && grid[x as usize - 1][y as usize] {
                            Some((x - 1, y))
                        } else {
                            None
                        },
                        if y != 0 && grid[x as usize][y as usize - 1] {
                            Some((x, y - 1))
                        } else {
                            None
                        },
                        if y != 70 && grid[x as usize][y as usize + 1] {
                            Some((x, y + 1))
                        } else {
                            None
                        },
                        if x != 70 && grid[x as usize + 1][y as usize] {
                            Some((x + 1, y))
                        } else {
                            None
                        },
                    ]
                    .into_iter()
                    .flatten()
                    .map(|s| (s, 1))
                },
                |&(x, y)| x == 70 && y == 70,
            )
            .is_none()
            {
                return Some(format!("{x},{y}").into());
            }
        }
        unreachable!()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    //     #[test]
    //     fn sample_test() {
    //         assert_eq!(
    //             inner(
    //                 "\
    // 5,4
    // 4,2
    // 4,5
    // 3,0
    // 2,1
    // 6,3
    // 2,4
    // 1,5
    // 0,6
    // 3,3
    // 2,6
    // 5,1
    // 1,2
    // 5,5
    // 2,5
    // 6,5
    // 1,4
    // 0,4
    // 6,4
    // 1,1
    // 6,1
    // 1,0
    // 0,5
    // 1,6
    // 2,0"
    //             ),
    //             (Some(22), Some(601))
    //         );
    //     }

    #[test]
    fn test_my_input() {
        assert_eq!(
            Day.run(None),
            (
                Some(PartResult::Int(340)),
                Some(PartResult::String(String::from("34,32")))
            )
        );
    }
}
