use regex::Regex;

use crate::utils::*;

pub struct Day;

#[derive(Clone, Copy, Debug)]
pub enum Instruction {
    Mul(usize),
    Do,
    Dont,
}

impl DayImpl for Day {
    type Input = Vec<Instruction>;
    const DAY: u8 = 3;

    fn parse(input: &str) -> Self::Input {
        let re = Regex::new(r"(mul)\((\d+),(\d+)\)|(do)\(\)()()|(don't)\(\)()()").unwrap();
        re.captures_iter(input)
            .map(|c| c.extract::<3>())
            .map(|c| match c.1[0].len() {
                3 => Instruction::Mul(
                    c.1[1].parse::<usize>().unwrap() * c.1[2].parse::<usize>().unwrap(),
                ),
                2 => Instruction::Do,
                5 => Instruction::Dont,
                _ => unreachable!(),
            })
            .collect()
    }

    fn solve_part1(input: Self::Input) -> Option<PartResult> {
        Some(
            input
                .iter()
                .filter_map(|i| match i {
                    Instruction::Mul(a) => Some(a),
                    _ => None,
                })
                .sum::<usize>()
                .into(),
        )
    }

    fn solve_part2(input: Self::Input) -> Option<PartResult> {
        Some(
            input
                .iter()
                .fold((true, 0), |(enabled, res), i| match i {
                    Instruction::Do => (true, res),
                    Instruction::Dont => (false, res),
                    Instruction::Mul(a) if enabled => (enabled, res + a),
                    _ => (enabled, res),
                })
                .1
                .into(),
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_test() {
        assert_eq!(
            Day.run(Some(
                "xmul(2,4)%&mul[3,7]!@^do_not_mul(5,5)+mul(32,64]then(mul(11,8)mul(8,5))"
            ))
            .0,
            Some(PartResult::Int(161))
        );
        assert_eq!(
            Day.run(Some(
                "xmul(2,4)&mul[3,7]!^don't()_mul(5,5)+mul(32,64](mul(11,8)undo()?mul(8,5))"
            ))
            .1,
            Some(PartResult::Int(48))
        );
    }

    #[test]
    fn test_my_input() {
        assert_eq!(
            Day.run(None),
            (
                Some(PartResult::Int(160672468)),
                Some(PartResult::Int(84893551))
            )
        );
    }
}
