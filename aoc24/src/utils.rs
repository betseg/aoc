use std::{fmt, fs};

#[derive(Eq, PartialEq)]
pub enum PartResult {
    Int(usize),
    String(String),
}

impl From<usize> for PartResult {
    fn from(value: usize) -> Self {
        Self::Int(value)
    }
}

impl From<String> for PartResult {
    fn from(value: String) -> Self {
        Self::String(value)
    }
}

impl fmt::Debug for PartResult {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Int(i) => write!(f, "{}", i),
            Self::String(s) => write!(f, "\"{}\"", s),
        }
    }
}

pub trait DayImpl {
    type Input: Clone;
    const DAY: u8;

    fn run(&self, input: Option<&'static str>) -> (Option<PartResult>, Option<PartResult>) {
        match input {
            Some(input) => {
                let parsed = Self::parse(input);
                let part1 = Self::solve_part1(parsed.clone());
                let part2 = Self::solve_part2(parsed);
                (part1, part2)
            }
            None => unsafe {
                let (input, cap) = Self::get_input();
                let len = input.len();
                let parsed = Self::parse(std::str::from_raw_parts(input.as_ptr(), len));
                let part1 = Self::solve_part1(parsed.clone());
                let part2 = Self::solve_part2(parsed);
                drop(String::from_raw_parts(input.as_mut_ptr(), len, cap));
                (part1, part2)
            },
        }
    }

    fn get_input() -> (&'static mut str, usize) {
        let input = fs::read_to_string(format!("input/day{:02}.txt", Self::DAY)).unwrap();
        let cap = input.capacity();
        let leak = input.leak();
        (leak, cap)
    }

    fn parse(input: &'static str) -> Self::Input;
    fn solve_part1(input: Self::Input) -> Option<PartResult>;
    fn solve_part2(input: Self::Input) -> Option<PartResult>;
}

pub trait Day {
    fn run(&self) -> (Option<PartResult>, Option<PartResult>);
}

impl<T: DayImpl> Day for T {
    fn run(&self) -> (Option<PartResult>, Option<PartResult>) {
        DayImpl::run(self, None)
    }
}

#[macro_export]
macro_rules! default_tests {
    ( $day_t:ty, $day_v:expr, $sample_input:expr, $sample_test:expr, $my_test:expr, ) => {
        #[test]
        fn sample_test() {
            assert_eq!(
                <$day_t as DayImpl>::run(&$day_v, $sample_input),
                $sample_test
            );
        }

        #[test]
        fn test_my_input() {
            assert_eq!(<$day_t as DayImpl>::run(&$day_v, None), $my_test);
        }
    };
}
