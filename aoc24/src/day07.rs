use std::ops::{Add, Mul};

use rayon::prelude::*;

use crate::utils::*;

pub struct Day;

#[derive(Debug, Clone)]
pub struct Equation {
    result: usize,
    numbers: Vec<usize>,
}

impl DayImpl for Day {
    type Input = Vec<Equation>;
    const DAY: u8 = 7;

    fn parse(input: &str) -> Self::Input {
        input
            .lines()
            .map(|l| {
                let (result, numbers) = l.split_once(": ").unwrap();
                Equation {
                    result: result.parse().unwrap(),
                    numbers: numbers
                        .split_ascii_whitespace()
                        .map(str::parse)
                        .map(Result::unwrap)
                        .collect(),
                }
            })
            .collect()
    }

    fn solve_part1(input: Self::Input) -> Option<PartResult> {
        Some(
            input
                .par_iter()
                .filter(|e| is_possible(e, 2))
                .map(|e| e.result)
                .sum::<usize>()
                .into(),
        )
    }

    fn solve_part2(input: Self::Input) -> Option<PartResult> {
        Some(
            input
                .par_iter()
                .filter(|e| is_possible(e, 3))
                .map(|e| e.result)
                .sum::<usize>()
                .into(),
        )
    }
}

fn is_possible(Equation { result, numbers }: &Equation, op_cnt: usize) -> bool {
    let ops = [<usize as Add>::add, <usize as Mul>::mul, concat];
    (0..op_cnt.pow(numbers.len() as u32) - 1)
        .into_par_iter()
        .map(|i| i * op_cnt)
        .any(|i| {
            *result
                == numbers
                    .iter()
                    .cloned()
                    .enumerate()
                    .reduce(|acc, (e, n)| (e, ops[i / op_cnt.pow(e as u32) % op_cnt](acc.1, n)))
                    .unwrap()
                    .1
        })
}

fn concat(a: usize, b: usize) -> usize {
    a * 10usize.pow(b.ilog10() + 1) + b
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::default_tests;

    default_tests!(
        Day,
        Day,
        Some(
            "\
190: 10 19
3267: 81 40 27
83: 17 5
156: 15 6
7290: 6 8 6 15
161011: 16 10 13
192: 17 8 14
21037: 9 7 18 13
292: 11 6 16 20"
        ),
        (Some(PartResult::Int(3749)), Some(PartResult::Int(11387))),
        (
            Some(PartResult::Int(42283209483350)),
            Some(PartResult::Int(1026766857276279))
        ),
    );
}
