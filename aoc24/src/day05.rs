use std::cmp::Ordering;

use crate::utils::*;

pub struct Day;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Rule {
    lesser: usize,
    greater: usize,
}

#[derive(Debug, Clone)]
pub struct Update {
    pages: Vec<usize>,
}

impl DayImpl for Day {
    type Input = (Vec<Rule>, Vec<Update>);
    const DAY: u8 = 5;

    fn parse(input: &str) -> Self::Input {
        let (rules, updates) = input.split_once("\n\n").unwrap();
        let rules = rules
            .lines()
            .map(|l| Rule {
                lesser: l[0..2].parse().unwrap(),
                greater: l[3..5].parse().unwrap(),
            })
            .collect::<Vec<_>>();
        let updates = updates
            .lines()
            .map(|l| Update {
                pages: l.split(',').map(|p| p.parse().unwrap()).collect(),
            })
            .collect();
        (rules, updates)
    }

    fn solve_part1((rules, updates): Self::Input) -> Option<PartResult> {
        Some(
            updates
                .iter()
                .filter(|u| u.is_sorted_by(&rules))
                .map(|u| u.pages[u.pages.len() / 2])
                .sum::<usize>()
                .into(),
        )
    }

    fn solve_part2((rules, updates): Self::Input) -> Option<PartResult> {
        let mut updates = updates
            .iter()
            .filter(|u| !u.is_sorted_by(&rules))
            .cloned()
            .collect::<Vec<_>>();
        for update in updates.iter_mut() {
            update.sort_by(&rules);
        }
        Some(
            updates
                .iter()
                .map(|u| u.pages[u.pages.len() / 2])
                .sum::<usize>()
                .into(),
        )
    }
}

impl Update {
    fn is_sorted_by(&self, rules: &[Rule]) -> bool {
        self.pages
            .array_windows()
            .all(|[l, r]| !rules.iter().any(|ru| ru.lesser == *r && ru.greater == *l))
    }

    fn sort_by(&mut self, rules: &[Rule]) {
        self.pages.sort_unstable_by(|l, r| {
            for rule in rules {
                if (Rule {
                    lesser: *l,
                    greater: *r,
                }) == *rule
                {
                    return Ordering::Greater;
                } else if (Rule {
                    lesser: *r,
                    greater: *l,
                }) == *rule
                {
                    return Ordering::Less;
                }
            }
            Ordering::Equal
        });
    }
}

// topological sort attempt
// there were cycles :(
//
// pub fn solve_part2((rules, updates): (Vec<Rule>, Vec<Update>)) -> usize {
//     let mut rules_map = HashMap::<usize, Vec<usize>>::new();
//     for rule in rules.iter() {
//         rules_map.entry(rule.lesser).or_default().push(rule.greater);
//     }
//     let roots = rules.iter().map(|r| r.lesser).collect::<Vec<_>>();
//     let sorted = topological_sort(&roots, |root| {
//         rules_map.get(root).unwrap_or(&vec![]).clone()
//     })
//     .unwrap();
//     let mut unsorted = updates
//         .iter()
//         .filter(|u| !u.is_sorted_by(&rules))
//         .cloned()
//         .collect::<Vec<_>>();
//     for u in unsorted.iter_mut() {
//         u.pages
//             .sort_by_key(|p| sorted.iter().position(|e| e == p).unwrap());
//     }
//     unsorted.iter().map(|u| u.pages[u.pages.len() / 2]).sum()
// }

#[cfg(test)]
mod tests {
    use super::*;
    use crate::default_tests;

    default_tests!(
        Day,
        Day,
        Some(
            "\
47|53
97|13
97|61
97|47
75|29
61|13
75|53
29|13
97|29
53|29
61|53
97|53
61|29
47|13
75|47
97|75
47|61
75|61
47|29
75|13
53|13

75,47,61,53,29
97,61,53,29,13
75,29,13
75,97,47,61,53
61,13,29
97,13,75,29,47"
        ),
        (Some(PartResult::Int(143)), Some(PartResult::Int(123))),
        (Some(PartResult::Int(5588)), Some(PartResult::Int(5331))),
    );
}
