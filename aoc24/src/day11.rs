use std::collections::HashMap;

use crate::utils::*;

pub struct Day;

impl DayImpl for Day {
    type Input = Vec<usize>;
    const DAY: u8 = 11;

    fn parse(input: &str) -> Self::Input {
        input
            .split_ascii_whitespace()
            .map(str::parse)
            .map(Result::unwrap)
            .collect()
    }

    fn solve_part1(input: Self::Input) -> Option<PartResult> {
        let mut cache = HashMap::new();
        Some(
            input
                .iter()
                .map(|s| iter_stone(*s, 25, &mut cache))
                .sum::<usize>()
                .into(),
        )
    }

    fn solve_part2(input: Self::Input) -> Option<PartResult> {
        let mut cache = HashMap::new();
        Some(
            input
                .iter()
                .map(|s| iter_stone(*s, 75, &mut cache))
                .sum::<usize>()
                .into(),
        )
    }
}

fn iter_stone(stone: usize, blinks: usize, cache: &mut HashMap<(usize, usize), usize>) -> usize {
    if let Some(res) = cache.get(&(stone, blinks)) {
        return *res;
    }
    let val = if blinks == 0 {
        1
    } else if stone == 0 {
        iter_stone(1, blinks - 1, cache)
    } else {
        let digits = stone.ilog10() + 1;
        if digits % 2 == 0 {
            iter_stone(stone / 10usize.pow(digits / 2), blinks - 1, cache)
                + iter_stone(stone % 10usize.pow(digits / 2), blinks - 1, cache)
        } else {
            iter_stone(stone * 2024, blinks - 1, cache)
        }
    };
    cache.insert((stone, blinks), val);
    val
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_test() {
        assert_eq!(Day.run(Some("125 17")).0, Some(PartResult::Int(55312)));
    }

    #[test]
    fn test_my_input() {
        assert_eq!(
            Day.run(None),
            (
                Some(PartResult::Int(193269)),
                Some(PartResult::Int(228449040027793))
            )
        );
    }
}
