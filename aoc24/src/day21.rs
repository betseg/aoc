use std::collections::HashMap;

use crate::utils::*;

pub struct Day;

impl DayImpl for Day {
    type Input = (HashMap<(u8, u8), String>, Vec<Vec<u8>>);
    const DAY: u8 = 21;

    fn parse(input: &str) -> Self::Input {
        let map = HashMap::from([
            ((0, 0), "".into()),
            ((0, 1), "".into()),
            ((0, 2), "".into()),
            ((0, 3), "".into()),
            // ...
            ((14, 14), "".into()),
        ]);
        let input = input
            .lines()
            .map(|l| {
                l.chars()
                    .map(|c| c.to_digit(10))
                    .map(|n| match n {
                        Some(n) => n as u8,
                        None => 10,
                    })
                    .collect()
            })
            .collect::<Vec<_>>();
        (map, input)
    }

    fn solve_part1(_input: Self::Input) -> Option<PartResult> {
        // Some(
        //     input
        //         .iter()
        //         .map(|c| (c[0] * 100 + c[1] * 10 + c[2]) as usize * solve(c, map, 3))
        //         .sum(),
        // )
        None
    }

    fn solve_part2(_: Self::Input) -> Option<PartResult> {
        // ??????
        None
    }
}

fn _solve(input: &[u8], map: &HashMap<(u8, u8), String>, robots: usize) -> usize {
    let mut val = 0;
    for _ in 0..robots {
        let mut seq = String::new();
        for w in input.array_windows::<2>() {
            seq += &map[&(w[0], w[1])];
        }
        let next = seq
            .bytes()
            .map(|b| match b {
                b'A' => 10,
                b'^' => 11,
                b'>' => 12,
                b'v' => 13,
                b'<' => 14,
                _ => unreachable!(),
            })
            .collect::<Vec<_>>();
        val += _solve(&next, map, robots - 1);
    }
    val
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::default_tests;

    default_tests!(
        Day,
        Day,
        Some(
            "\
029A
980A
179A
456A
379A"
        ),
        (Some(PartResult::Int(126384)), None),
        (None, None),
    );
}
