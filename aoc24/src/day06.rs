use std::{
    collections::HashSet,
    ops::{Add, AddAssign, Not},
};

use rayon::prelude::*;

use crate::utils::*;

pub struct Day;

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
pub struct Pos(u8, u8);

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
pub struct Dir(u8, u8);

impl DayImpl for Day {
    type Input = (HashSet<Pos>, (u8, u8), Pos);
    const DAY: u8 = 6;

    fn parse(input: &str) -> Self::Input {
        let mut obstacles = HashSet::<Pos>::new();
        let mut guard = Pos(1, 1);
        for (i, line) in input.lines().enumerate() {
            for (j, ch) in line.bytes().enumerate() {
                if ch == b'#' {
                    obstacles.insert(Pos(i as u8 + 1, j as u8 + 1));
                }
                if ch == b'^' {
                    guard = Pos(i as u8 + 1, j as u8 + 1);
                }
            }
        }
        (
            obstacles,
            (
                input.lines().count() as u8,
                input.lines().next().unwrap().len() as u8,
            ),
            guard,
        )
    }

    fn solve_part1((obstacles, size, guard): Self::Input) -> Option<PartResult> {
        let visited = route(&obstacles, size, guard);
        Some(visited.len().into())
    }

    fn solve_part2((obstacles, size, guard): Self::Input) -> Option<PartResult> {
        Some(
            route(&obstacles, size, guard)
                .par_iter()
                .filter_map(|p| {
                    let mut obstacles = obstacles.clone();
                    let mut hit = HashSet::<(Pos, Dir)>::new();
                    obstacles.insert(*p);
                    let mut guard = guard;
                    let mut dir = Dir(0, 1);
                    loop {
                        if obstacles.contains(&(guard + dir)) {
                            if hit.contains(&(guard + dir, dir)) {
                                return Some(());
                            }
                            hit.insert((guard + dir, dir));
                            dir = !dir;
                        } else {
                            guard += dir;
                        }
                        if !guard.is_in_bounds(size) {
                            return None;
                        }
                    }
                })
                .count()
                .into(),
        )
    }
}

fn route(obstacles: &HashSet<Pos>, size: (u8, u8), guard: Pos) -> HashSet<Pos> {
    let mut visited = HashSet::<Pos>::new();
    let mut guard = guard;
    let mut dir = Dir(0, 1);
    loop {
        visited.insert(guard);
        if obstacles.contains(&(guard + dir)) {
            dir = !dir;
        } else {
            guard += dir;
        }
        if !guard.is_in_bounds(size) {
            break;
        }
    }
    visited
}

impl Pos {
    fn is_in_bounds(&self, size: (u8, u8)) -> bool {
        self.0 >= 1 && self.0 <= size.0 && self.1 >= 1 && self.1 <= size.1
    }
}

impl Add<Dir> for Pos {
    type Output = Self;

    fn add(self, rhs: Dir) -> Self::Output {
        Pos(self.0 + rhs.0 - 1, self.1 + rhs.1 - 1)
    }
}

impl AddAssign<Dir> for Pos {
    fn add_assign(&mut self, rhs: Dir) {
        self.0 = self.0 + rhs.0 - 1;
        self.1 = self.1 + rhs.1 - 1;
    }
}

impl Not for Dir {
    type Output = Self;

    fn not(self) -> Self::Output {
        match self {
            Self(0, 1) => Self(1, 2),
            Self(1, 2) => Self(2, 1),
            Self(2, 1) => Self(1, 0),
            Self(1, 0) => Self(0, 1),
            _ => unreachable!(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::default_tests;

    default_tests!(
        Day,
        Day,
        Some(
            "\
....#.....
.........#
..........
..#.......
.......#..
..........
.#..^.....
........#.
#.........
......#..."
        ),
        (Some(PartResult::Int(41)), Some(PartResult::Int(6))),
        (Some(PartResult::Int(5162)), Some(PartResult::Int(1909))),
    );
}
