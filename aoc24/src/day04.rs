use crate::utils::*;

pub struct Day;

impl DayImpl for Day {
    type Input = Vec<&'static [u8]>;
    const DAY: u8 = 4;

    fn parse(input: &'static str) -> Self::Input {
        input.lines().map(|l| l.as_bytes()).collect()
    }

    fn solve_part1(input: Self::Input) -> Option<PartResult> {
        let mut seen = 0;
        for i in 0..input.len() {
            for j in 0..input[0].len() {
                let mut possible = [0; 4];
                'ds: for d in [(0, 1), (1, -1), (1, 0), (1, 1)] {
                    #[allow(clippy::needless_range_loop)]
                    for r in 0..4 {
                        let ix = match i.checked_add_signed(r * d.0) {
                            Some(ix) => ix,
                            None => continue 'ds,
                        };
                        let jx = match j.checked_add_signed(r * d.1) {
                            Some(jx) => jx,
                            None => continue 'ds,
                        };
                        possible[r as usize] = *input
                            .get(ix)
                            .map(|l| l.get(jx))
                            .unwrap_or(Some(&0))
                            .unwrap_or(&0);
                    }
                    if possible == *b"XMAS" || possible == *b"SAMX" {
                        seen += 1;
                    }
                }
            }
        }
        Some(seen.into())
    }

    fn solve_part2(input: Self::Input) -> Option<PartResult> {
        let mut seen = 0;
        for i in 1..input.len() - 1 {
            for j in 1..input[0].len() - 1 {
                let possible = &[
                    input[i - 1][j - 1],
                    input[i - 1][j + 1],
                    input[i + 1][j + 1],
                    input[i + 1][j - 1],
                ];
                if input[i][j] == b'A'
                    && (possible == b"MMSS"
                        || possible == b"SMMS"
                        || possible == b"SSMM"
                        || possible == b"MSSM")
                {
                    seen += 1;
                }
            }
        }
        Some(seen.into())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::default_tests;

    default_tests!(
        Day,
        Day,
        Some(
            "\
MMMSXXMASM
MSAMXMSMSA
AMXSXMAAMM
MSAMASMSMX
XMASAMXAMM
XXAMMXXAMA
SMSMSASXSS
SAXAMASAAA
MAMMMXMMMM
MXMXAXMASX"
        ),
        (Some(PartResult::Int(18)), Some(PartResult::Int(9))),
        (Some(PartResult::Int(2514)), Some(PartResult::Int(1888))),
    );
}
