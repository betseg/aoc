use std::collections::{HashMap, HashSet};

use itertools::Itertools;

use crate::utils::*;

pub struct Day;

impl DayImpl for Day {
    type Input = HashMap<&'static str, HashSet<&'static str>>;
    const DAY: u8 = 23;

    fn parse(input: &'static str) -> Self::Input {
        let mut graph = HashMap::<&str, HashSet<&str>>::new();
        for line in input.lines() {
            graph.entry(&line[..2]).or_default().insert(&line[3..]);
            graph.entry(&line[3..]).or_default().insert(&line[..2]);
        }
        graph
    }

    fn solve_part1(input: Self::Input) -> Option<PartResult> {
        let mut seen = HashSet::new();
        for (node, neighbors) in input.iter().filter(|(n, _)| n.starts_with('t')) {
            for (a, b) in neighbors.iter().tuple_combinations() {
                let mut current = [node, a, b];
                current.sort_unstable();
                if input[a].contains(b) && !seen.contains(&current) {
                    seen.insert(current);
                }
            }
        }
        Some(seen.len().into())
    }

    fn solve_part2(input: Self::Input) -> Option<PartResult> {
        let mut graph = input.to_owned();
        let mut max = HashSet::new();
        while !graph.is_empty() {
            let clique = find_clique(graph.keys().next().unwrap(), &input);
            // why does this work
            // thanks eric for the good input
            for node in clique.iter() {
                graph.remove(node);
            }
            if clique.len() > max.len() {
                max = clique;
            }
        }
        let mut vec = max.iter().copied().collect::<Vec<_>>();
        vec.sort_unstable();
        Some(vec.join(",").into())
    }
}

fn find_clique(
    node: &'static str,
    graph: &HashMap<&'static str, HashSet<&'static str>>,
) -> HashSet<&'static str> {
    let mut clique = HashSet::from([node]);
    for node in graph.keys() {
        if clique.contains(node) {
            continue;
        }
        if clique.iter().all(|n| graph[*n].contains(node)) {
            clique.insert(*node);
        }
    }
    clique
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::default_tests;

    default_tests!(
        Day,
        Day,
        Some(
            "\
kh-tc
qp-kh
de-cg
ka-co
yn-aq
qp-ub
cg-tb
vc-aq
tb-ka
wh-tc
yn-cg
kh-ub
ta-co
de-co
tc-td
tb-wq
wh-td
ta-ka
td-qp
aq-cg
wq-ub
ub-vc
de-ta
wq-aq
wq-vc
wh-yn
ka-de
kh-ta
co-tc
wh-qp
tb-vc
td-yn"
        ),
        (
            Some(PartResult::Int(7)),
            Some(PartResult::String(String::from("co,de,ka,ta")))
        ),
        (
            Some(PartResult::Int(1218)),
            Some(PartResult::String(String::from(
                "ah,ap,ek,fj,fr,jt,ka,ln,me,mp,qa,ql,zg"
            )))
        ),
    );
}
