use crate::utils::*;

pub struct Day;

impl DayImpl for Day {
    type Input = Vec<Vec<u8>>;
    const DAY: u8 = 10;

    fn parse(input: &str) -> Self::Input {
        input
            .lines()
            .map(|l| l.bytes().map(|b| b - b'0').collect())
            .collect()
    }

    fn solve_part1(input: Self::Input) -> Option<PartResult> {
        let mut scores = vec![vec![(false, vec![]); input[0].len()]; input.len()];
        for line in 0..input.len() {
            for height in 0..input[0].len() {
                if input[line][height] == 9 {
                    scores[line][height].0 = true;
                    scores[line][height].1.push((line, height));
                }
            }
        }

        for dist in (0..9).rev() {
            for line in 0..input.len() {
                for height in 0..input[0].len() {
                    if input[line][height] == dist {
                        let mut paths = vec![];
                        for d in [(-1, 0), (0, -1), (0, 1), (1, 0)] {
                            if let Some(dline) = line.checked_add_signed(d.0)
                                && let Some(dheight) = height.checked_add_signed(d.1)
                                && let Some(neighbor) =
                                    input.get(dline).and_then(|l| l.get(dheight))
                                && scores[dline][dheight].0
                                && *neighbor == dist + 1
                            {
                                for c in scores[dline][dheight].1.iter() {
                                    paths.push(*c);
                                }
                            }
                        }
                        if paths.is_empty() {
                            scores[line][height].0 = false;
                        } else {
                            scores[line][height].0 = true;
                            for c in paths.iter() {
                                scores[line][height].1.push(*c);
                            }
                            scores[line][height].1.sort_unstable();
                            scores[line][height].1.dedup();
                        }
                    }
                }
            }
        }
        let mut sum = 0;
        for line in 0..scores.len() {
            for height in 0..scores[0].len() {
                if scores[line][height].0 && input[line][height] == 0 {
                    sum += scores[line][height].1.len();
                }
            }
        }
        Some(sum.into())
    }

    fn solve_part2(input: Self::Input) -> Option<PartResult> {
        let mut scores = input
            .iter()
            .map(|l| {
                l.iter()
                    .map(|&h| if h == 9 { (true, 1) } else { (true, 0) })
                    .collect::<Vec<_>>()
            })
            .collect::<Vec<_>>();
        for dist in (0..9).rev() {
            for line in 0..input.len() {
                for height in 0..input[0].len() {
                    if input[line][height] == dist {
                        let mut paths = 0;
                        for d in [(-1, 0), (0, -1), (0, 1), (1, 0)] {
                            if let Some(dline) = line.checked_add_signed(d.0)
                                && let Some(dheight) = height.checked_add_signed(d.1)
                                && let Some(neighbor) =
                                    input.get(dline).and_then(|l| l.get(dheight))
                                && scores[dline][dheight].0
                                && *neighbor == dist + 1
                            {
                                paths += scores[dline][dheight].1;
                            }
                        }
                        if paths == 0 {
                            scores[line][height].0 = false;
                        } else {
                            scores[line][height].1 += paths;
                        }
                    }
                }
            }
        }
        let mut sum = 0;
        for line in 0..scores.len() {
            for height in 0..scores[0].len() {
                if scores[line][height].0 && input[line][height] == 0 {
                    sum += scores[line][height].1 as usize;
                }
            }
        }
        Some(sum.into())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::default_tests;

    default_tests!(
        Day,
        Day,
        Some(
            "\
89010123
78121874
87430965
96549874
45678903
32019012
01329801
10456732"
        ),
        (Some(PartResult::Int(36)), Some(PartResult::Int(81))),
        (Some(PartResult::Int(811)), Some(PartResult::Int(1794))),
    );
}
