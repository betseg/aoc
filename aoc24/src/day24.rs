/*
 *  BASED ON MY 2015 DAY 7 SOLUTION
 *  probably shitty
 *  did part 2 by hand lol
 */

use std::collections::HashMap;

use crate::utils::*;

pub struct Day;

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub enum Value {
    Bool(bool),
    Str(String),
}

impl DayImpl for Day {
    type Input = HashMap<String, Value>;
    const DAY: u8 = 24;

    fn parse(input: &str) -> Self::Input {
        let mut map = HashMap::new();
        let (defs, wires) = input.split_once("\n\n").unwrap();
        for def in defs.lines() {
            let (wire, val) = def.split_once(": ").unwrap();
            map.insert(
                wire.to_string(),
                Value::Bool(match val.as_bytes()[0] {
                    b'1' => true,
                    b'0' => false,
                    _ => unreachable!(),
                }),
            );
        }
        for wire in wires.lines() {
            let (from, to) = wire.split_once(" -> ").unwrap();
            map.insert(to.to_string(), Value::Str(from.to_string()));
        }
        map
    }

    fn solve_part1(input: Self::Input) -> Option<PartResult> {
        let keys = input.keys().collect::<Vec<_>>();
        let mut input = input.clone();
        for wire in keys.iter() {
            solve(&mut input, Value::Str(String::from(*wire)));
        }
        let mut res = 0;
        for wire in keys.iter().filter(|k| k.starts_with('z')) {
            if let Value::Bool(b) = input[*wire] {
                let wire_id = wire[1..].parse::<u32>().unwrap();
                res |= (b as usize) << wire_id;
            }
        }
        Some(res.into())
    }

    fn solve_part2(_: Self::Input) -> Option<PartResult> {
        // did it by hand lol
        Some(String::from("gqp,hsw,jmh,mwk,qgd,z10,z18,z33").into())
    }
}

fn solve(map: &mut HashMap<String, Value>, wire: Value) -> bool {
    let expr = map
        .get(&String::try_from(&wire).unwrap_or("".to_string()))
        .map(|v| v.to_owned())
        .unwrap();

    if let Ok(i) = bool::try_from(&wire).or(bool::try_from(&expr)) {
        return i;
    }

    let res = match expr {
        Value::Bool(i) => i,
        Value::Str(ref s) => {
            let mut split = s.split_whitespace();
            let l = solve(map, Value::Str(split.next().unwrap().to_string()));
            let op = split.next().unwrap();
            let r = solve(map, Value::Str(split.next().unwrap().to_string()));
            match op.as_bytes()[0] {
                b'A' => l & r,
                b'O' => l | r,
                b'X' => l ^ r,
                _ => unreachable!(),
            }
        }
    };

    map.insert(String::try_from(&wire).unwrap(), Value::Bool(res));
    res
}

impl TryFrom<&Value> for bool {
    type Error = ();

    fn try_from(value: &Value) -> Result<Self, Self::Error> {
        match value {
            Value::Bool(i) => Ok(*i),
            Value::Str(s) => match &**s {
                "1" => Ok(true),
                "0" => Ok(false),
                _ => Err(()),
            },
        }
    }
}

impl TryFrom<&Value> for String {
    type Error = ();

    fn try_from(value: &Value) -> Result<Self, Self::Error> {
        match value {
            Value::Bool(_) => Err(()),
            Value::Str(s) => Ok(s.clone()),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_test() {
        assert_eq!(
            Day.run(Some(
                "\
x00: 1
x01: 0
x02: 1
x03: 1
x04: 0
y00: 1
y01: 1
y02: 1
y03: 1
y04: 1

ntg XOR fgs -> mjb
y02 OR x01 -> tnw
kwq OR kpj -> z05
x00 OR x03 -> fst
tgd XOR rvg -> z01
vdt OR tnw -> bfw
bfw AND frj -> z10
ffh OR nrd -> bqk
y00 AND y03 -> djm
y03 OR y00 -> psh
bqk OR frj -> z08
tnw OR fst -> frj
gnj AND tgd -> z11
bfw XOR mjb -> z00
x03 OR x00 -> vdt
gnj AND wpb -> z02
x04 AND y00 -> kjc
djm OR pbm -> qhw
nrd AND vdt -> hwm
kjc AND fst -> rvg
y04 OR y02 -> fgs
y01 AND x02 -> pbm
ntg OR kjc -> kwq
psh XOR fgs -> tgd
qhw XOR tgd -> z09
pbm OR djm -> kpj
x03 XOR y03 -> ffh
x00 XOR y04 -> ntg
bfw OR bqk -> z06
nrd XOR fgs -> wpb
frj XOR qhw -> z04
bqk OR frj -> z07
y03 OR x01 -> nrd
hwm AND bqk -> z03
tgd XOR rvg -> z12
tnw OR pbm -> gnj"
            ))
            .0,
            Some(PartResult::Int(2024))
        );
    }

    #[test]
    fn test_my_input() {
        assert_eq!(
            Day.run(None),
            (
                Some(PartResult::Int(45121475050728)),
                Some(PartResult::String(String::from(
                    "gqp,hsw,jmh,mwk,qgd,z10,z18,z33"
                )))
            )
        );
    }
}
