use std::collections::BinaryHeap;

use itertools::Itertools;

use crate::utils::*;

pub struct Day;

impl DayImpl for Day {
    type Input = (BinaryHeap<usize>, BinaryHeap<usize>);
    const DAY: u8 = 1;

    fn parse(input: &str) -> Self::Input {
        let len = input.bytes().position(|b| b == b' ').unwrap();
        input
            .lines()
            .map(|s| {
                (
                    s[0..len].parse::<usize>().unwrap(),
                    s[len + 3..len * 2 + 3].parse::<usize>().unwrap(),
                )
            })
            .unzip()
    }

    fn solve_part1((left, right): Self::Input) -> Option<PartResult> {
        Some(
            left.clone()
                .into_iter_sorted()
                .zip(right.clone().into_iter_sorted())
                .map(|(l, r)| l.abs_diff(r))
                .sum::<usize>()
                .into(),
        )
    }

    fn solve_part2((left, right): Self::Input) -> Option<PartResult> {
        let counts = right.iter().counts();
        Some(
            left.iter()
                .map(|n| counts.get(n).map(|c| c * n).unwrap_or_default())
                .sum::<usize>()
                .into(),
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::default_tests;

    default_tests!(
        Day,
        Day,
        Some(
            "\
3   4
4   3
2   5
1   3
3   9
3   3"
        ),
        (Some(PartResult::Int(11)), Some(PartResult::Int(31))),
        (
            Some(PartResult::Int(2000468)),
            Some(PartResult::Int(18567089))
        ),
    );
}
