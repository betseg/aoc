use std::collections::{HashMap, HashSet};

use crate::utils::*;

pub struct Day;

impl DayImpl for Day {
    type Input = Vec<usize>;
    const DAY: u8 = 22;

    fn parse(input: &str) -> Self::Input {
        input
            .split_ascii_whitespace()
            .map(str::parse)
            .map(Result::unwrap)
            .collect()
    }

    fn solve_part1(input: Self::Input) -> Option<PartResult> {
        Some(
            input
                .iter()
                .map(|n| {
                    let mut n = *n;
                    for _ in 0..2000 {
                        change(&mut n);
                    }
                    n
                })
                .sum::<usize>()
                .into(),
        )
    }

    fn solve_part2(input: Self::Input) -> Option<PartResult> {
        let mut scores = HashMap::<[i8; 4], usize>::new();
        let mut seen_diffs = HashSet::new();
        for n in input {
            let mut n = n;
            let mut last_diffs = [0; 4];
            (1..5).for_each(|i| {
                let prev = n;
                change(&mut n);
                last_diffs[i - 1] = (n % 10) as i8 - (prev % 10) as i8;
            });
            *scores.entry(last_diffs).or_default() += n % 10;
            seen_diffs.insert(last_diffs);
            for _ in 0..1995 {
                last_diffs.rotate_left(1);
                let prev = n;
                change(&mut n);
                *last_diffs.last_mut().unwrap() = (n % 10) as i8 - (prev % 10) as i8;
                if !seen_diffs.contains(&last_diffs) {
                    *scores.entry(last_diffs).or_default() += n % 10;
                }
                seen_diffs.insert(last_diffs);
            }
            seen_diffs.drain();
        }
        Some((*scores.values().max().unwrap()).into())
    }
}

fn change(n: &mut usize) {
    *n ^= *n << 6;
    *n %= 1 << 24;
    *n ^= *n >> 5;
    *n %= 1 << 24;
    *n ^= *n << 11;
    *n %= 1 << 24;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_test() {
        assert_eq!(
            Day.run(Some(
                "\
1
10
100
2024"
            ))
            .0,
            Some(PartResult::Int(37327623))
        );
        assert_eq!(
            Day.run(Some(
                "\
1
2
3
2024"
            ))
            .1,
            Some(PartResult::Int(23))
        );
    }

    #[test]
    fn test_my_input() {
        assert_eq!(
            Day.run(None),
            (
                Some(PartResult::Int(12664695565)),
                Some(PartResult::Int(1444))
            )
        );
    }
}
