use crate::utils::*;

pub struct Day;

impl DayImpl for Day {
    type Input = Vec<Vec<usize>>;
    const DAY: u8 = 2;

    fn parse(input: &str) -> Self::Input {
        input
            .lines()
            .map(|l| {
                l.split_ascii_whitespace()
                    .map(str::parse)
                    .map(Result::unwrap)
                    .collect()
            })
            .collect()
    }

    fn solve_part1(input: Self::Input) -> Option<PartResult> {
        Some(
            input
                .iter()
                .filter(|l| report_safe(l[0].cmp(&l[1]), l.iter()))
                .count()
                .into(),
        )
    }

    fn solve_part2(input: Self::Input) -> Option<PartResult> {
        Some(
            input
                .iter()
                .filter(|l| {
                    (0..l.len()).any(|i| {
                        report_safe(
                            if i == 0 {
                                l[1].cmp(&l[2])
                            } else if i == 1 {
                                l[0].cmp(&l[2])
                            } else {
                                l[0].cmp(&l[1])
                            },
                            l.iter().take(i).chain(l.iter().skip(i + 1)),
                        )
                    })
                })
                .count()
                .into(),
        )
    }
}

fn report_safe<'a>(cmp: std::cmp::Ordering, l: impl Iterator<Item = &'a usize>) -> bool {
    l.map_windows(|[a, b]| a.cmp(b) == cmp && (1..=3).contains(&a.abs_diff(**b)))
        .all(std::convert::identity)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::default_tests;

    default_tests!(
        Day,
        Day,
        Some(
            "\
7 6 4 2 1
1 2 7 8 9
9 7 6 2 1
1 3 2 4 5
8 6 4 4 1
1 3 6 7 9"
        ),
        (Some(PartResult::Int(2)), Some(PartResult::Int(4))),
        (Some(PartResult::Int(230)), Some(PartResult::Int(301))),
    );
}
