use rayon::prelude::*;
use scan_fmt::scan_fmt;

use crate::utils::*;

pub struct Day;

impl DayImpl for Day {
    type Input = Vec<[[isize; 3]; 2]>;
    const DAY: u8 = 13;

    fn parse(input: &'static str) -> Self::Input {
        input
            .split("\n\n")
            .par_bridge()
            .map(|c| {
                let (a_x, a_y, b_x, b_y, p_x, p_y) = scan_fmt!(
                    c,
                    "Button A: X+{}, Y+{}\nButton B: X+{}, Y+{}\nPrize: X={}, Y={}",
                    isize,
                    isize,
                    isize,
                    isize,
                    isize,
                    isize
                )
                .unwrap();
                [[a_x, b_x, p_x], [a_y, b_y, p_y]]
            })
            .collect()
    }

    fn solve_part1(input: Self::Input) -> Option<PartResult> {
        Some(
            (input
                .iter()
                .filter_map(|c| {
                    let (x, y) = solve(*c);
                    if x <= 100
                        && y <= 100
                        && x * c[0][0] + y * c[0][1] == c[0][2]
                        && x * c[1][0] + y * c[1][1] == c[1][2]
                    {
                        Some(3 * x + y)
                    } else {
                        None
                    }
                })
                .sum::<isize>() as usize)
                .into(),
        )
    }

    fn solve_part2(input: Self::Input) -> Option<PartResult> {
        Some(
            (input
                .iter()
                .filter_map(|c| {
                    let new_c = [
                        [c[0][0], c[0][1], c[0][2] + 10000000000000],
                        [c[1][0], c[1][1], c[1][2] + 10000000000000],
                    ];
                    let (x, y) = solve(new_c);
                    if x * new_c[0][0] + y * new_c[0][1] == new_c[0][2]
                        && x * new_c[1][0] + y * new_c[1][1] == new_c[1][2]
                    {
                        Some(3 * x + y)
                    } else {
                        None
                    }
                })
                .sum::<isize>() as usize)
                .into(),
        )
    }
}

fn solve(c: [[isize; 3]; 2]) -> (isize, isize) {
    let det = c[0][0] * c[1][1] - c[0][1] * c[1][0];
    let x = (c[0][2] * c[1][1] - c[0][1] * c[1][2]) / det;
    let y = (c[0][0] * c[1][2] - c[0][2] * c[1][0]) / det;
    (x, y)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_test() {
        assert_eq!(
            Day.run(Some(
                "\
Button A: X+94, Y+34
Button B: X+22, Y+67
Prize: X=8400, Y=5400

Button A: X+26, Y+66
Button B: X+67, Y+21
Prize: X=12748, Y=12176

Button A: X+17, Y+86
Button B: X+84, Y+37
Prize: X=7870, Y=6450

Button A: X+69, Y+23
Button B: X+27, Y+71
Prize: X=18641, Y=10279"
            ))
            .0,
            Some(PartResult::Int(480))
        );
    }

    #[test]
    fn test_my_input() {
        assert_eq!(
            Day.run(None),
            (
                Some(PartResult::Int(39996)),
                Some(PartResult::Int(73267584326867))
            )
        );
    }
}
