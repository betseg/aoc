use crate::utils::*;

pub struct Day;

impl DayImpl for Day {
    type Input = Vec<Vec<u8>>;
    const DAY: u8 = 12;

    fn parse(input: &str) -> Self::Input {
        input.lines().map(|l| l.bytes().collect()).collect()
    }

    fn solve_part1(input: Self::Input) -> Option<PartResult> {
        let mut seen = vec![vec![false; input[0].len()]; input.len()];
        let mut sum = 0;
        for i in 0..seen.len() {
            for j in 0..seen[0].len() {
                if !seen[i][j] {
                    let score = flood_fill(&input, &mut seen, i, j);
                    sum += score.0 * score.1;
                }
            }
        }
        Some(sum.into())
    }

    fn solve_part2(_: Self::Input) -> Option<PartResult> {
        None
    }
}

fn flood_fill(input: &[Vec<u8>], seen: &mut [Vec<bool>], i: usize, j: usize) -> (usize, usize) {
    let mut area = 0;
    let mut edges = 0;
    if !seen[i][j] {
        seen[i][j] = true;
        area = 1;
        for d in [(-1, 0), (0, -1), (0, 1), (1, 0)] {
            if let Some(di) = i.checked_add_signed(d.0)
                && let Some(dj) = j.checked_add_signed(d.1)
                && let Some(neighbor) = seen.get(di).and_then(|l| l.get(dj))
                && input[di][dj] == input[i][j]
            {
                if !*neighbor {
                    let score = flood_fill(input, seen, di, dj);
                    area += score.0;
                    edges += score.1;
                }
            } else {
                edges += 1;
            }
        }
    }
    (area, edges)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_test() {
        assert_eq!(
            Day.run(Some(
                "\
AAAA
BBCD
BBCC
EEEC"
            )),
            (Some(PartResult::Int(140)), Some(PartResult::Int(80)))
        );
        assert_eq!(
            Day.run(Some(
                "\
OOOOO
OXOXO
OOOOO
OXOXO
OOOOO"
            )),
            (Some(PartResult::Int(772)), Some(PartResult::Int(436)))
        );
        assert_eq!(
            Day.run(Some(
                "\
RRRRIICCFF
RRRRIICCCF
VVRRRCCFFF
VVRCCCJFFF
VVVVCJJCFE
VVIVCCJJEE
VVIIICJJEE
MIIIIIJJEE
MIIISIJEEE
MMMISSJEEE"
            )),
            (Some(PartResult::Int(1930)), Some(PartResult::Int(1206)))
        );
        assert_eq!(
            Day.run(Some(
                "\
EEEEE
EXXXX
EEEEE
EXXXX
EEEEE"
            ))
            .1,
            Some(PartResult::Int(236))
        );
        assert_eq!(
            Day.run(Some(
                "\
AAAAAA
AAABBA
AAABBA
ABBAAA
ABBAAA
AAAAAA"
            ))
            .1,
            Some(PartResult::Int(368))
        );
    }

    #[test]
    fn test_my_input() {
        assert_eq!(Day.run(None), (Some(PartResult::Int(1424472)), None));
    }
}
