use std::collections::HashMap;

use regex::Regex;

use crate::utils::*;

pub struct Day;

impl DayImpl for Day {
    type Input = (Vec<&'static str>, Vec<&'static str>);
    const DAY: u8 = 19;

    fn parse(input: &'static str) -> Self::Input {
        let (towels, designs) = input.split_once("\n\n").unwrap();
        let towels = towels.split(", ").collect();
        let designs = designs.lines().collect();
        (towels, designs)
    }

    fn solve_part1((towels, designs): Self::Input) -> Option<PartResult> {
        // let mut map = HashMap::new();
        // Some(
        //     designs
        //         .iter()
        //         .map(|d| possible_ways(d, &towels, &mut map))
        //         .count(),
        // )
        // regex faster
        let re = format!("^({})+$", towels.join("|"));
        let re = Regex::new(&re).unwrap();
        Some(designs.iter().filter(|d| re.is_match(d)).count().into())
    }

    fn solve_part2((towels, designs): Self::Input) -> Option<PartResult> {
        let mut map = HashMap::new();
        Some(
            designs
                .iter()
                .map(|d| possible_ways(d, &towels, &mut map))
                .sum::<usize>()
                .into(),
        )
    }
}

fn possible_ways(
    design: &'static str,
    towels: &[&'static str],
    map: &mut HashMap<&'static str, usize>,
) -> usize {
    if let Some(cnt) = map.get(design) {
        *cnt
    } else if design.is_empty() {
        1
    } else {
        let cnt = towels
            .iter()
            .map(|t| {
                if !design.starts_with(t) {
                    0
                } else {
                    possible_ways(&design[t.len()..], towels, map)
                }
            })
            .sum();
        map.insert(design, cnt);
        cnt
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::default_tests;

    default_tests!(
        Day,
        Day,
        Some(
            "\
r, wr, b, g, bwu, rb, gb, br

brwrr
bggr
gbbr
rrbgbr
ubwu
bwurrg
brgr
bbrgwb"
        ),
        (Some(PartResult::Int(6)), Some(PartResult::Int(16))),
        (
            Some(PartResult::Int(315)),
            Some(PartResult::Int(625108891232249))
        ),
    );
}
