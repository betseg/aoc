use std::collections::HashMap;

use crate::utils::*;

pub struct Day;

#[derive(Debug, Clone, Copy)]
pub struct Robot {
    pos: (isize, isize),
    vel: (isize, isize),
}

impl DayImpl for Day {
    type Input = Vec<Robot>;
    const DAY: u8 = 14;

    fn parse(input: &'static str) -> Self::Input {
        input
            .lines()
            .map(|l| {
                let (pos, vel) = l.split_once(' ').unwrap();
                let (p_x, p_y) = pos[2..].split_once(',').unwrap();
                let p_x = p_x.parse().unwrap();
                let p_y = p_y.parse().unwrap();
                let (v_x, v_y) = vel[2..].split_once(',').unwrap();
                let v_x = v_x.parse().unwrap();
                let v_y = v_y.parse().unwrap();
                Robot {
                    pos: (p_x, p_y),
                    vel: (v_x, v_y),
                }
            })
            .collect()
    }

    fn solve_part1(input: Self::Input) -> Option<PartResult> {
        let mut map = HashMap::<(isize, isize), usize>::new();
        for robot in input {
            *map.entry((
                (robot.pos.0 + robot.vel.0 * 100).rem_euclid(101),
                (robot.pos.1 + robot.vel.1 * 100).rem_euclid(103),
            ))
            .or_default() += 1;
        }
        let mut quads = [0; 4];
        for (loc, n) in map {
            #[allow(non_contiguous_range_endpoints)]
            match loc {
                (..50, ..51) => quads[0] += n,
                (..50, 52..) => quads[1] += n,
                (51.., ..51) => quads[2] += n,
                (51.., 52..) => quads[3] += n,
                _ => (),
            }
        }
        Some(quads.iter().product::<usize>().into())
    }

    fn solve_part2(input: Self::Input) -> Option<PartResult> {
        let robots = input.len();
        for i in 0.. {
            let mut map = HashMap::<(isize, isize), usize>::new();
            for robot in &input {
                *map.entry((
                    (robot.pos.0 + robot.vel.0 * i).rem_euclid(101),
                    (robot.pos.1 + robot.vel.1 * i).rem_euclid(103),
                ))
                .or_default() += 1;
            }
            let mut quads = [0; 4];
            for (loc, n) in map {
                #[allow(non_contiguous_range_endpoints)]
                match loc {
                    (..50, ..51) => quads[0] += n,
                    (..50, 52..) => quads[1] += n,
                    (51.., ..51) => quads[2] += n,
                    (51.., 52..) => quads[3] += n,
                    _ => (),
                }
            }
            if quads[0] > robots / 2
                || quads[1] > robots / 2
                || quads[2] > robots / 2
                || quads[3] > robots / 2
            {
                return Some((i as usize).into());
            }
        }
        unreachable!()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    //     #[test]
    //     fn sample_test() {
    //         assert_eq!(
    //             inner(
    //                 "\
    // p=0,4 v=3,-3
    // p=6,3 v=-1,-3
    // p=10,3 v=-1,2
    // p=2,0 v=2,-1
    // p=0,0 v=1,3
    // p=3,0 v=-2,-2
    // p=7,6 v=-1,-3
    // p=3,0 v=-1,-2
    // p=9,3 v=2,3
    // p=7,3 v=-1,2
    // p=2,4 v=2,-3
    // p=9,5 v=-3,-3"
    //             ),
    //             (Some(12), None)
    //         );
    //     }

    #[test]
    fn test_my_input() {
        assert_eq!(
            Day.run(None),
            (
                Some(PartResult::Int(224969976)),
                Some(PartResult::Int(7892))
            )
        );
    }
}
