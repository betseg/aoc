use std::collections::HashSet;

const INPUT: &[u8] = include_bytes!("../input/day06.txt");

pub fn run() -> (usize, usize) {
    inner(INPUT)
}

fn inner(input: &[u8]) -> (usize, usize) {
    let part1 = solve(input, 4);
    let part2 = solve(input, 14);
    (part1, part2)
}

fn solve(input: &[u8], n: usize) -> usize {
    input
        .windows(n)
        .position(|a| a.iter().collect::<HashSet<_>>().len() == n)
        .unwrap()
        + n
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_test() {
        assert_eq!(inner(b"mjqjpqmgbljsphdztnvjfqwrcgsmlb"), (7, 19));
    }

    #[test]
    fn test_my_input() {
        assert_eq!(run(), (1238, 3037));
    }
}
