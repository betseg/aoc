use itertools::Itertools;

const INPUT: &[u8] = include_bytes!("../input/day03.txt");

pub fn run() -> (usize, usize) {
    inner(INPUT)
}

fn inner(input: &[u8]) -> (usize, usize) {
    let part1 = solve_part1(input);
    let part2 = solve_part2(input);
    (part1, part2)
}

fn solve_part1(input: &[u8]) -> usize {
    input
        .split(|&b| b == b'\n')
        .map(|i| reduce(i.iter().chunks(i.len() / 2).into_iter().map(str_to_bits)))
        .sum::<u32>() as usize
}

fn solve_part2(input: &[u8]) -> usize {
    input
        .split(|&b| b == b'\n')
        .chunks(3)
        .into_iter()
        .map(|c| reduce(c.map(|b| str_to_bits(b.iter()))))
        .sum::<u32>() as usize
}

fn reduce(input: impl Iterator<Item = u64>) -> u32 {
    input.reduce(|a: u64, s| a & s).unwrap().trailing_zeros()
}

fn str_to_bits<'a>(input: impl Iterator<Item = &'a u8>) -> u64 {
    input
        .map(|&b| 1 << item_to_val(b))
        .reduce(|a, s| a | s)
        .unwrap()
}

fn item_to_val(item: u8) -> u32 {
    (if item.is_ascii_lowercase() {
        item - b'a' + 1
    } else {
        item - b'A' + 27
    }) as u32
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_test() {
        assert_eq!(
            inner(
                b"vJrwpWtwJgWrhcsFMMfFFhFp\n\
        jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL\n\
        PmmdzqPrVvPwwTWBwg\n\
        wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn\n\
        ttgJtRGJQctTZtZT\n\
        CrZsJsPPZsGzwwsLwLmpwMDw"
            ),
            (157, 70)
        );
    }

    #[test]
    fn test_my_input() {
        assert_eq!(run(), (7597, 2607));
    }
}
