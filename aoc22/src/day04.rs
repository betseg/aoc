use scan_fmt::scan_fmt;

const INPUT: &str = include_str!("../input/day04.txt");

pub fn run() -> (usize, usize) {
    inner(INPUT)
}

fn inner(input: &str) -> (usize, usize) {
    let parsed = parse(input);
    let part1 = solve_part1(&parsed);
    let part2 = solve_part2(&parsed);
    (part1, part2)
}

fn parse(input: &str) -> Vec<(i16, i16, i16, i16)> {
    input
        .lines()
        .map(|l| scan_fmt!(l, "{}-{},{}-{}", i16, i16, i16, i16).unwrap())
        .collect()
}

fn solve_part1(input: &[(i16, i16, i16, i16)]) -> usize {
    input
        .iter()
        .filter(|(a, b, c, d)| (c - a) * (d - b) <= 0)
        .count()
}

fn solve_part2(input: &[(i16, i16, i16, i16)]) -> usize {
    input
        .iter()
        .filter(|(a, b, c, d)| a.max(c) <= b.min(d))
        .count()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_test() {
        assert_eq!(
            inner(
                "2-4,6-8\n\
        2-3,4-5\n\
        5-7,7-9\n\
        2-8,3-7\n\
        6-6,4-6\n\
        2-6,4-8"
            ),
            (2, 4)
        );
    }

    #[test]
    fn test_my_input() {
        assert_eq!(run(), (485, 857));
    }
}
