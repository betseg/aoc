use std::{cmp::Ordering, ops::Not};

const INPUT: &[u8] = include_bytes!("../input/day02.txt");

#[derive(PartialEq, Eq, Clone, Copy)]
enum Hand {
    Rock,
    Paper,
    Scissors,
}

pub fn run() -> (usize, usize) {
    inner(INPUT)
}

fn inner(input: &[u8]) -> (usize, usize) {
    let parsed = parse(input);
    let part1 = solve_part1(parsed.iter().copied());
    let part2 = solve_part2(parsed.into_iter());
    (part1, part2)
}

fn parse(input: &[u8]) -> Vec<(Hand, Hand)> {
    input
        .split(|&b| b == b'\n')
        .map(|l| (l[0].into(), l[2].into()))
        .collect()
}

fn solve_part1(input: impl Iterator<Item = (Hand, Hand)>) -> usize {
    input
        .map(|(opn, own)| {
            (match own {
                Hand::Rock => 1,
                Hand::Paper => 2,
                Hand::Scissors => 3,
            } + match own.cmp(&opn) {
                Ordering::Less => 0,
                Ordering::Equal => 3,
                Ordering::Greater => 6,
            })
        })
        .sum()
}

fn solve_part2(input: impl Iterator<Item = (Hand, Hand)>) -> usize {
    solve_part1(input.map(|(opn, own)| {
        (
            opn,
            match own {
                Hand::Rock => !!opn,
                Hand::Paper => opn,
                Hand::Scissors => !opn,
            },
        )
    }))
}

impl From<u8> for Hand {
    fn from(s: u8) -> Self {
        match s {
            b'A' | b'X' => Self::Rock,
            b'B' | b'Y' => Self::Paper,
            b'C' | b'Z' => Self::Scissors,
            _ => unreachable!(),
        }
    }
}

impl Ord for Hand {
    fn cmp(&self, other: &Self) -> Ordering {
        if self == other {
            Ordering::Equal
        } else if self == &Self::Paper && other == &Self::Rock
            || self == &Self::Rock && other == &Self::Scissors
            || self == &Self::Scissors && other == &Self::Paper
        {
            Ordering::Greater
        } else {
            Ordering::Less
        }
    }
}

impl PartialOrd for Hand {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Not for Hand {
    type Output = Self;

    fn not(self) -> Self::Output {
        match self {
            Hand::Rock => Hand::Paper,
            Hand::Paper => Hand::Scissors,
            Hand::Scissors => Hand::Rock,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_test() {
        assert_eq!(inner(b"A Y\nB X\nC Z"), (15, 12));
    }

    #[test]
    fn test_my_input() {
        assert_eq!(run(), (15572, 16098));
    }
}
