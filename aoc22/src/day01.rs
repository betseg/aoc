use std::cmp::Reverse;

use itertools::Itertools;

const INPUT: &str = include_str!("../input/day01.txt");

pub fn run() -> (usize, usize) {
    inner(INPUT)
}

fn inner(input: &str) -> (usize, usize) {
    let parsed = parse(input);
    let part1 = parsed[0];
    let part2 = parsed[0..3].iter().sum();
    (part1, part2)
}

fn parse(input: &str) -> Vec<usize> {
    input
        .split("\n\n")
        .map(|n| n.lines().map(|n| n.parse::<usize>().unwrap()).sum())
        .sorted_unstable_by_key(|&a| Reverse(a))
        .collect::<Vec<_>>()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_test() {
        assert_eq!(
            inner(
                "1000\n\
            2000\n\
            3000\n\
            \n\
            4000\n\
            \n\
            5000\n\
            6000\n\
            \n\
            7000\n\
            8000\n\
            9000\n\
            \n\
            10000"
            ),
            (24000, 45000)
        );
    }

    #[test]
    fn test_my_input() {
        assert_eq!(run(), (72017, 212520));
    }
}
