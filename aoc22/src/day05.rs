use scan_fmt::scan_fmt;

const INPUT: &str = include_str!("../input/day05.txt");

#[derive(Clone, Debug)]
struct Move {
    from: u8,
    to: u8,
    n: u8,
}

pub fn run() -> (usize, usize) {
    inner(INPUT)
}

fn inner(input: &str) -> (usize, usize) {
    let parsed = parse(input);
    let part1 = solve(parsed.clone(), true);
    // println!();
    let part2 = solve(parsed, false);
    // println!();
    (part1, part2)
}

fn parse(input: &str) -> (Vec<Vec<char>>, Vec<Move>) {
    let (stacks, moves) = input.split_once("\n\n").unwrap();
    let stack_cnt = (stacks.lines().next().unwrap().len() + 1) / 4;
    let mut stacks_v = vec![vec![]; stack_cnt];
    for line in stacks.lines().map(|l| l.as_bytes()) {
        for i in 0..stack_cnt {
            if line[i * 4 + 1].is_ascii_uppercase() {
                stacks_v[i].push(line[i * 4 + 1] as char);
            }
        }
    }
    for stack in &mut stacks_v {
        stack.reverse();
    }
    let moves = moves
        .lines()
        .map(|l| scan_fmt!(l, "move {} from {} to {}", u8, u8, u8).unwrap())
        .map(|(n, from, to)| Move {
            n,
            from: from - 1,
            to: to - 1,
        })
        .collect();
    (stacks_v, moves)
}

fn solve((stacks, moves): (Vec<Vec<char>>, Vec<Move>), rev: bool) -> usize {
    let mut stacks: Vec<Vec<_>> = stacks.iter().map(|s| s.iter().collect()).collect();
    for r#move in moves {
        let Move { n, from, to } = r#move;
        let len = stacks[from as usize].len();
        let top = &mut stacks[from as usize].split_off(len - n as usize);
        if rev {
            top.reverse();
        }
        stacks[to as usize].extend_from_slice(top);
    }
    stacks
        .iter()
        .map(|s| **s.last().unwrap() as usize)
        // .inspect(|i| print!("{}", *i as u8 as char))
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_test() {
        assert_eq!(
            inner(
                "    [D]    \n\
            [N] [C]    \n\
            [Z] [M] [P]\n\
             1   2   3 \n\
            \n\
            move 1 from 2 to 1\n\
            move 3 from 1 to 3\n\
            move 2 from 2 to 1\n\
            move 1 from 1 to 2"
            ),
            (
                b"CMZ".iter().map(|c| *c as usize).sum(),
                b"MCD".iter().map(|c| *c as usize).sum(),
            )
        );
    }

    #[test]
    fn test_my_input() {
        assert_eq!(
            run(),
            (
                b"LBLVVTVLP".iter().map(|c| *c as usize).sum(),
                b"TPFFBDRJD".iter().map(|c| *c as usize).sum(),
            )
        );
    }
}
