use std::{collections::HashMap, path::PathBuf};

const INPUT: &str = include_str!("../input/day07.txt");

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
struct Dir(PathBuf);

pub fn run() -> (usize, usize) {
    inner(INPUT)
}

fn inner(input: &str) -> (usize, usize) {
    let parsed = parse(input);
    let part1 = solve_part1(&parsed);
    let part2 = solve_part2(&parsed);
    (part1, part2)
}

fn parse(input: &str) -> HashMap<Dir, usize> {
    let mut fs = HashMap::new();
    let mut working_dir = PathBuf::from("/");
    for line in input.lines() {
        let mut words = line.split_ascii_whitespace();
        if line.bytes().next().unwrap() == b'$' {
            let command = words.nth(1).unwrap();
            if command == "cd" {
                let dir = words.next().unwrap();
                if dir == ".." {
                    working_dir.pop();
                } else {
                    working_dir.push(dir);
                }
                fs.entry(Dir(working_dir.to_owned())).or_insert(0);
            }
        } else {
            let size_or_dir = words.next().unwrap();
            if size_or_dir != "dir" {
                let size = size_or_dir.parse::<usize>().unwrap();
                for dir in working_dir.ancestors() {
                    fs.entry(Dir(dir.to_owned())).and_modify(|e| *e += size);
                }
            }
        }
    }
    fs
}

fn solve_part1(input: &HashMap<Dir, usize>) -> usize {
    input
        .iter()
        .filter(|(f, s)| matches!((f, s),(Dir(_), s) if **s < 100000))
        .map(|(_, s)| s)
        .sum()
}

fn solve_part2(input: &HashMap<Dir, usize>) -> usize {
    let req = input.get(&Dir(PathBuf::from("/"))).unwrap() - 40000000;
    input
        .iter()
        .filter(|(_, s)| **s > req)
        .min_by_key(|(_, s)| *s)
        .map(|(_, s)| *s)
        .unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_test() {
        assert_eq!(
            inner(
                "$ cd /\n\
                $ ls\n\
                dir a\n\
                14848514 b.txt\n\
                8504156 c.dat\n\
                dir d\n\
                $ cd a\n\
                $ ls\n\
                dir e\n\
                29116 f\n\
                2557 g\n\
                62596 h.lst\n\
                $ cd e\n\
                $ ls\n\
                584 i\n\
                $ cd ..\n\
                $ cd ..\n\
                $ cd d\n\
                $ ls\n\
                4060174 j\n\
                8033020 d.log\n\
                5626152 d.ext\n\
                7214296 k"
            ),
            (95437, 24933642)
        );
    }

    #[test]
    fn test_my_input() {
        assert_eq!(run(), (1350966, 6296435));
    }
}
