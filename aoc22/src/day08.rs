use std::{collections::HashSet, iter};

const INPUT: &str = include_str!("../input/day08.txt");

struct Forest {
    size: usize,
    field: Vec<u8>,
}

pub fn run() -> (usize, usize) {
    inner(INPUT)
}

fn inner(input: &str) -> (usize, usize) {
    let parsed = parse(input);
    let part1 = solve_part1(&parsed);
    let part2 = solve_part2(&parsed);
    (part1, part2)
}

fn parse(input: &str) -> Forest {
    let size = input.find('\n').unwrap();
    let field = input
        .bytes()
        .filter(|b| b.is_ascii_digit())
        .map(|b| b - b'0')
        .collect();
    Forest { size, field }
}

fn solve_part1(input: &Forest) -> usize {
    let Forest { size, field } = input;
    let size = *size;
    let mut visible = HashSet::new();
    for i in 0..size {
        for f in [
            (|i, j, size| i * size + j) as fn(usize, usize, usize) -> usize,
            |i, j, size| j * size + i,
            |i, j, size| (i + 1) * size - j - 1,
            |i, j, size| (size - j - 1) * size + i,
        ]
        .iter()
        {
            let mut tallest = None;
            for j in 0..size {
                if tallest.is_none() || field[f(i, j, size)] > tallest.unwrap() {
                    visible.insert(f(i, j, size));
                    tallest = Some(field[f(i, j, size)]);
                }
            }
        }
    }
    visible.len()
}

fn solve_part2(input: &Forest) -> usize {
    let Forest { size, field } = input;
    let size = *size;
    let mut best = 0;
    for i in 1..size {
        for j in 1..size - 1 {
            best = std::cmp::max(best, {
                let mut prod = 1;
                for f in [
                    Box::new((0..i).rev().zip(iter::repeat(j))) as Box<dyn Iterator<Item = _>>,
                    Box::new(iter::repeat(i).zip((0..j).rev())),
                    Box::new((i + 1..size).zip(iter::repeat(j))),
                    Box::new(iter::repeat(i).zip(j + 1..size)),
                ] {
                    let view = field[i * size + j];
                    let mut cnt = 0;
                    for (i, j) in f {
                        cnt += 1;
                        if field[i * size + j] >= view {
                            break;
                        }
                    }
                    prod *= cnt;
                }
                prod
            });
        }
    }
    best
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_test() {
        assert_eq!(
            inner(
                "30373\n\
                 25512\n\
                 65332\n\
                 33549\n\
                 35390"
            ),
            (21, 8)
        );
    }

    #[test]
    fn test_my_input() {
        assert_eq!(run(), (1840, 0));
    }
}
