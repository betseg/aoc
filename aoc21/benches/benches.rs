use aoc21::*;
use criterion::{black_box, criterion_group, criterion_main, Criterion};

fn criterion_benchmark(c: &mut Criterion) {
    c.bench_function("day01", |b| b.iter(|| black_box(day01::run())));
    c.bench_function("day02", |b| b.iter(|| black_box(day02::run())));
    c.bench_function("day03", |b| b.iter(|| black_box(day03::run())));
    c.bench_function("day04", |b| b.iter(|| black_box(day04::run())));
    c.bench_function("day05", |b| b.iter(|| black_box(day05::run())));
    c.bench_function("day06", |b| b.iter(|| black_box(day06::run())));
    c.bench_function("day07", |b| b.iter(|| black_box(day07::run())));
    c.bench_function("day08", |b| b.iter(|| black_box(day08::run())));
    c.bench_function("day09", |b| b.iter(|| black_box(day09::run())));
    c.bench_function("day10", |b| b.iter(|| black_box(day10::run())));
    c.bench_function("day11", |b| b.iter(|| black_box(day11::run())));
    c.bench_function("day12", |b| b.iter(|| black_box(day12::run())));
    c.bench_function("day13", |b| b.iter(|| black_box(day13::run())));
    c.bench_function("day14", |b| b.iter(|| black_box(day14::run())));
    c.bench_function("day15", |b| b.iter(|| black_box(day15::run())));
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
