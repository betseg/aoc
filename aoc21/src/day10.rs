const INPUT: &str = include_str!("../input/day10.txt");

pub fn run() -> (usize, usize) {
    inner(INPUT)
}

fn inner(input: &'static str) -> (usize, usize) {
    let part1 = solve_part1(input);
    let part2 = solve_part2(input);
    (part1, part2)
}

fn solve_part1(input: &str) -> usize {
    input
        .lines()
        .filter_map(|l| first_invalid_or_rest(l).ok())
        .map(|v| match v {
            b')' => 3,
            b']' => 57,
            b'}' => 1197,
            b'>' => 25137,
            _ => unreachable!(),
        })
        .sum()
}

fn solve_part2(input: &str) -> usize {
    let mut vals = input
        .lines()
        .filter_map(|l| first_invalid_or_rest(l).err())
        .map(|v| {
            v.iter().rev().fold(0, |acc, i| {
                acc * 5
                    + match i {
                        b'(' => 1,
                        b'[' => 2,
                        b'{' => 3,
                        b'<' => 4,
                        _ => unreachable!(),
                    }
            })
        })
        .collect::<Vec<_>>();
    let len = vals.len();
    *vals.select_nth_unstable(len / 2).1
}

fn first_invalid_or_rest(input: &str) -> Result<u8, Vec<u8>> {
    let mut chars = Vec::<u8>::new();
    for char in input.bytes() {
        if chars.is_empty() {
            chars.push(char);
        } else {
            let last = *chars.last().unwrap();
            match char {
                b')' if last == b'(' => {
                    chars.pop();
                }
                b']' if last == b'[' => {
                    chars.pop();
                }
                b'}' if last == b'{' => {
                    chars.pop();
                }
                b'>' if last == b'<' => {
                    chars.pop();
                }
                b'(' | b'[' | b'{' | b'<' => chars.push(char),
                _ => {
                    return Ok(char);
                }
            };
        }
    }
    Err(chars)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_test() {
        assert_eq!(
            inner(include_str!("../input/day10-test.txt")),
            (26397, 288957)
        );
    }

    #[test]
    fn test_my_input() {
        assert_eq!(run(), (436497, 2377613374));
    }
}
