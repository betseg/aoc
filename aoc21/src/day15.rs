use if_chain::if_chain;
use pathfinding::prelude::dijkstra;

const INPUT: &str = include_str!("../input/day15.txt");

pub fn run() -> (usize, usize) {
    inner(INPUT)
}

fn inner(input: &str) -> (usize, usize) {
    let parsed = parse(input);
    let part1 = solve_part1(&parsed);
    let part2 = solve_part2(&parsed);
    (part1, part2)
}

fn parse(input: &str) -> Vec<Vec<usize>> {
    let mut vec = Vec::new();
    for l in input.lines() {
        let mut inner = Vec::new();
        for c in l.bytes() {
            inner.push((c - b'0') as usize);
        }
        vec.push(inner)
    }
    vec
}

fn solve_part1(input: &[Vec<usize>]) -> usize {
    dijkstra(
        &(0, 0),
        |p| succ(input, p),
        |&p| p == (input.len() - 1, input[0].len() - 1),
    )
    .unwrap()
    .1
}

fn solve_part2(input: &[Vec<usize>]) -> usize {
    let mut new = Vec::new();
    for i in 0..5 {
        for line in input {
            let mut newline = Vec::new();
            for j in 0..5 {
                for e in line {
                    newline.push((*e + i + j - 1) % 9 + 1);
                }
            }
            new.push(newline);
        }
    }
    solve_part1(&new)
}

fn succ(points: &[Vec<usize>], (i, j): &(usize, usize)) -> Vec<((usize, usize), usize)> {
    let mut vec = Vec::new();
    for (di, dj) in [(0, 1), (1, 0), (1, 2), (2, 1)] {
        if_chain! {
            if let Some(di) = (i + di).checked_sub(1);
            if let Some(dj) = (j + dj).checked_sub(1);
            if let Some(ix) = points.get(di);
            if let Some(_) = ix.get(dj);
            then {
                vec.push(((di, dj), points[di][dj]));
            }
        }
    }
    vec
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_test() {
        assert_eq!(inner(include_str!("../input/day15-test.txt")), (40, 315));
    }

    #[test]
    fn test_my_input() {
        assert_eq!(run(), (393, 2823));
    }
}
