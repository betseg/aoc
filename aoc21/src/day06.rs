use std::collections::VecDeque;

const INPUT: &str = include_str!("../input/day06.txt");

pub fn run() -> (usize, usize) {
    inner(INPUT)
}

fn inner(input: &str) -> (usize, usize) {
    let parsed = parse(input);
    let part1 = offsprings(&parsed, 80);
    let part2 = offsprings(&parsed, 256);
    (part1, part2)
}

fn parse(input: &str) -> VecDeque<usize> {
    input
        .split(',')
        .map(str::parse)
        .map(Result::unwrap)
        .collect()
}

fn offsprings(parents: &VecDeque<usize>, length: usize) -> usize {
    let mut ages = [0; 9];
    for parent in parents {
        ages[*parent] += 1;
    }
    for _ in 0..length {
        let births = ages[0];
        ages.rotate_left(1);
        ages[6] += births;
    }
    ages.iter().sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_test() {
        assert_eq!(
            inner(include_str!("../input/day06-test.txt")),
            (5934, 26984457539)
        );
    }

    #[test]
    fn test_my_input() {
        assert_eq!(run(), (375482, 1689540415957));
    }
}
