use std::collections::HashMap;

const INPUT: &str = include_str!("../input/day12.txt");

#[derive(PartialEq, Eq, Hash, Clone, Copy, Debug)]
enum Node {
    Start,
    End,
    Small(&'static str),
    Big(&'static str),
}

#[derive(Debug, Default, Clone)]
struct Graph(HashMap<Node, Vec<Node>>);

pub fn run() -> (usize, usize) {
    inner(INPUT)
}

fn inner(input: &'static str) -> (usize, usize) {
    let parsed = parse(input);
    let part1 = solve_part1(&parsed);
    let part2 = solve_part2(&parsed);
    (part1, part2)
}

fn parse(input: &'static str) -> Graph {
    let mut graph: Graph = Default::default();
    for l in input.lines() {
        let (from, to) = l.split_once('-').unwrap();
        let from = str_to_node(from);
        let to = str_to_node(to);
        graph.0.entry(from).or_default().push(to);
        graph.0.entry(to).or_default().push(from);
    }
    graph
}

fn str_to_node(node: &'static str) -> Node {
    match node {
        "start" => Node::Start,
        "end" => Node::End,
        str if str.chars().next().unwrap().is_lowercase() => Node::Small(str),
        str => Node::Big(str),
    }
}

fn solve_part1(input: &Graph) -> usize {
    input.travel(Node::Start, Vec::new(), 0, true)
}

fn solve_part2(input: &Graph) -> usize {
    input.travel(Node::Start, Vec::new(), 0, false)
}

impl Graph {
    fn travel(&self, start: Node, visited: Vec<Node>, ans: usize, visited_twice: bool) -> usize {
        if start == Node::End {
            return ans + 1;
        }
        let mut visited_twice = visited_twice;
        if !matches!(start, Node::Big(_)) {
            if start == Node::Start && !visited.is_empty()
                || visited_twice && visited.contains(&start)
            {
                return ans;
            } else if visited.contains(&start) {
                visited_twice = true;
            }
        }

        let mut visited = visited;
        visited.push(start);
        ans + self
            .0
            .get(&start)
            .unwrap()
            .iter()
            .map(|n| self.travel(*n, visited.clone(), ans, visited_twice))
            .sum::<usize>()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn smol_test() {
        assert_eq!(
            inner(
                "start-A\n\
                 start-b\n\
                 A-c\n\
                 A-b\n\
                 b-d\n\
                 A-end\n\
                 b-end"
            ),
            (10, 36)
        );
    }

    #[test]
    fn med_test() {
        assert_eq!(
            inner(
                "dc-end\n\
                 HN-start\n\
                 start-kj\n\
                 dc-start\n\
                 dc-HN\n\
                 LN-dc\n\
                 HN-end\n\
                 kj-sa\n\
                 kj-HN\n\
                 kj-dc"
            ),
            (19, 103)
        );
    }

    #[test]
    fn larg_test() {
        assert_eq!(
            inner(
                "fs-end\n\
                 he-DX\n\
                 fs-he\n\
                 start-DX\n\
                 pj-DX\n\
                 end-zg\n\
                 zg-sl\n\
                 zg-pj\n\
                 pj-he\n\
                 RW-he\n\
                 fs-DX\n\
                 pj-RW\n\
                 zg-RW\n\
                 start-pj\n\
                 he-WI\n\
                 zg-he\n\
                 pj-fs\n\
                 start-RW"
            ),
            (226, 3509)
        );
    }

    #[test]
    fn test_my_input() {
        assert_eq!(run(), (4754, 143562));
    }
}
