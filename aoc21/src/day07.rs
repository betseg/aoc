use itertools::Itertools;

const INPUT: &str = include_str!("../input/day07.txt");

pub fn run() -> (usize, usize) {
    inner(INPUT)
}

fn inner(input: &str) -> (usize, usize) {
    let parsed = parse(input);
    let part1 = solve_part1(&parsed);
    let part2 = solve_part2(&parsed);
    (part1, part2)
}

fn parse(input: &str) -> Vec<usize> {
    input
        .split(',')
        .map(str::parse)
        .map(Result::unwrap)
        .sorted()
        .collect()
}

fn solve_part1(input: &[usize]) -> usize {
    let median = input[input.len() / 2];
    input.iter().map(|x| x.abs_diff(median)).sum()
}

fn solve_part2(input: &[usize]) -> usize {
    let max = *input.iter().max().unwrap();
    (0..max)
        .map(|x| {
            input
                .iter()
                .map(|c| {
                    let d = c.abs_diff(x);
                    (d * (d + 1)) / 2
                })
                .sum()
        })
        .min()
        .unwrap()
}

#[allow(dead_code)]
/// FIXME: TODO: PRODUCES WRONG RESULT
fn solve_part2_wrong(input: &[usize]) -> usize {
    let mean = input.iter().sum::<usize>() / input.len();
    dbg!(mean);
    input
        .iter()
        .map(|x| {
            let d = x.abs_diff(mean);
            (d * (d + 1)) / 2
        })
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_test() {
        assert_eq!(inner(include_str!("../input/day07-test.txt")), (37, 168));
    }

    #[test]
    fn test_my_input() {
        assert_eq!(run(), (349357, 96708205));
    }
}
