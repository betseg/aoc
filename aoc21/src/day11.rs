use if_chain::if_chain;
use itertools::Itertools;
use std::ops::{Index, IndexMut};

const INPUT: &str = include_str!("../input/day11.txt");

#[derive(Clone)]
struct Field(Vec<Vec<u32>>);

pub fn run() -> (usize, usize) {
    inner(INPUT)
}

fn inner(input: &'static str) -> (usize, usize) {
    let parsed = parse(input);
    let part1 = solve_part1(parsed.clone());
    let part2 = solve_part2(parsed);
    (part1, part2)
}

fn parse(input: &str) -> Field {
    Field(
        input
            .lines()
            .map(|l| l.chars().map(|b| b.to_digit(10).unwrap()).collect())
            .collect(),
    )
}

fn solve_part1(mut input: Field) -> usize {
    let mut flashes = 0;
    for _ in 0..100 {
        flashes += input.step();
    }
    flashes
}

fn solve_part2(mut input: Field) -> usize {
    for i in 1.. {
        if input.step() == input.0.iter().map(|l| l.len()).sum::<usize>() {
            return i;
        }
    }
    unreachable!()
}

impl Field {
    fn step(&mut self) -> usize {
        for i in 0..self.0.len() {
            for j in 0..self[0].len() {
                self[i][j] += 1;
            }
        }
        for i in 0..self.0.len() {
            for j in 0..self[0].len() {
                if self[i][j] > 9 {
                    self.flash(i, j);
                }
            }
        }
        self.0
            .iter()
            .map(|l| l.iter().filter(|&c| *c == 0).count())
            .sum()
    }

    fn flash(&mut self, i: usize, j: usize) {
        self[i][j] = 0;
        for (ni, nj) in (0..=2).cartesian_product(0..=2) {
            if_chain! {
                if ni != 1 || nj != 1;
                if let Some(di) = (i + ni).checked_sub(1);
                if let Some(dj) = (j + nj).checked_sub(1);
                if di < self.0.len();
                if dj < self.0[0].len();
                if self[di][dj] != 0;
                then {
                    self[di][dj] += 1;
                    if self[di][dj] > 9 {
                        self.flash(di, dj);
                    }
               }
            }
        }
    }
}

impl Index<usize> for Field {
    type Output = Vec<u32>;

    fn index(&self, index: usize) -> &Self::Output {
        &self.0[index]
    }
}

impl IndexMut<usize> for Field {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        &mut self.0[index]
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_test() {
        assert_eq!(inner(include_str!("../input/day11-test.txt")), (1656, 195));
    }

    #[test]
    fn test_my_input() {
        assert_eq!(run(), (1697, 344));
    }
}
