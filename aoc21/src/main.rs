#![feature(extract_if)]
#![feature(hash_extract_if)]

use structopt::StructOpt;

#[derive(StructOpt)]
struct Opt {
    #[structopt(short, long)]
    day: Option<usize>,
}

mod day01;
mod day02;
mod day03;
mod day04;
mod day05;
mod day06;
mod day07;
mod day08;
mod day09;
mod day10;
mod day11;
mod day12;
mod day13;
mod day14;
mod day15;

const SOLUTIONS: [&dyn Fn() -> (usize, usize); 15] = [
    &day01::run,
    &day02::run,
    &day03::run,
    &day04::run,
    &day05::run,
    &day06::run,
    &day07::run,
    &day08::run,
    &day09::run,
    &day10::run,
    &day11::run,
    &day12::run,
    &day13::run,
    &day14::run,
    &day15::run,
];

fn main() {
    if let Some(day) = Opt::from_args().day {
        println!("Day {:02}: {:?}", day, SOLUTIONS[day - 1]());
    } else {
        for (i, f) in SOLUTIONS.iter().enumerate() {
            println!("Day {:02}: {:?}", i + 1, f());
        }
    }
}
