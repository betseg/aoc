use std::ops::{Index, IndexMut};

const INPUT: &str = include_str!("../input/day04.txt");

#[derive(Default, Debug, Clone, Copy)]
struct Cell {
    marked: bool,
    value: usize,
}

#[derive(Debug, Clone, Copy)]
struct Board {
    board: [[Cell; 5]; 5],
}

pub fn run() -> (usize, usize) {
    inner(INPUT)
}

fn inner(input: &str) -> (usize, usize) {
    let (numbers, boards) = parse(input);
    let part1 = solve_part1(&numbers, &boards);
    let part2 = solve_part2(&numbers, &boards);
    (part1, part2)
}

fn parse(input: &str) -> (Vec<usize>, Vec<Board>) {
    let (numbers, boards) = input.split_once("\n\n").unwrap();
    let numbers = numbers
        .split(',')
        .map(str::parse::<usize>)
        .map(Result::unwrap)
        .collect();
    let boards = boards.split("\n\n").map(Board::from_str).collect();
    (numbers, boards)
}

fn solve_part1(numbers: &[usize], boards: &[Board]) -> usize {
    let mut boards = boards.to_owned();
    for i in numbers {
        for board in boards.iter_mut() {
            board.mark(*i);
            if board.wins() {
                return board.sum_of_unmarked() * i;
            }
        }
    }
    unreachable!()
}

fn solve_part2(numbers: &[usize], boards: &[Board]) -> usize {
    let mut boards = boards.to_owned();
    for i in numbers {
        for board in boards.iter_mut() {
            board.mark(*i);
        }
        boards.extract_if(|b| b.wins());
        if boards.len() == 1 {
            break;
        }
    }
    solve_part1(numbers, &boards)
}

impl Board {
    fn from_str(input: &str) -> Self {
        let mut arr = [Default::default(); 25];
        for (i, v) in input.split_ascii_whitespace().enumerate() {
            arr[i] = Cell::new(false, v.parse().unwrap());
        }
        Self {
            board: unsafe { std::mem::transmute::<[Cell; 25], [[Cell; 5]; 5]>(arr) },
        }
    }

    fn wins(&self) -> bool {
        #[allow(clippy::never_loop)]
        'outer_hor: for i in 0..5 {
            for j in 0..5 {
                if !self[i][j].is_marked() {
                    continue 'outer_hor;
                }
            }
            return true;
        }
        #[allow(clippy::never_loop)]
        'outer_vert: for i in 0..5 {
            for j in 0..5 {
                if !self[j][i].is_marked() {
                    continue 'outer_vert;
                }
            }
            return true;
        }
        false
    }

    fn sum_of_unmarked(&self) -> usize {
        let mut i = 0;
        for x in 0..5 {
            for y in 0..5 {
                if !self[x][y].is_marked() {
                    i += self[x][y].value();
                }
            }
        }
        i
    }

    fn mark(&mut self, number: usize) {
        for i in 0..5 {
            for j in 0..5 {
                if self[i][j].value == number {
                    self[i][j].mark()
                }
            }
        }
    }
}

impl Cell {
    fn new(marked: bool, value: usize) -> Self {
        Self { marked, value }
    }

    fn is_marked(&self) -> bool {
        self.marked
    }

    fn value(&self) -> usize {
        self.value
    }

    fn mark(&mut self) {
        self.marked = true;
    }
}

impl Index<usize> for Board {
    type Output = [Cell; 5];
    fn index(&self, ix: usize) -> &Self::Output {
        &self.board[ix]
    }
}

impl IndexMut<usize> for Board {
    fn index_mut(&mut self, ix: usize) -> &mut Self::Output {
        &mut self.board[ix]
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_test() {
        assert_eq!(inner(include_str!("../input/day04-test.txt")), (4512, 1924));
    }

    #[test]
    fn test_my_input() {
        assert_eq!(run(), (25410, 2730));
    }
}
