const INPUT: &str = include_str!("../input/day01.txt");

pub fn run() -> (usize, usize) {
    inner(INPUT)
}

fn inner(input: &str) -> (usize, usize) {
    let parsed = parse(input);
    let part1 = solve(&parsed, 2);
    let part2 = solve(&parsed, 4);
    (part1, part2)
}

fn parse(input: &str) -> Vec<i32> {
    input
        .lines()
        .map(str::parse::<i32>)
        .map(Result::unwrap)
        .collect()
}

fn solve(input: &[i32], window_size: usize) -> usize {
    input
        .windows(window_size)
        .filter(|w| w.last() > w.first())
        .count()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_test() {
        assert_eq!(inner(include_str!("../input/day01-test.txt")), (7, 5));
    }

    #[test]
    fn test_my_input() {
        assert_eq!(run(), (1448, 1471));
    }
}
