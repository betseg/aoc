use std::cmp::Ordering;
use std::collections::HashSet;

const INPUT: &str = include_str!("../input/day03.txt");

pub fn run() -> (usize, usize) {
    inner(INPUT)
}

fn inner(input: &str) -> (usize, usize) {
    let parsed = parse(input);
    let width = input.split_once('\n').unwrap().0.len();
    let part1 = solve_part1(&parsed, width);
    let part2 = solve_part2(&parsed, width);
    (part1, part2)
}

fn parse(input: &str) -> Vec<usize> {
    input
        .lines()
        .map(|l| usize::from_str_radix(l, 2).unwrap())
        .collect()
}

fn nth_bit(num: usize, bit: usize) -> usize {
    num >> bit & 1
}

fn solve_part1(input: &[usize], width: usize) -> usize {
    let gamma = (0..width)
        .filter(|&i| {
            let gc = input.iter().filter(|&n| nth_bit(*n, i) == 1).count();
            gc > input.len() / 2
        })
        .map(|i| 1 << i)
        .sum::<usize>();
    let size = std::mem::size_of::<usize>() * 8;
    gamma * (!gamma << (size - width) >> (size - width))
}

fn solve_part2(input: &[usize], width: usize) -> usize {
    let set = input.iter().copied().collect::<HashSet<_>>();
    filter(&set, Ordering::Greater, 1, width) * filter(&set, Ordering::Less, 0, width)
}

fn filter(set: &HashSet<usize>, freq: Ordering, tb: usize, width: usize) -> usize {
    let mut set = set.clone();
    for b in (0..width).rev() {
        let ones = set.iter().filter(|&n| nth_bit(*n, b) == 1).count();
        let zeroes = set.len() - ones;
        let filter = if ones == zeroes {
            tb
        } else if ones.cmp(&zeroes) == freq {
            1
        } else {
            0
        };

        set.extract_if(|n| nth_bit(*n, b) == filter);

        if set.len() == 1 {
            break;
        }
    }
    *set.iter().next().unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_test() {
        assert_eq!(inner(include_str!("../input/day03-test.txt")), (198, 230));
    }

    #[test]
    fn test_my_input() {
        assert_eq!(run(), (3687446, 4406844));
    }
}
