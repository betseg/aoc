const INPUT: &str = include_str!("../input/day02.txt");

pub fn run() -> (usize, usize) {
    inner(INPUT)
}

fn inner(input: &str) -> (usize, usize) {
    parse_and_solve(input)
}

fn parse_and_solve(input: &str) -> (usize, usize) {
    let (a, b, c) = input.lines().fold((0, 0, 0), |(a, b, c), m| {
        let m = m.split_once(' ').unwrap();
        let v = m.1.parse::<usize>().unwrap();
        match m.0 {
            "forward" => (a + v, b + c * v, c),
            "down" => (a, b, c + v),
            "up" => (a, b, c - v),
            _ => unreachable!(),
        }
    });
    (a * c, a * b)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_test() {
        assert_eq!(inner(include_str!("../input/day02-test.txt")), (150, 900));
    }

    #[test]
    fn test_my_input() {
        assert_eq!(run(), (1698735, 1594785890));
    }
}
