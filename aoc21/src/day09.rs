use if_chain::if_chain;
use pathfinding::prelude::bfs_reach;

const INPUT: &str = include_str!("../input/day09.txt");

pub fn run() -> (usize, usize) {
    inner(INPUT)
}

fn inner(input: &'static str) -> (usize, usize) {
    let parsed = parse(input);
    let part1_list = solve_part1(&parsed);
    let part1 = part1_list
        .iter()
        .map(|(i, j)| (parsed[*i][*j] + 1) as usize)
        .sum();
    let part2 = solve_part2(&parsed, &part1_list);
    (part1, part2)
}

fn parse(input: &str) -> Vec<Vec<u32>> {
    input
        .lines()
        .map(|l| l.chars().map(|c| c.to_digit(10).unwrap()).collect())
        .collect()
}

fn solve_part1(input: &[Vec<u32>]) -> Vec<(usize, usize)> {
    let mut total = Vec::new();
    for i in 0..input.len() {
        'next: for j in 0..input[0].len() {
            for (di, dj) in [(0, 1), (1, 0), (1, 2), (2, 1)] {
                if_chain! {
                    if let Some(di) = (i + di).checked_sub(1);
                    if let Some(dj) = (j + dj).checked_sub(1);
                    if let Some(ix) = input.get(di);
                    if let Some(jx) = ix.get(dj);
                    if input[i][j] >= *jx;
                    then {
                        continue 'next;
                    }
                }
            }
            total.push((i, j));
        }
    }
    total
}

fn solve_part2(input: &[Vec<u32>], lows: &[(usize, usize)]) -> usize {
    let mut vec = Vec::new();
    for low in lows {
        vec.push(bfs_reach(*low, |&low| succ(input, low)).count());
    }
    vec.sort_unstable_by(|a, b| b.cmp(a));
    vec[0] * vec[1] * vec[2]
}

fn succ(points: &[Vec<u32>], (i, j): (usize, usize)) -> Vec<(usize, usize)> {
    let mut vec = Vec::new();
    for (di, dj) in [(0, 1), (1, 0), (1, 2), (2, 1)] {
        if_chain! {
            if let Some(di) = (i + di).checked_sub(1);
            if let Some(dj) = (j + dj).checked_sub(1);
            if let Some(ix) = points.get(di);
            if let Some(jx) = ix.get(dj);
            if *jx != 9;
            then {
                vec.push((di, dj));
            }
        }
    }
    vec
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_test() {
        assert_eq!(inner(include_str!("../input/day09-test.txt")), (15, 1134));
    }

    #[test]
    fn test_my_input() {
        assert_eq!(run(), (494, 1048128));
    }
}
