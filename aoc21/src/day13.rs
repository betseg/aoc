const INPUT: &str = include_str!("../input/day13.txt");

#[derive(Clone, Copy, Debug)]
enum Fold {
    X(usize),
    Y(usize),
}

pub fn run() -> (usize, usize) {
    inner(INPUT)
}

fn inner(input: &'static str) -> (usize, usize) {
    let (points, folds) = parse(input);
    let part1 = solve(points.clone(), vec![folds[0]]);
    let part2 = solve(points, folds);
    (part1, part2)
}

fn parse(input: &'static str) -> (Vec<(usize, usize)>, Vec<Fold>) {
    let (points, folds) = input.split_once("\n\n").unwrap();
    let mut pvec = Vec::new();
    for (x, y) in points.lines().map(|l| l.split_once(',').unwrap()) {
        pvec.push((x.parse().unwrap(), y.parse().unwrap()));
    }
    let mut fvec = Vec::new();
    for (axis, amount) in folds.lines().map(|l| l.split_once('=').unwrap()) {
        match (axis.bytes().last().unwrap(), amount.parse()) {
            (b'x', Ok(amount)) => fvec.push(Fold::X(amount)),
            (b'y', Ok(amount)) => fvec.push(Fold::Y(amount)),
            _ => unreachable!(),
        }
    }
    (pvec, fvec)
}

fn solve(mut points: Vec<(usize, usize)>, folds: Vec<Fold>) -> usize {
    for fold in &folds {
        for point in points.iter_mut() {
            match fold {
                Fold::X(amount) => {
                    if point.0 > *amount {
                        point.0 -= (point.0 - amount) * 2;
                    }
                }
                Fold::Y(amount) => {
                    if point.1 > *amount {
                        point.1 -= (point.1 - amount) * 2;
                    }
                }
            }
        }
        points.sort_unstable();
        points.dedup();
    }
    if folds.len() != 1 {
        print(&points);
    }
    points.len()
}

fn print(points: &[(usize, usize)]) {
    let xw = points.iter().max_by_key(|p| p.0).unwrap().0;
    let yw = points.iter().max_by_key(|p| p.1).unwrap().1;
    let mut grid: Vec<Vec<bool>> = vec![vec![false; xw + 1]; yw + 1];
    for point in points {
        grid[point.1][point.0] = true;
    }
    for y in grid {
        for x in y {
            print!("{}", if x { '#' } else { '.' });
        }
        println!();
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_test() {
        assert_eq!(inner(include_str!("../input/day13-test.txt")).0, 17);
    }

    #[test]
    fn test_my_input() {
        assert_eq!(run().0, 847);
    }
}
