use itertools::Itertools;

const INPUT: &str = include_str!("../input/day14.txt");

type Polymer = [[usize; 26]; 26];
type Rules = [[u8; 26]; 26];

pub fn run() -> (usize, usize) {
    inner(INPUT)
}

fn inner(input: &str) -> (usize, usize) {
    let parsed = parse(input);
    let part1 = solve(&parsed, 10);
    let part2 = solve(&parsed, 40);
    (part1, part2)
}

fn parse(input: &str) -> (Polymer, Rules, u8) {
    let (template, rules) = input.split_once("\n\n").unwrap();

    let first = template.as_bytes()[0];

    let template = {
        let mut temp = [[0; 26]; 26];
        for (f, s) in template.bytes().tuple_windows() {
            temp[char_to_ix(s)][char_to_ix(f)] += 1;
        }
        temp
    };

    let rules = {
        let mut rulesmap = [[0; 26]; 26];
        for (p, b) in rules.lines().map(|l| l.split_once(" -> ").unwrap()) {
            let (f, s) = p.bytes().tuple_windows().next().unwrap();
            rulesmap[char_to_ix(s)][char_to_ix(f)] = char_to_ix(b.as_bytes()[0]) as u8;
        }
        rulesmap
    };

    (template, rules, first)
}

fn char_to_ix(c: u8) -> usize {
    (c - b'A') as usize
}

fn solve((polymer, rules, first): &(Polymer, Rules, u8), steps: usize) -> usize {
    let mut polymer = *polymer;

    for _ in 0..steps {
        let mut next = [[0; 26]; 26];
        for i in 0..26 {
            for j in 0..26 {
                let insert = rules[j][i] as usize;
                let cnt = polymer[j][i];
                if insert != 0 {
                    next[insert][i] += cnt;
                    next[j][insert] += cnt;
                } else {
                    next[i][j] += cnt;
                }
            }
        }
        polymer = next;
    }

    let mut cnts = [0; 26];

    cnts[char_to_ix(*first)] = 1;

    for j in 0..26 {
        cnts[j] += polymer[j].iter().sum::<usize>();
    }

    match cnts.iter().filter(|&&n| n != 0).minmax() {
        itertools::MinMaxResult::MinMax(n, x) => x - n,
        _ => unreachable!(),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_test() {
        assert_eq!(
            inner(include_str!("../input/day14-test.txt")),
            (1588, 2188189693529)
        );
    }

    #[test]
    fn test_my_input() {
        assert_eq!(run(), (2549, 2516901104210));
    }
}
