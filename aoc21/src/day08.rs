use std::collections::{HashMap, HashSet};

const INPUT: &str = include_str!("../input/day08.txt");

pub fn run() -> (usize, usize) {
    inner(INPUT)
}

fn inner(input: &'static str) -> (usize, usize) {
    let parsed = parse(input);
    let part1 = solve_part1(&parsed);
    let part2 = solve_part2(&parsed);
    (part1, part2)
}

fn parse(input: &str) -> Vec<(&str, &str)> {
    input
        .lines()
        .map(|l| l.split_once(" | ").unwrap())
        .collect()
}

fn solve_part1(input: &[(&str, &str)]) -> usize {
    input
        .iter()
        .map(|e| {
            e.1.split_ascii_whitespace()
                .filter(|v| matches!(v.len(), 2 | 3 | 4 | 7))
                .count()
        })
        .sum()
}

fn solve_part2(input: &[(&str, &str)]) -> usize {
    input
        .iter()
        .map(|(pats, vals)| solve_pats(pats, vals))
        .sum()
}

fn solve_pats(pats: &str, vals: &str) -> usize {
    let map = pats
        .split_ascii_whitespace()
        .map(|p| (p.len(), p.chars().collect::<HashSet<_>>()))
        .collect::<HashMap<_, _>>();
    let mut res = 0;
    for v in vals.split_ascii_whitespace() {
        let v = v.chars().collect::<HashSet<_>>();
        res *= 10;
        match (v.len(), (&v & &map[&4]).len(), (&v & &map[&2]).len()) {
            (2, _, _) => res += 1,
            (3, _, _) => res += 7,
            (4, _, _) => res += 4,
            (7, _, _) => res += 8,
            (5, 2, _) => res += 2,
            (5, 3, 1) => res += 5,
            (5, 3, 2) => res += 3,
            (6, 3, 1) => res += 6,
            (6, 3, 2) => res += 0, // does it even matter
            (6, 4, _) => res += 9,
            _ => unreachable!(),
        }
    }
    res
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_test() {
        assert_eq!(inner(include_str!("../input/day08-test.txt")), (26, 61229));
    }

    #[test]
    fn shorter_test() {
        assert_eq!(inner("acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf"), (0, 5353));
    }

    #[test]
    fn test_my_input() {
        assert_eq!(run(), (245, 983026));
    }
}
