use scan_fmt::scan_fmt;
use std::collections::HashMap;

const INPUT: &str = include_str!("../input/day05.txt");

#[derive(PartialEq, Eq, Hash, Clone, Copy, Debug)]
struct Point(isize, isize);

#[derive(PartialEq, Eq, Hash, Clone, Copy, Debug)]
struct Line {
    start: Point,
    end: Point,
}

#[derive(Clone, Copy)]
enum Dir {
    Ne,
    Se,
    Sw,
    Nw,
}

pub fn run() -> (usize, usize) {
    inner(INPUT)
}

fn inner(input: &str) -> (usize, usize) {
    let parsed = parse(input);
    let part1 = solve_part1(&parsed);
    let part2 = solve_part2(&parsed);
    (part1, part2)
}

fn parse(input: &str) -> Vec<Line> {
    input
        .lines()
        .map(|l| {
            let (x1, y1, x2, y2) =
                scan_fmt!(l, "{},{} -> {},{}", isize, isize, isize, isize).unwrap();
            Line::new(x1, y1, x2, y2)
        })
        .collect()
}

fn solve_part1(input: &[Line]) -> usize {
    let mut map = HashMap::new();
    for line in input {
        if line.is_linear() {
            let (axis, fixed, iter) = if line.start.0 == line.end.0 {
                (
                    true,
                    line.start.0,
                    if line.start.1 < line.end.1 {
                        line.start.1..=line.end.1
                    } else {
                        line.end.1..=line.start.1
                    },
                )
            } else if line.start.1 == line.end.1 {
                (
                    false,
                    line.start.1,
                    if line.start.0 < line.end.0 {
                        line.start.0..=line.end.0
                    } else {
                        line.end.0..=line.start.0
                    },
                )
            } else {
                continue;
            };
            for point in iter {
                *map.entry(if axis {
                    Point::new(fixed, point)
                } else {
                    Point::new(point, fixed)
                })
                .or_insert(0) += 1;
            }
        }
    }
    map.iter().filter(|(_, &n)| n > 1).count()
}

fn solve_part2(input: &[Line]) -> usize {
    let mut map = HashMap::new();
    for line in input {
        if line.is_linear() {
            let (axis, fixed, iter) = if line.start.0 == line.end.0 {
                (
                    true,
                    line.start.0,
                    if line.start.1 < line.end.1 {
                        line.start.1..=line.end.1
                    } else {
                        line.end.1..=line.start.1
                    },
                )
            } else if line.start.1 == line.end.1 {
                (
                    false,
                    line.start.1,
                    if line.start.0 < line.end.0 {
                        line.start.0..=line.end.0
                    } else {
                        line.end.0..=line.start.0
                    },
                )
            } else {
                continue;
            };
            for point in iter {
                *map.entry(if axis {
                    Point::new(fixed, point)
                } else {
                    Point::new(point, fixed)
                })
                .or_insert(0) += 1;
            }
        } else if line.is_diagonal() {
            let diff = (line.start.0 - line.end.0).abs();
            let dir = if line.start.0 > line.end.0 {
                if line.start.1 > line.end.1 {
                    Dir::Sw
                } else {
                    Dir::Nw
                }
            } else if line.start.1 > line.end.1 {
                Dir::Se
            } else {
                Dir::Ne
            };
            let mut point = line.start;
            for _ in 0..=diff {
                *map.entry(point).or_insert(0) += 1;
                point = point.increment(dir);
            }
        }
    }
    map.iter().filter(|(_, &n)| n > 1).count()
}

impl Line {
    fn new(x1: isize, y1: isize, x2: isize, y2: isize) -> Self {
        Self {
            start: Point::new(x1, y1),
            end: Point::new(x2, y2),
        }
    }

    fn is_linear(&self) -> bool {
        self.start.is_linear_with(&self.end)
    }

    fn is_diagonal(&self) -> bool {
        self.start.is_diagonal_with(&self.end)
    }
}

impl Point {
    fn new(x: isize, y: isize) -> Self {
        Self(x, y)
    }

    fn is_linear_with(&self, other: &Self) -> bool {
        self.0 == other.0 || self.1 == other.1
    }

    fn is_diagonal_with(&self, other: &Self) -> bool {
        let diff_x = (self.0 - other.0).abs();
        let diff_y = (self.1 - other.1).abs();
        diff_x == diff_y
    }

    fn increment(&self, dir: Dir) -> Self {
        match dir {
            Dir::Ne => Point::new(self.0 + 1, self.1 + 1),
            Dir::Se => Point::new(self.0 + 1, self.1 - 1),
            Dir::Sw => Point::new(self.0 - 1, self.1 - 1),
            Dir::Nw => Point::new(self.0 - 1, self.1 + 1),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_test() {
        assert_eq!(inner(include_str!("../input/day05-test.txt")), (5, 12));
    }

    #[test]
    fn test_my_input() {
        assert_eq!(run(), (6005, 23864));
    }
}
