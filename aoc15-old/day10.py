from itertools import groupby

seq = "1321131112"
p1 = ''

for i in range(50):
    res = ""
    for g,l in groupby(seq):
        l = list(l)
        res += str(len(l))
        res += str(l[0])
    if i == 39:
        p1 = res
    seq = res

print(len(p1), len(res))