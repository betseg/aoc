with open("inputs/day01.txt") as file:
    d = file.read()

n = 0
i = None
for x, c in enumerate(d):
    if c == '(':
        n += 1
    else:
        n -= 1
    if i is None and n == -1:
        i = x

print(n, i+1)