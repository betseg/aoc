from collections import defaultdict

with open("inputs/day07.txt") as file:
    conns = file.readlines()

circuit_orig = defaultdict(str)

for conn in conns:
    frm,to = conn.strip().split(' -> ')
    circuit_orig[to] = frm

def solve(wire):
    global circuit
    try:
        return int(wire)
    except:
        expr = circuit[wire]
        try:
            return int(expr)
        except:
            expr = expr.split()
    res = 0
    if len(expr) == 1:
        res = solve(expr[0])
    elif len(expr) == 2:
        res = ~solve(expr[1])
    else:
        l = solve(expr[0])
        r = solve(expr[2])
        match expr[1]:
            case 'AND':
                res = l & r
            case 'OR':
                res = l | r
            case 'LSHIFT':
                res = l << r
            case 'RSHIFT':
                res = l >> r
            case no:
                assert(False)
    circuit[wire] = res
    return res

circuit = circuit_orig.copy()
p1 = solve('a')
print(p1)
circuit = circuit_orig.copy()
circuit['b'] = p1
p2 = solve('a')
print(p2)
