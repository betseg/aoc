from collections import defaultdict
from itertools import permutations

graph = defaultdict(defaultdict)
locs = set()

with open("inputs/day09.txt") as file:
    for edge in file:
        edge = edge.split()
        locs.add(edge[0])
        locs.add(edge[2])
        graph[edge[0]][edge[2]]=int(edge[4])
        graph[edge[2]][edge[0]]=int(edge[4])

shortest = None
longest = None
for route in permutations(locs):
    dist = 0
    for i in range(len(route)-1):
        dist += graph[route[i]][route[i+1]]
    if shortest is None or shortest > dist:
        shortest = dist
    if longest is None or longest < dist:
        longest = dist

print(shortest, longest)
