with open("inputs/day02.txt") as file:
    sizes = file.readlines()

p = 0
r = 0
for size in sizes:
    size = [int(i) for i in size.split('x')]
    size.sort()
    s_p = size[0] * size[1]
    s_r = 2 * (size[0] + size[1])
    p += 2 * (size[0]*size[1]+size[1]*size[2]+size[0]*size[2]) + s_p
    r += s_r + size[0]*size[1]*size[2]

print(p,r)