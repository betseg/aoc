from collections import defaultdict

with open("inputs/day03.txt") as file:
    dirs = file.read()

visited = defaultdict(int)
visited[(0,0)]+=1
x,y = 0,0
for c in dirs:
    match c:
        case '<':
            x -= 1
        case '>':
            x += 1
        case '^':
            y += 1
        case 'v':
            y -= 1
    visited[(x,y)] += 1
print(len(visited))

visited = defaultdict(int)
visited[(0,0)]+=1
x_santa,y_santa = 0,0
x_robot,y_robot = 0,0
for i,c in enumerate(dirs):
    if i%2==0:
        match c:
            case '<':
                x_santa -= 1
            case '>':
                x_santa += 1
            case '^':
                y_santa += 1
            case 'v':
                y_santa -= 1
        visited[(x_santa,y_santa)] += 1
    else:
        match c:
            case '<':
                x_robot -= 1
            case '>':
                x_robot += 1
            case '^':
                y_robot += 1
            case 'v':
                y_robot -= 1
        visited[(x_robot,y_robot)] += 1
print(len(visited))