from collections import defaultdict

with open("inputs/day06.txt") as file:
    instrs = file.readlines()

p1 = defaultdict(bool)
p2 = defaultdict(bool)

for instr in instrs:
    instr = instr.split()
    if len(instr) == 5:
        instr = instr[1:]
    x1,y1 = [int(i) for i in instr[1].split(',')]
    x2,y2 = [int(i) for i in instr[3].split(',')]
    if instr[0] == "on":
        for x in range(x1,x2+1):
            for y in range(y1,y2+1):
                p1[(x,y)] = True
                p2[(x,y)] += 1
    elif instr[0] == "off":
        for x in range(x1,x2+1):
            for y in range(y1,y2+1):
                p1[(x,y)] = False
                if p2[(x,y)] > 0:
                    p2[(x,y)] -= 1
    else:
        for x in range(x1,x2+1):
            for y in range(y1,y2+1):
                p1[(x,y)] = not p1[(x,y)]
                p2[(x,y)] += 2
print(len([i for i in p1 if p1[i]]))
print(sum(p2.values()))