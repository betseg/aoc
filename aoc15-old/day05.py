from itertools import groupby

with open("inputs/day05.txt") as file:
    strs = file.readlines()

p1 = 0
for nice in strs:
    not_nice = False
    for sub in ['ab','cd','pq','xy']:
        if sub in nice:
            not_nice = True
    if not_nice:
        continue
    v = 0
    for c in nice:
        if c in ['a','e','i','o','u']:
            v += 1
    if v < 3:
        continue
    if len(nice) > len([i[0] for i in groupby(nice)]):
        p1 += 1

p2 = 0
for nice in strs:
    found = False
    for i in range(len(nice)-3):
        if nice[i:i+2] in nice[i+2:]:
            found = True
    if not found:
        continue

    passes = False
    for sub in [nice[i:i+3] for i in range(len(nice)-3)]:
        if not passes and sub[0] == sub[2]:
            passes = True
    if passes:
        p2 += 1

print(p1, p2)