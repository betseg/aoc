import json

p1 = 0
p2 = 0
with open("inputs/day08.txt") as file:
    for line in file:
        p1 += len(line.strip()) - len(eval(line.strip()))
        p2 += len(json.dumps(line.strip())) - len(line.strip())
print(p1, p2)