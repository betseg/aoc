from hashlib import md5

prefix = "ckczppom"

i = 0
p1 = None
p2 = None
while True:
    if p1 is None and md5((prefix+str(i)).encode()).hexdigest().startswith("00000"):
        p1 = i
    if p2 is None and md5((prefix+str(i)).encode()).hexdigest().startswith("000000"):
        p2 = i
    if p1 is not None and p2 is not None:
        break
    i += 1
print(p1, p2)