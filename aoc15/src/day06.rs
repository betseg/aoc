use std::collections::HashMap;

use scan_fmt::scan_fmt;

const INPUT: &str = include_str!("../input/day06.txt");

#[derive(Debug)]
enum Instr {
    On,
    Off,
    Toggle,
}

pub fn pass() -> (usize, usize) {
    (0, 0)
}

pub fn run() -> (usize, usize) {
    inner(INPUT)
}

fn inner(input: &str) -> (usize, usize) {
    let parsed = parse(input);
    let part1 = solve_part1(&parsed);
    let part2 = solve_part2(&parsed);
    (part1, part2)
}

fn parse(input: &str) -> Vec<(Instr, [u16; 4])> {
    input
        .lines()
        .map(|instr| {
            let (first, mut rest) = instr.split_once(' ').unwrap();
            let i = if first != "toggle" {
                let (first, tmp_rest) = rest.split_once(' ').unwrap();
                rest = tmp_rest;
                match first {
                    "on" => Instr::On,
                    "off" => Instr::Off,
                    _ => unreachable!(),
                }
            } else {
                Instr::Toggle
            };
            let (x1, y1, x2, y2) =
                scan_fmt!(rest, "{d},{d} through {d},{d}", u16, u16, u16, u16).unwrap();
            (i, [x1, y1, x2, y2])
        })
        .collect()
}

fn solve_part1(input: &[(Instr, [u16; 4])]) -> usize {
    let mut map = HashMap::new();
    for i in input {
        for x in i.1[0]..i.1[2] + 1 {
            for y in i.1[1]..i.1[3] + 1 {
                match i.0 {
                    Instr::On => *map.entry((x, y)).or_default() = true,
                    Instr::Off => *map.entry((x, y)).or_default() = false,
                    Instr::Toggle => {
                        map.entry((x, y)).and_modify(|e| *e = !*e).or_insert(true);
                    }
                }
            }
        }
    }
    map.iter().filter(|(_, &v)| v).count()
}

fn solve_part2(input: &[(Instr, [u16; 4])]) -> usize {
    let mut map: HashMap<(u16, u16), usize> = HashMap::new();
    for i in input {
        for x in i.1[0]..i.1[2] + 1 {
            for y in i.1[1]..i.1[3] + 1 {
                match i.0 {
                    Instr::On => *map.entry((x, y)).or_default() += 1,
                    Instr::Off => {
                        map.entry((x, y))
                            .and_modify(|e| *e = e.saturating_sub(1))
                            .or_default();
                    }
                    Instr::Toggle => *map.entry((x, y)).or_default() += 2,
                }
            }
        }
    }
    map.values().sum()
}

#[cfg(test)]
mod tests {
    use super::*;
    #[ignore]
    #[test]
    fn test_my_input() {
        assert_eq!(run(), (377891, 14110788));
    }
}
