// #![feature(bench_black_box)]
use clap::Parser;

#[derive(Parser)]
struct Opts {
    #[arg(short, long)]
    day: Option<usize>,
}

mod day01;
mod day02;
mod day03;
mod day04;
mod day05;
mod day06;
mod day07;
mod day08;
mod day09;
mod day10;

const SOLUTIONS: [&dyn Fn() -> (usize, usize); 10] = [
    &day01::run,
    &day02::run,
    &day03::run,
    // &day04::run,
    &day04::pass,
    &day05::run,
    // &day06::run,
    &day06::pass,
    &day07::run,
    &day08::run,
    &day09::run,
    &day10::run,
];

fn main() {
    if let Some(day) = Opts::parse().day {
        println!("Day {:02}: {:?}", day, SOLUTIONS[day - 1]());
    } else {
        for (i, f) in SOLUTIONS.iter().enumerate() {
            println!("Day {:02}: {:?}", i + 1, f());
        }
    }
}

// fn main() {
//     use std::hint::black_box;
//     use std::time::Instant;

//     for (i, f) in SOLUTIONS.iter().enumerate() {
//         for _ in 0..1000 {
//             black_box(f());
//         }
//         let now = Instant::now();
//         for _ in 0..5000 {
//             black_box(f());
//         }
//         let elapsed = now.elapsed();
//         println!("day {:02}: {:>10?}", i + 1, elapsed / 5000);
//     }
// }
