const INPUT: &str = include_str!("../input/day09.txt");

pub fn run() -> (usize, usize) {
    inner(INPUT)
}

fn inner(input: &str) -> (usize, usize) {
    (0, 0)
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_my_input() {
        assert_eq!(run(), (141, 736));
    }
}
