const INPUT: &str = include_str!("../input/day01.txt");

pub fn run() -> (usize, usize) {
    inner(INPUT)
}

fn inner(input: &str) -> (usize, usize) {
    let val = input
        .chars()
        .enumerate()
        .fold((0isize, None), |(n, i), (x, c)| {
            (
                if c == '(' { n + 1 } else { n - 1 },
                if i.is_none() && n == -1 { Some(x) } else { i },
            )
        });
    (val.0 as usize, val.1.unwrap())
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_my_input() {
        assert_eq!(run(), (74, 1795));
    }
}
