use itertools::Itertools;

const INPUT: &[u8] = include_bytes!("../input/day05.txt");

pub fn run() -> (usize, usize) {
    inner(INPUT)
}

fn inner(input: &[u8]) -> (usize, usize) {
    let mut parsed = input.split(|&c| c == b'\n');
    let part1 = solve_part1(&mut parsed.clone());
    let part2 = solve_part2(&mut parsed);
    (part1, part2)
}

fn solve_part1(parsed: &mut dyn Iterator<Item = &[u8]>) -> usize {
    parsed
        .filter(|s| {
            [b"ab", b"cd", b"pq", b"xy"]
                .iter()
                .all(|sub| !s.windows(2).any(|w| w == *sub))
        })
        .filter(|s| {
            s.iter()
                .filter(|&c| b"aeiou".iter().any(|f| f == c))
                .count()
                >= 3
        })
        .filter(|s| s.iter().dedup().count() != 16)
        .count()
}

fn solve_part2(parsed: &mut dyn Iterator<Item = &[u8]>) -> usize {
    parsed
        .filter(|s| s.windows(3).any(|w| w[0] == w[2]))
        .filter(|s| (0..s.len() - 3).any(|i| s[i + 2..].windows(2).any(|w| *w == s[i..i + 2])))
        .count()
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_my_input() {
        assert_eq!(run(), (258, 53));
    }
}
