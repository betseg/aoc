use std::collections::HashMap;

const INPUT: &str = include_str!("../input/day07.txt");

pub fn run() -> (usize, usize) {
    inner(INPUT)
}

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
enum Value {
    Int(u16),
    Str(String),
    Invalid,
}

fn inner(input: &str) -> (usize, usize) {
    let mut parsed = parse(input);
    let part1 = solve(&mut parsed.clone(), Value::Str("a".to_string()));
    parsed.insert("b".to_string(), Value::Int(part1));
    let part2 = solve(&mut parsed, Value::Str("a".to_string()));
    (part1 as usize, part2 as usize)
}

fn parse(input: &str) -> HashMap<String, Value> {
    let mut map = HashMap::new();
    for line in input.lines() {
        let (from, to) = line.split_once(" -> ").unwrap();
        map.insert(to.to_string(), Value::Str(from.to_string()));
    }
    map
}

fn solve(map: &mut HashMap<String, Value>, wire: Value) -> u16 {
    let expr = map
        .get(&String::try_from(&wire).unwrap_or("".to_string()))
        .map(|v| v.to_owned())
        .unwrap_or(Value::Invalid);

    if let Ok(i) = u16::try_from(&wire).or(u16::try_from(&expr)) {
        return i;
    }

    let res = match expr {
        Value::Int(i) => i,
        Value::Str(ref s) => {
            let cnt = s.chars().filter(|&c| c == ' ').count();
            let split = s.split_whitespace().collect::<Vec<_>>();
            match cnt {
                0 => solve(map, Value::Str(s.to_string())),
                1 => !solve(map, Value::Str(split[1].to_string())),
                2 => {
                    let l = solve(map, Value::Str(split[0].to_string()));
                    let r = solve(map, Value::Str(split[2].to_string()));
                    match split[1] {
                        "AND" => l & r,
                        "OR" => l | r,
                        "LSHIFT" => l << r,
                        "RSHIFT" => l >> r,
                        _ => unreachable!(),
                    }
                }
                _ => unreachable!(),
            }
        }
        Value::Invalid => unreachable!(),
    };

    map.insert(String::try_from(&wire).unwrap(), Value::Int(res));
    res
}

impl TryFrom<&Value> for u16 {
    type Error = ();

    fn try_from(value: &Value) -> Result<Self, Self::Error> {
        match value {
            Value::Int(i) => Ok(*i),
            Value::Str(s) => s.parse().map_err(|_| ()),
            Value::Invalid => Err(()),
        }
    }
}

impl TryFrom<&Value> for String {
    type Error = ();

    fn try_from(value: &Value) -> Result<Self, Self::Error> {
        match value {
            Value::Int(_) | Value::Invalid => Err(()),
            Value::Str(s) => Ok(s.clone()),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_my_input() {
        assert_eq!(run(), (3176, 14710));
    }
}
