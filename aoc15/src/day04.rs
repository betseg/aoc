use md5::{Digest, Md5};

pub fn pass() -> (usize, usize) {
    (0, 0)
}

pub fn run() -> (usize, usize) {
    inner(b"ckczppom")
}

fn inner(input: &[u8]) -> (usize, usize) {
    let first = 'out: loop {
        for i in 0.. {
            let mut hasher = Md5::new();
            hasher.update(input);
            hasher.update(&i.to_string());
            let res = hasher.finalize();
            if res[0..2] == [0, 0] && res[2] < 16 {
                break 'out i;
            }
        }
    };

    let second = 'out: loop {
        for i in 0.. {
            let mut hasher = Md5::new();
            hasher.update(input);
            hasher.update(&i.to_string());
            let res = hasher.finalize();
            if res[0..3] == [0, 0, 0] {
                break 'out i;
            }
        }
    };

    (first, second)
}

#[cfg(test)]
mod tests {
    use super::*;
    #[ignore]
    #[test]
    fn test_my_input() {
        assert_eq!(run(), (117946, 3938038));
    }
}
