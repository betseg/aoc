use std::collections::{HashMap, HashSet};

use itertools::{Itertools, MinMaxResult};

const INPUT: &str = include_str!("../input/day09.txt");

pub fn run() -> (usize, usize) {
    inner(INPUT)
}

fn inner(input: &str) -> (usize, usize) {
    let parsed = parse(input);
    solve(parsed)
}

fn parse(input: &str) -> (HashMap<&str, HashMap<&str, usize>>, HashSet<&str>) {
    let mut graph: HashMap<&str, HashMap<&str, usize>> = HashMap::new();
    let mut locs = HashSet::new();
    for line in input.lines() {
        let mut split = line.split_ascii_whitespace();
        let from = split.next().unwrap();
        let to = split.nth(1).unwrap();
        let cost = split.nth(1).unwrap().parse().unwrap();
        locs.insert(from);
        locs.insert(to);
        graph.entry(from).or_default().insert(to, cost);
        graph.entry(to).or_default().insert(from, cost);
    }
    (graph, locs)
}

fn solve<'a>(
    input: (HashMap<&'a str, HashMap<&'a str, usize>>, HashSet<&'a str>),
) -> (usize, usize) {
    println!("{:#?}", input);
    match input
        .1
        .iter()
        .permutations(input.1.len())
        .map(|route| {
            let mut dist = 0;
            for i in 0..route.len() - 1 {
                dist += input.0.get(route[i]).unwrap().get(route[i + 1]).unwrap();
            }
            dist
        })
        .minmax()
    {
        MinMaxResult::MinMax(l, r) => (l, r),
        _ => unreachable!(),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_my_input() {
        assert_eq!(run(), (141, 736));
    }
}
