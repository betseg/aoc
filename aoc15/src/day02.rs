const INPUT: &str = include_str!("../input/day02.txt");

pub fn run() -> (usize, usize) {
    inner(INPUT)
}

fn inner(input: &str) -> (usize, usize) {
    let parsed = parse(input);
    let part1 = solve_part1(&mut parsed.iter());
    let part2 = solve_part2(&mut parsed.iter());
    (part1, part2)
}

fn parse(input: &str) -> Vec<Vec<u32>> {
    input
        .lines()
        .map(|l| {
            let mut v: Vec<u32> = l.split('x').map(|v| v.parse().unwrap()).collect();
            v.sort_unstable();
            v
        })
        .collect()
}

fn solve_part1(input: &mut dyn Iterator<Item = &Vec<u32>>) -> usize {
    input.fold(0u32, |acc, size| {
        acc + size[0] * size[1] + 2 * (size[0] * size[1] + size[1] * size[2] + size[0] * size[2])
    }) as usize
}

fn solve_part2(input: &mut dyn Iterator<Item = &Vec<u32>>) -> usize {
    input.fold(0u32, |acc, size| {
        acc + 2 * (size[0] + size[1]) + size[0] * size[1] * size[2]
    }) as usize
}
#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_my_input() {
        assert_eq!(run(), (1586300, 3737498));
    }
}
