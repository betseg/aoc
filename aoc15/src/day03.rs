use std::collections::HashMap;

const INPUT: &[u8] = include_bytes!("../input/day03.txt");

pub fn run() -> (usize, usize) {
    inner(INPUT)
}

fn inner(input: &[u8]) -> (usize, usize) {
    let part1 = solve_part1(input);
    let part2 = solve_part2(input);
    (part1, part2)
}

fn solve_part1(input: &[u8]) -> usize {
    let mut visited = HashMap::new();
    visited.insert((0, 0), 1);
    let (mut x, mut y) = (0, 0);
    for c in input.iter() {
        match c {
            b'<' => x -= 1,
            b'>' => x += 1,
            b'^' => y += 1,
            b'v' => y -= 1,
            _ => unreachable!(),
        }
        *visited.entry((x, y)).or_default() += 1;
    }
    visited.len()
}

fn solve_part2(input: &[u8]) -> usize {
    let mut visited = HashMap::new();
    visited.insert((0, 0), 1);
    let (mut x_santa, mut y_santa) = (0, 0);
    let (mut x_robot, mut y_robot) = (0, 0);
    for (i, c) in input.iter().enumerate() {
        if i % 2 == 0 {
            match c {
                b'<' => x_santa -= 1,
                b'>' => x_santa += 1,
                b'^' => y_santa += 1,
                b'v' => y_santa -= 1,
                _ => unreachable!(),
            }
            *visited.entry((x_santa, y_santa)).or_default() += 1;
        } else {
            match c {
                b'<' => x_robot -= 1,
                b'>' => x_robot += 1,
                b'^' => y_robot += 1,
                b'v' => y_robot -= 1,
                _ => unreachable!(),
            }
            *visited.entry((x_robot, y_robot)).or_default() += 1;
        }
    }
    visited.len()
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_my_input() {
        assert_eq!(run(), (2565, 2639));
    }
}
