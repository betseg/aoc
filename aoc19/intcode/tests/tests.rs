#[cfg(test)]
mod tests {
    use std::{sync::mpsc, thread};
    extern crate aoc19intcode;
    use aoc19intcode::IntcodeVM;

    fn string_to_prog(input: &str) -> Vec<i64> {
        input.split(',').map(|i| i.parse().unwrap()).collect()
    }

    fn test_runner(input: &str, idx: usize) -> i64 {
        let input = string_to_prog(input);
        IntcodeVM::from_prog(&input).run_prog().unwrap()[&idx]
    }

    fn day2runner(input: &[i64]) -> i64 {
        let mut prog = IntcodeVM::from_prog(input);
        prog[1] = 12;
        prog[2] = 2;
        prog.run_prog().unwrap()[&0]
    }

    #[test]
    fn day2examples() {
        assert_eq!(day2runner(&string_to_prog("1,0,0,0,99")), 2);
        assert_eq!(test_runner("2,3,0,3,99", 3), 6);
        assert_eq!(test_runner("2,4,4,5,99,0", 5), 9801);
        assert_eq!(day2runner(&string_to_prog("1,1,1,4,99,5,6,0,99")), 30);
    }

    fn day9runner(input: &[i64]) -> i64 {
        let (mtx, prx) = mpsc::channel();
        let (ptx, mrx) = mpsc::channel();
        mtx.send(1).unwrap();
        let mut prog = IntcodeVM::with_io(input, Some(prx), Some(ptx));
        thread::spawn(move || prog.run_prog());
        mrx.recv().unwrap()
    }

    #[test]
    fn day9examples() {
        assert_eq!(
            day9runner(&string_to_prog("1102,34915192,34915192,7,4,7,99,0")),
            1_219_070_632_396_864
        );
        assert_eq!(
            day9runner(&string_to_prog("104,1125899906842624,99")),
            1_125_899_906_842_624
        );
    }
}
