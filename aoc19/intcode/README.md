# A standalone Advent of Code 2019 Intcode VM implementation

[![aoc19intcode](https://docs.rs/aoc19intcode/badge.svg)](https://docs.rs/aoc19intcode)

That's it. IDK what else to tell ya.

## Examples

```rs
use aoc19intcode::IntcodeVM;
assert_eq!(
    IntcodeVM::from_prog(&[2, 4, 4, 5, 99, 0])
        .run_prog()
        .unwrap()[5],
    9801
);
```
