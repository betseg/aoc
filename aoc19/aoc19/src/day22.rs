use scan_fmt::scan_fmt;

const LEN: usize = 10007;

#[aoc_generator(day22)]
pub fn input_generator(input: &str) -> Vec<usize> {
    let mut deck = (0..LEN).collect::<Vec<_>>();
    for line in input.lines() {
        if let Ok(num) = scan_fmt!(&line, "cut {d}", isize) {
            deck = cut(
                &deck,
                if num < 0 {
                    LEN - num.abs() as usize
                } else {
                    num as usize
                },
            )
        } else if let Ok(num) =
            scan_fmt!(&line, "deal with increment {d}", usize)
        {
            deck = deal(&deck, num);
        } else {
            // into new stack
            deck = deck.iter().rev().copied().collect();
        }
    }
    deck
}

#[aoc(day22, part1)]
pub fn solve_part1(input: &[usize]) -> usize {
    input.iter().position(|v| v == &2019).unwrap()
}

fn cut(deck: &[usize], num: usize) -> Vec<usize> {
    deck.iter()
        .skip(num)
        .chain(deck.iter().take(num))
        .copied()
        .collect()
}

fn deal(deck: &[usize], step: usize) -> Vec<usize> {
    let mut ret = vec![0; deck.len()];
    for i in 0..deck.len() {
        ret[i * step % deck.len()] = deck[i];
    }
    ret
}
