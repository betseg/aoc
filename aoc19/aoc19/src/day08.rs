const WIDTH: usize = 25;
const HAGTH: usize = 6;
#[aoc_generator(day8)]
pub fn input_generator(input: &[u8]) -> Vec<String> {
    input
        .chunks(WIDTH * HAGTH)
        .map(|c| String::from_utf8(c.to_vec()))
        .collect::<Result<Vec<String>, _>>()
        .unwrap()
}

#[aoc(day8, part1)]
pub fn solve_part1(input: &[String]) -> usize {
    let min0 = input
        .iter()
        .min_by(|a, b| a.matches('0').count().cmp(&b.matches('0').count()))
        .unwrap();
    min0.matches('1').count() * min0.matches('2').count()
}

#[aoc(day8, part2)]
pub fn solve_part2(input: &[String]) -> String {
    let mut res = vec!['2'; WIDTH * HAGTH];
    for (i, elem) in res.iter_mut().enumerate() {
        let mut layer = 0;
        'inner: loop {
            let cpix = input[layer].chars().nth(i).unwrap();
            if cpix != '2' {
                *elem = cpix;
                break 'inner;
            }
            layer += 1;
        }
    }

    let mut ret = String::from("\n");

    for sub in res.chunks(WIDTH) {
        for ch in sub {
            match ch {
                '0' => ret.push('.'),
                '1' => ret.push('#'),
                _ => unreachable!(),
            }
        }
        ret.push('\n');
    }

    ret
}

#[cfg(test)]
mod tests {
    //use super::*;

/*    #[test]
    fn example2() {
        assert_eq!(
            solve_part2(&input_generator("0222112222120000".as_bytes())),
            "\n.#\n#."
        );
    }*/
}
