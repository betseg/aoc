use num_integer::Integer;
use std::{
    collections::{HashMap, HashSet},
    f32::{
        consts::{FRAC_PI_2, PI},
        EPSILON,
    },
};

#[aoc_generator(day10)]
pub fn input_generator(input: &[u8]) -> HashSet<(usize, usize)> {
    let input = input
        .split(|c| *c == b'\n')
        .map(|l| l.iter().collect::<Vec<_>>())
        .collect::<Vec<_>>();
    let mut astrs = HashSet::new();
    for i in 0..input.len() {
        for j in 0..input[1].len() {
            if *input[i][j] == b'#' {
                astrs.insert((j, i));
            }
        }
    }
    astrs
}

fn station(
    input: &HashSet<(usize, usize)>,
) -> ((usize, usize), HashSet<(i32, i32)>) {
    let mut vsb = HashMap::new();
    for i in input {
        vsb.insert(*i, HashSet::new());
        for j in input {
            if i == j {
                continue;
            }
            if let Some(set) = vsb.get_mut(&i) {
                let a = i.0 as i32 - j.0 as i32;
                let b = i.1 as i32 - j.1 as i32;
                let gcd = a.gcd(&b);
                set.insert((a / gcd, b / gcd));
            }
        }
    }
    let res = vsb.iter().max_by_key(|(_, v)| v.len()).unwrap();
    (res.0.to_owned(), res.1.to_owned())
}

#[aoc(day10, part1)]
pub fn solve_part1(input: &HashSet<(usize, usize)>) -> usize {
    station(input).1.len()
}

#[aoc(day10, part2)]
pub fn solve_part2(input: &HashSet<(usize, usize)>) -> usize {
    let stat = station(input);
    let mut withangs = input
        .iter()
        .filter_map(|astr| {
            let a = (stat.0).0 as i32 - astr.0 as i32;
            let b = (stat.0).1 as i32 - astr.1 as i32;
            let gcd = a.gcd(&b);
            if *astr == stat.0 {
                None
            } else {
                let ang = ((b / gcd) as f32).atan2((a / gcd) as f32)
                    - FRAC_PI_2
                    + EPSILON;
                Some((
                    astr,
                    if ang > 0. { ang } else { PI * 2. + ang },
                    (a.abs() + b.abs()),
                ))
            }
        })
        .collect::<Vec<_>>();
    withangs.sort_unstable_by(|a, b| a.1.partial_cmp(&b.1).unwrap());

    let mut angs = withangs.iter().map(|e| e.1.to_string()).collect::<Vec<_>>();
    angs.dedup();

    let mut sortedastr = HashMap::new();
    for astr in angs.iter() {
        sortedastr.insert(astr, HashSet::new());
    }
    for astr in withangs.iter() {
        if let Some(s) = sortedastr.get_mut(&astr.1.to_string()) {
            s.insert((astr.0, astr.2));
        }
    }

    let mut vaporlist = Vec::new();
    let mut i = 0;
    while vaporlist.len() < input.len() - 1 {
        if i >= angs.len() {
            i = 0;
        }
        let ix = &angs[i];
        if !sortedastr[ix].is_empty() {
            let val = sortedastr[ix]
                .iter()
                .min_by_key(|a| a.1)
                .unwrap()
                .to_owned();
            vaporlist.push(val);
            sortedastr.get_mut(ix).unwrap().remove(&val);
        }
        i += 1;
    }

    let res = vaporlist[199].0;
    res.0 * 100 + res.1
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn examples1() {
        assert_eq!(
            solve_part1(&input_generator(
                b".#..#
.....
#####
....#
...##"
            )),
            8
        );
        assert_eq!(
            solve_part1(&input_generator(
                b"......#.#.
#..#.#....
..#######.
.#.#.###..
.#..#.....
..#....#.#
#..#....#.
.##.#..###
##...#..#.
.#....####"
            )),
            33
        );
        assert_eq!(
            solve_part1(&input_generator(
                b"#.#...#.#.
.###....#.
.#....#...
##.#.#.#.#
....#.#.#.
.##..###.#
..#...##..
..##....##
......#...
.####.###."
            )),
            35
        );
        assert_eq!(
            solve_part1(&input_generator(
                b".#..#..###
####.###.#
....###.#.
..###.##.#
##.##.#.#.
....###..#
..#.#..#.#
#..#.#.###
.##...##.#
.....#.#.."
            )),
            41
        );
        assert_eq!(
            solve_part1(&input_generator(
                b".#..##.###...#######
##.############..##.
.#.######.########.#
.###.#######.####.#.
#####.##.#.##.###.##
..#####..#.#########
####################
#.####....###.#.#.##
##.#################
#####.##.###..####..
..######..##.#######
####.##.####...##..#
.#####..#.######.###
##...#.##########...
#.##########.#######
.####.#.###.###.#.##
....##.##.###..#####
.#.#.###########.###
#.#.#.#####.####.###
###.##.####.##.#..##"
            )),
            210
        );
    }

    #[test]
    fn examples2() {
        assert_eq!(
            solve_part2(&input_generator(
                b".#..##.###...#######
##.############..##.
.#.######.########.#
.###.#######.####.#.
#####.##.#.##.###.##
..#####..#.#########
####################
#.####....###.#.#.##
##.#################
#####.##.###..####..
..######..##.#######
####.##.####...##..#
.#####..#.######.###
##...#.##########...
#.##########.#######
.####.#.###.###.#.##
....##.##.###..#####
.#.#.###########.###
#.#.#.#####.####.###
###.##.####.##.#..##"
            )),
            802
        );
    }
}
