use crate::intcode::IntcodeVM;
use std::sync::mpsc;
use std::thread;

#[aoc_generator(day17)]
pub fn input_generator(input: &str) -> Vec<i128> {
    input.split(',').map(|i| i.parse().unwrap()).collect()
}

#[aoc(day17, part1)]
pub fn solve_part1(input: &[i128]) -> usize {
    let (ptx, mrx) = mpsc::channel();
    let mut prog = IntcodeVM::with_io(input, None, Some(ptx));
    thread::spawn(move || prog.run_prog());
    let vec = mrx
        .iter()
        .collect::<Vec<_>>()
        .split(|c| *c as u8 == b'\n')
        .map(|s| s.to_vec())
        .collect::<Vec<_>>();

    let mut cnt = 0;
    for i in 1..vec.len() - 2 {
        for j in 1..vec[0].len() - 1 {
            if vec[i][j] as u8 == b'#'
                && vec[i - 1][j] as u8 == b'#'
                && vec[i][j - 1] as u8 == b'#'
                && vec[i][j + 1] as u8 == b'#'
                && vec[i + 1][j] as u8 == b'#'
            {
                cnt += i * j;
            }
        }
    }
    cnt
}

#[aoc(day17, part2)]
pub fn solve_part2(input: &[i128]) -> i128 {
    let (mtx, prx) = mpsc::channel();
    let (ptx, mrx) = mpsc::channel();
    let sol: String = String::from(
        "A,B,A,C,A,B,C,C,A,B
R,8,L,10,R,8
R,12,R,8,L,8,L,12
L,12,L,10,L,8
n
",
    );
    let mut prog = IntcodeVM::with_io(input, Some(prx), Some(ptx));
    prog[0] = 2;
    thread::spawn(move || prog.run_prog());
    for c in sol.bytes() {
        mtx.send(c as i128).unwrap();
    }
    mrx.iter().last().unwrap()
}
