use crate::intcode::IntcodeVM;

#[aoc_generator(day2)]
pub fn input_generator(input: &str) -> Vec<i128> {
    input.split(',').map(|i| i.parse().unwrap()).collect()
}

#[aoc(day2, part1)]
pub fn solve_part1(input: &[i128]) -> i128 {
    let mut prog = IntcodeVM::from_prog(input);
    prog[1] = 12;
    prog[2] = 2;
    prog.run_prog().unwrap()[0]
}

#[aoc(day2, part2)]
pub fn solve_part2(input: &[i128]) -> i128 {
    for i in 0..100 {
        for j in 0..100 {
            let mut prog = IntcodeVM::from_prog(input);
            prog[1] = i;
            prog[2] = j;
            if prog.run_prog().unwrap()[0] == 19_690_720 {
                return i * 100 + j;
            }
        }
    }
    unreachable!();
}

#[cfg(test)]
mod tests {
    use super::*;

    fn test_runner(input: &str, idx: usize) -> i128 {
        let input = input_generator(input);
        IntcodeVM::from_prog(&input).run_prog().unwrap()[idx]
    }

    #[test]
    fn examples1() {
        assert_eq!(solve_part1(&input_generator("1,0,0,0,99")), 2);
        assert_eq!(test_runner("2,3,0,3,99", 3), 6);
        assert_eq!(test_runner("2,4,4,5,99,0", 5), 9801);
        assert_eq!(solve_part1(&input_generator("1,1,1,4,99,5,6,0,99")), 30);
    }

    #[test]
    fn examples2() {
        //assert_eq!(solve_part2(&input_generator("14")), 2);
        //assert_eq!(solve_part2(&input_generator("1969")), 966);
        //assert_eq!(solve_part2(&input_generator("100756")), 50346);
    }
}
