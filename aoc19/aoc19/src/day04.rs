use std::collections::{HashMap, HashSet};

#[aoc_generator(day4)]
pub fn input_generator(input: &str) -> (u32, u32) {
    let input = input
        .split('-')
        .map(|s| s.parse().unwrap())
        .collect::<Vec<_>>();
    (input[0], input[1])
}

#[aoc(day4, part1)]
pub fn solve_part1((lo, hi): &(u32, u32)) -> usize {
    let mut val = 0;
    for i in *lo..=*hi {
        let v = i.to_string().into_bytes();
        let mut sorted = v.to_vec();
        sorted.sort_unstable();
        if v != sorted {
            continue;
        }
        let mut set = HashSet::new();
        for c in v.iter() {
            set.insert(c);
        }
        if set.len() != 6 {
            val += 1;
        }
    }
    val
}

#[aoc(day4, part2)]
pub fn solve_part2((lo, hi): &(u32, u32)) -> usize {
    let mut val = 0;
    for i in *lo..=*hi {
        let v = i.to_string().into_bytes();
        let mut sorted = v.to_vec();
        sorted.sort_unstable();
        if v != sorted {
            continue;
        }
        let mut map = HashMap::new();
        for c in v.iter() {
            *map.entry(c).or_insert(0) += 1;
        }
        if map.values().any(|x| x == &2) {
            val += 1;
        }
    }
    val
}

#[cfg(test)]
mod tests {}
