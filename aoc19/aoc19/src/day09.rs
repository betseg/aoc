use crate::intcode::IntcodeVM;
use std::sync::mpsc;
use std::thread;

#[aoc_generator(day9)]
pub fn input_generator(input: &str) -> Vec<i128> {
    input.split(',').map(|i| i.parse().unwrap()).collect()
}

fn runner(input: &[i128], i: i128) -> i128 {
    let (mtx, prx) = mpsc::channel();
    let (ptx, mrx) = mpsc::channel();
    mtx.send(i).unwrap();
    let mut prog = IntcodeVM::with_io(input, Some(prx), Some(ptx));
    thread::spawn(move || prog.run_prog());
    mrx.recv().unwrap()
}

#[aoc(day9, part1)]
pub fn solve_part1(input: &[i128]) -> i128 {
    runner(input, 1)
}

#[aoc(day9, part2)]
pub fn solve_part2(input: &[i128]) -> i128 {
    runner(input, 2)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn examples1() {
        let res =
            solve_part1(&input_generator("1102,34915192,34915192,7,4,7,99,0"));
        assert!(res > 1_000_000_000_000_000);
        assert!(res < 10_000_000_000_000_000);
        assert_eq!(
            solve_part1(&input_generator("104,1125899906842624,99")),
            1_125_899_906_842_624
        );
    }
}
