use crate::intcode::IntcodeVM;
use std::{
    collections::VecDeque,
    sync::{mpsc, Arc, Mutex},
    thread,
};

#[aoc_generator(day23)]
pub fn input_generator(input: &str) -> Vec<i128> {
    input.split(',').map(|i| i.parse().unwrap()).collect()
}

fn input_wrap(
    channels: Arc<Mutex<Vec<VecDeque<i128>>>>,
    mtx: mpsc::Sender<i128>,
    ftx: mpsc::Receiver<()>,
    i: i128,
) {
    for _ in ftx.iter() {
        let val = channels.lock().unwrap()[i as usize]
            .pop_front()
            .unwrap_or(-1);
        mtx.send(val).unwrap();
    }
}

fn output_wrap(
    channels: Arc<Mutex<Vec<VecDeque<i128>>>>,
    mrx: mpsc::Receiver<i128>,
    rtx: mpsc::Sender<i128>,
    send: bool,
) {
    for addr in mrx.iter() {
        let x = mrx.recv().unwrap();
        let y = mrx.recv().unwrap();
        let addr = if addr == 255 { 50 } else { addr as usize };
        channels.lock().unwrap()[addr].push_back(x);
        channels.lock().unwrap()[addr].push_back(y);
        if send && addr == 50 {
            rtx.send(x).unwrap();
            rtx.send(y).unwrap();
        }
    }
}

#[aoc(day23, part1)]
pub fn solve_part1(input: &[i128]) -> i128 {
    let channels = Arc::new(Mutex::new(vec![VecDeque::new(); 51]));
    let (rtx, rrx) = mpsc::channel();
    for i in 0..50 {
        let (mtx, prx) = mpsc::channel();
        let (ptx, mrx) = mpsc::channel();
        let (ftx, frx) = mpsc::channel();
        let input = input.to_vec();
        mtx.send(i).unwrap();
        thread::spawn(move || {
            IntcodeVM::with_inp_flag(&input, Some(prx), Some(ptx), Some(ftx))
                .run_prog()
        });
        let chns = channels.clone();
        thread::spawn(move || input_wrap(chns, mtx, frx, i));
        let chns = channels.clone();
        let rtx = rtx.clone();
        thread::spawn(move || output_wrap(chns, mrx, rtx, true));
    }
    rrx.recv().unwrap();
    rrx.recv().unwrap()
}

fn nat(channels: Arc<Mutex<Vec<VecDeque<i128>>>>) -> i128 {
    let mut prevy = None;
    loop {
        let chns = channels.lock().unwrap().to_vec();
        if !chns[50].is_empty() && chns.iter().take(50).all(VecDeque::is_empty)
        {
            let y = Some(channels.lock().unwrap()[50].pop_back().unwrap());
            let x = channels.lock().unwrap()[50].pop_back().unwrap();
            if prevy == y {
                break y.unwrap();
            }
            prevy = y;
            channels.lock().unwrap()[0].push_back(x);
            channels.lock().unwrap()[0].push_back(y.unwrap());
        }
    }
}

#[aoc(day23, part2)]
pub fn solve_part2(input: &[i128]) -> i128 {
    let channels = Arc::new(Mutex::new(vec![VecDeque::new(); 51]));
    for i in 0..50 {
        let (mtx, prx) = mpsc::channel();
        let (ptx, mrx) = mpsc::channel();
        let (ftx, frx) = mpsc::channel();
        let input = input.to_vec();
        mtx.send(i).unwrap();
        thread::spawn(move || {
            IntcodeVM::with_inp_flag(&input, Some(prx), Some(ptx), Some(ftx))
                .run_prog()
        });
        let chns = channels.clone();
        thread::spawn(move || input_wrap(chns, mtx, frx, i));
        let chns = channels.clone();
        let (h, _) = mpsc::channel();
        thread::spawn(move || output_wrap(chns, mrx, h, false));
    }
    nat(channels)
}
