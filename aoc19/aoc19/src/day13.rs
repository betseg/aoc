use crate::intcode::IntcodeVM;
use std::{
    collections::HashMap,
    sync::{mpsc, Arc, Mutex},
    thread,
};

#[aoc_generator(day13)]
pub fn input_generator(input: &str) -> Vec<i128> {
    input.split(',').map(|i| i.parse().unwrap()).collect()
}

#[aoc(day13, part1)]
pub fn solve_part1(input: &[i128]) -> usize {
    let (atx, mrx) = mpsc::channel();
    IntcodeVM::with_io(input, None, Some(atx))
        .run_prog()
        .unwrap();
    mrx.iter().skip(2).step_by(3).filter(|t| t == &2).count()
}

// only works sometimes and i have no idea why
#[aoc(day13, part2)]
pub fn solve_part2(input: &[i128]) -> i128 {
    let (atx, mrx) = mpsc::channel();
    let (mtx, arx) = mpsc::channel();
    let (ftx, frx) = mpsc::channel();
    let mut arcade =
        IntcodeVM::with_inp_flag(input, Some(arx), Some(atx), Some(ftx));
    arcade[0] = 2;
    thread::spawn(move || arcade.run_prog());

    let pxs = Arc::new(Mutex::new(HashMap::<(i128, i128), i128>::new()));

    let tpxs = pxs.clone();
    thread::spawn(move || {
        for _ in frx.iter() {
            let ball = (tpxs
                .lock()
                .unwrap()
                .iter()
                .find(|(_, v)| **v == 4)
                .unwrap()
                .0)
                .0;
            let pddl = (tpxs
                .lock()
                .unwrap()
                .iter()
                .find(|(_, v)| **v == 3)
                .unwrap()
                .0)
                .0;
            mtx.send((ball - pddl).signum()).unwrap();
        }
    });

    let mut sf = false;
    let mut score = 0;
    let mut loc = (0, 0);
    for (i, recv) in mrx.iter().enumerate() {
        match i % 3 {
            0 => {
                sf = recv == -1;
                loc.0 = recv;
            }
            1 => {
                sf = sf && recv == 0;
                loc.1 = recv;
            }
            2 => {
                if sf {
                    score = recv;
                }
                pxs.lock().unwrap().insert(loc, recv);
            }
            _ => unreachable!(),
        }
    }
    score
}

#[cfg(test)]
mod tests {}
