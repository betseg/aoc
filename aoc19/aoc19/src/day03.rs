use csv::ReaderBuilder;
use std::collections::{HashMap, HashSet};

type LocsMap = HashMap<(i32, i32), i32>;
type LocsSet = HashSet<(i32, i32)>;

#[aoc_generator(day3)]
pub fn input_generator(input: &str) -> (Vec<LocsSet>, Vec<LocsMap>) {
    let mut csvrdr = ReaderBuilder::new()
        .has_headers(false)
        .from_reader(input.as_bytes());

    let cables = csvrdr
        .records()
        .map(|l| l.unwrap().iter().map(|s| s.to_owned()).collect::<Vec<_>>())
        .collect::<Vec<_>>();

    let locs = vec![cable(&cables[0]), cable(&cables[1])];

    (
        locs.to_vec()
            .iter()
            .map(|c| c.keys().copied().collect::<LocsSet>())
            .collect::<Vec<_>>(),
        locs,
    )
}

fn cable(steps: &[String]) -> LocsMap {
    let mut dirs = HashMap::new();
    dirs.insert('L', (-1, 0));
    dirs.insert('U', (0, 1));
    dirs.insert('R', (1, 0));
    dirs.insert('D', (0, -1));

    let mut x = 0;
    let mut y = 0;
    let mut step = 0;
    let mut locs = HashMap::new();
    for i in steps.iter() {
        let (dir, num) = i.split_at(1);
        let (dx, dy) = dirs[&dir.parse().unwrap()];
        for _ in 0..num.parse().unwrap() {
            step += 1;
            x += dx;
            y += dy;
            locs.insert((x, y), step);
        }
    }

    locs
}

#[aoc(day3, part1)]
pub fn solve_part1(locs: &(Vec<LocsSet>, Vec<LocsMap>)) -> i32 {
    let (a, b) = locs.0[0]
        .intersection(&locs.0[1])
        .min_by_key(|v| v.0.abs() + v.1.abs())
        .unwrap();
    a.abs() + b.abs()
}

#[aoc(day3, part2)]
pub fn solve_part2(locs: &(Vec<LocsSet>, Vec<LocsMap>)) -> i32 {
    let i = locs.0[0]
        .intersection(&locs.0[1])
        .min_by_key(|v| locs.1[0][v] + locs.1[0][v])
        .unwrap();
    locs.1[0][i] + locs.1[1][i]
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn examples1() {
        assert_eq!(
            solve_part1(&input_generator(
                "R8,U5,L5,D3
U7,R6,D4,L4"
            )),
            6
        );
        assert_eq!(
            solve_part1(&input_generator(
                "R75,D30,R83,U83,L12,D49,R71,U7,L72
U62,R66,U55,R34,D71,R55,D58,R83,R0"
            )),
            159
        );
        assert_eq!(
            solve_part1(&input_generator(
                "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51
U98,R91,D20,R16,D67,R40,U7,R15,U6,R7,R0"
            )),
            135
        );
    }

    #[test]
    fn examples2() {
        assert_eq!(
            solve_part2(&input_generator(
                "R8,U5,L5,D3
U7,R6,D4,L4"
            )),
            30
        );
        assert_eq!(
            solve_part2(&input_generator(
                "R75,D30,R83,U83,L12,D49,R71,U7,L72
U62,R66,U55,R34,D71,R55,D58,R83,R0"
            )),
            610
        );
        assert_eq!(
            solve_part2(&input_generator(
                "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51
U98,R91,D20,R16,D67,R40,U7,R15,U6,R7,R0"
            )),
            410
        );
    }
}
