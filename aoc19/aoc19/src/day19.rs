use crate::intcode::IntcodeVM;
use std::sync::mpsc;
use std::thread;

#[aoc_generator(day19)]
pub fn input_generator(input: &str) -> Vec<i128> {
    input.split(',').map(|i| i.parse().unwrap()).collect()
}

#[aoc(day19, part1)]
pub fn solve_part1(input: &[i128]) -> usize {
    let mut ctr = 0;
    for i in 0..50 {
        for j in 0..50 {
            let (mtx, prx) = mpsc::channel();
            let (ptx, mrx) = mpsc::channel();
            let mut prog = IntcodeVM::with_io(input, Some(prx), Some(ptx));
            thread::spawn(move || prog.run_prog());
            mtx.send(i).unwrap();
            mtx.send(j).unwrap();
            if mrx.recv().unwrap() == 1 {
                ctr += 1;
            }
        }
    }
    ctr
}
