use crate::intcode::IntcodeVM;
use std::{
    collections::HashMap,
    sync::mpsc,
    sync::mpsc::{Receiver, Sender},
    thread,
};

#[aoc_generator(day11)]
pub fn input_generator(input: &str) -> Vec<i128> {
    input.split(',').map(|i| i.parse().unwrap()).collect()
}

fn controller(
    part: i128,
    rx: Receiver<i128>,
    tx: Sender<i128>,
) -> HashMap<(i128, i128), i128> {
    tx.send(part - 1).unwrap();
    let mut panels = HashMap::new();
    let mut dir = 0;
    let mut x = 0;
    let mut y = 0;
    panels.insert((x, y), 1);
    for (i, recv) in rx.iter().enumerate() {
        if recv == -1 {
            break;
        }
        match i % 2 {
            0 => {
                panels.insert((x, y), recv);
            }
            1 => {
                dir += if recv == 0 { -1 } else { 1 };
                dir %= 4;
                if dir < 0 {
                    dir += 4;
                }
                match dir {
                    0 => y += 1,
                    1 => x += 1,
                    2 => y -= 1,
                    3 => x -= 1,
                    _ => unreachable!(),
                }
                let val = panels.entry((x, y)).or_insert(0).to_owned();
                if tx.send(val).is_err() {
                    break;
                }
                panels.insert((x, y), val);
            }
            _ => unreachable!(),
        }
    }
    panels
}

#[aoc(day11, part1)]
pub fn solve_part1(input: &[i128]) -> usize {
    let (ct, rr) = mpsc::channel();
    let (rt, cr) = mpsc::channel();
    let mut robot = IntcodeVM::with_io(input, Some(rr), Some(rt));
    thread::spawn(move || robot.run_prog());
    controller(1, cr, ct).len()
}

#[aoc(day11, part2)]
pub fn solve_part2(input: &[i128]) -> String {
    let (ct, rr) = mpsc::channel();
    let (rt, cr) = mpsc::channel();
    let mut robot = IntcodeVM::with_io(input, Some(rr), Some(rt));
    thread::spawn(move || robot.run_prog());
    let panels = controller(2, cr, ct);

    let mxx = panels.keys().max_by_key(|p| p.0).unwrap().0 as i128;
    let mnx = panels.keys().min_by_key(|p| p.0).unwrap().0 as i128;
    let mxy = panels.keys().max_by_key(|p| p.1).unwrap().1 as i128;
    let mny = panels.keys().min_by_key(|p| p.1).unwrap().1 as i128;
    let xr = (mxx - mnx).abs() + 1;
    let yr = (mxy - mny).abs() + 1;
    let mut reg = vec![vec![0; xr as usize]; yr as usize];

    for i in panels.iter() {
        reg[((i.0).1 - mny) as usize][((i.0).0 - mnx) as usize] = *i.1;
    }

    let mut res = String::from("\n");
    for i in (0..reg.len()).rev() {
        for j in 0..reg[0].len() {
            res.push(if reg[i][j] == 0 { '.' } else { '#' });
        }
        res.push('\n');
    }
    res
}

#[cfg(test)]
mod tests {}
