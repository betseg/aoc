use std::{f32::consts::FRAC_PI_2, iter::repeat};

#[aoc_generator(day16)]
pub fn input_generator(input: &str) -> Vec<i32> {
    input
        .chars()
        .map(|n| n.to_digit(10).unwrap() as i32)
        .collect()
}

fn do_a_thing(prev: &[i32]) -> Vec<i32> {
    let mut newv = vec![0; prev.len()];
    for (i, new) in newv.iter_mut().enumerate() {
        let mut val = 0;
        for (j, k) in (0..)
            .map(|n| (n as f32 * FRAC_PI_2).sin())
            .flat_map(|n| repeat(n).take(i + 1))
            .skip(1)
            .take(prev.len())
            .enumerate()
        {
            val += prev[j] * k.round() as i32;
        }
        *new = val.abs() % 10;
    }
    newv
}

#[aoc(day16, part1)]
pub fn solve_part1(input: &[i32]) -> String {
    let mut input = input.to_vec();
    for _ in 0..100 {
        input = do_a_thing(&input);
    }
    String::from_utf8(
        input
            .iter()
            .take(8)
            .map(|n| *n as u8 + b'0')
            .collect::<Vec<_>>(),
    )
    .unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn examples1() {
        assert_eq!(
            solve_part1(&input_generator("80871224585914546619083218645595")),
            "24176176"
        );
        assert_eq!(
            solve_part1(&input_generator("19617804207202209144916044189917")),
            "73745418"
        );
        assert_eq!(
            solve_part1(&input_generator("69317163492948606335995924319873")),
            "52432133"
        );
    }
}
