use num_integer::lcm;
use scan_fmt::scan_fmt;
use std::collections::HashSet;

#[derive(Clone, Copy, Hash)]
struct Position {
    x: i128,
    y: i128,
    z: i128,
}

#[derive(Clone, Copy, Hash)]
struct Velocity {
    x: i128,
    y: i128,
    z: i128,
}

#[derive(Clone, Copy, Hash)]
pub struct Moon {
    pos: Position,
    vel: Velocity,
}

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum Axis {
    X,
    Y,
    Z,
}

#[aoc_generator(day12)]
pub fn input_generator(input: &str) -> Vec<Moon> {
    let mut moons = Vec::new();
    for moon in input.split('\n') {
        if let Ok((x, y, z)) =
            scan_fmt!(moon, "<x={d}, y={d}, z={d}", i128, i128, i128)
        {
            moons.push(Moon {
                pos: Position { x, y, z },
                vel: Velocity { x: 0, y: 0, z: 0 },
            });
        }
    }
    moons
}

fn update(moons: &mut Vec<Moon>, axis: Axis) {
    upd_vel(moons, axis);
    upd_pos(moons, axis);
}

fn upd_vel(moons: &mut Vec<Moon>, axis: Axis) {
    for i in 0..moons.len() {
        for j in 0..moons.len() {
            match axis {
                Axis::X => {
                    moons[i].vel.x += (moons[j].pos.x - moons[i].pos.x).signum()
                }
                Axis::Y => {
                    moons[i].vel.y += (moons[j].pos.y - moons[i].pos.y).signum()
                }
                Axis::Z => {
                    moons[i].vel.z += (moons[j].pos.z - moons[i].pos.z).signum()
                }
            }
        }
    }
}

fn upd_pos(moons: &mut Vec<Moon>, axis: Axis) {
    for i in moons.iter_mut() {
        match axis {
            Axis::X => i.pos.x += i.vel.x,
            Axis::Y => i.pos.y += i.vel.y,
            Axis::Z => i.pos.z += i.vel.z,
        }
    }
}

#[aoc(day12, part1)]
pub fn solve_part1(input: &[Moon]) -> i128 {
    let mut moons = input.to_vec();
    for _ in 0..1000 {
        update(&mut moons, Axis::X);
        update(&mut moons, Axis::Y);
        update(&mut moons, Axis::Z);
    }
    moons.iter().map(Moon::energy).sum()
}

#[aoc(day12, part2)]
pub fn solve_part2(input: &[Moon]) -> i128 {
    let mut vals = Vec::new();
    for axis in [Axis::X, Axis::Y, Axis::Z].iter() {
        let mut moons = input.to_vec();
        let mut poss = HashSet::new();
        poss.insert(
            moons
                .iter()
                .map(|m| (m.get_pos(*axis), m.get_vel(*axis)))
                .collect::<Vec<_>>(),
        );
        let mut i = 1;
        loop {
            update(&mut moons, *axis);
            let ax = moons
                .iter()
                .map(|m| (m.get_pos(*axis), m.get_vel(*axis)))
                .collect::<Vec<_>>();
            if poss.contains(&ax) {
                vals.push(i);
                break;
            }
            poss.insert(ax);
            i += 1;
        }
    }
    lcm(lcm(vals[0], vals[1]), vals[2])
}

impl Moon {
    fn energy(&self) -> i128 {
        self.ke() * self.pe()
    }

    fn ke(&self) -> i128 {
        self.vel.x.abs() + self.vel.y.abs() + self.vel.z.abs()
    }

    fn pe(&self) -> i128 {
        self.pos.x.abs() + self.pos.y.abs() + self.pos.z.abs()
    }

    pub fn get_pos(&self, axis: Axis) -> i128 {
        match axis {
            Axis::X => self.pos.x,
            Axis::Y => self.pos.y,
            Axis::Z => self.pos.z,
        }
    }

    pub fn get_vel(&self, axis: Axis) -> i128 {
        match axis {
            Axis::X => self.vel.x,
            Axis::Y => self.vel.y,
            Axis::Z => self.vel.z,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn examples2() {
        assert_eq!(
            solve_part2(&input_generator(
                "<x=-1, y=0, z=2>
<x=2, y=-10, z=-7>
<x=4, y=-8, z=8>
<x=3, y=5, z=-1>"
            )),
            2772
        );
        assert_eq!(
            solve_part2(&input_generator(
                "<x=-8, y=-10, z=0>
<x=5, y=5, z=10>
<x=2, y=-7, z=3>
<x=9, y=-8, z=-3>"
            )),
            4_686_774_924
        );
    }
}
