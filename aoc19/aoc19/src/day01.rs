type Rocket = i32;

#[aoc_generator(day1)]
pub fn input_generator(input: &str) -> Vec<Rocket> {
    input.lines().map(|l| l.trim().parse().unwrap()).collect()
}

fn do_one(one: Rocket) -> i32 {
    one / 3 - 2
}

#[aoc(day1, part1)]
pub fn solve_part1(input: &[Rocket]) -> i32 {
    input.iter().map(|r| do_one(*r)).sum()
}

#[aoc(day1, part2)]
pub fn solve_part2(input: &[Rocket]) -> i32 {
    input
        .iter()
        .map(|r| {
            let mut r = *r;
            let mut v = -r;
            while r > 0 {
                v += r;
                r = do_one(r);
            }
            v
        })
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn examples1() {
        assert_eq!(solve_part1(&input_generator("12")), 2);
        assert_eq!(solve_part1(&input_generator("14")), 2);
        assert_eq!(solve_part1(&input_generator("1969")), 654);
        assert_eq!(solve_part1(&input_generator("100756")), 33583);
    }

    #[test]
    fn examples2() {
        assert_eq!(solve_part2(&input_generator("14")), 2);
        assert_eq!(solve_part2(&input_generator("1969")), 966);
        assert_eq!(solve_part2(&input_generator("100756")), 50346);
    }
}
