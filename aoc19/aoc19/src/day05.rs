use crate::intcode::IntcodeVM;
use std::sync::mpsc::channel;
use std::thread;

#[aoc_generator(day5)]
pub fn input_generator(input: &str) -> Vec<i128> {
    input.split(',').map(|i| i.parse().unwrap()).collect()
}

#[aoc(day5, part1)]
pub fn solve_part1(input: &[i128]) -> i128 {
    let (mtx, prx) = channel();
    let (ptx, mrx) = channel();
    mtx.send(1).unwrap();
    let mut prog = IntcodeVM::with_io(input, Some(prx), Some(ptx));
    thread::spawn(move || prog.run_prog());
    mrx.iter().last().unwrap()
}

#[aoc(day5, part2)]
pub fn solve_part2(input: &[i128]) -> i128 {
    let (mtx, prx) = channel();
    let (ptx, mrx) = channel();
    mtx.send(5).unwrap();
    let mut prog = IntcodeVM::with_io(input, Some(prx), Some(ptx));
    thread::spawn(move || prog.run_prog());
    mrx.iter().last().unwrap()
}

#[cfg(test)]
mod tests {
    #[test]
    fn examples1() {}

    #[test]
    fn examples2() {
        //assert_eq!(solve_part2(&input_generator("14")), 2);
        //assert_eq!(solve_part2(&input_generator("1969")), 966);
        //assert_eq!(solve_part2(&input_generator("100756")), 50346);
    }
}
