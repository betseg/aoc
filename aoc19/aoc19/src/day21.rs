use crate::intcode::IntcodeVM;
use std::sync::mpsc;
use std::thread;

#[aoc_generator(day21)]
pub fn input_generator(input: &str) -> Vec<i128> {
    input.split(',').map(|i| i.parse().unwrap()).collect()
}

fn runner(input: &str, prog: &[i128]) -> i128 {
    let (mtx, prx) = mpsc::channel();
    let (ptx, mrx) = mpsc::channel();
    let mut prog = IntcodeVM::with_io(prog, Some(prx), Some(ptx));
    thread::spawn(move || prog.run_prog());
    for c in input.bytes() {
        mtx.send(c as i128).unwrap();
    }
    mrx.iter().last().unwrap()
}

#[aoc(day21, part1)]
pub fn solve_part1(input: &[i128]) -> i128 {
    let inp = String::from(
        // (!A|!B|!C)&D WALK
        "\
NOT A T
NOT B J
OR T J
NOT C T
OR T J
AND D J
WALK
",
    );
    runner(&inp, input)
}

#[aoc(day21, part2)]
pub fn solve_part2(input: &[i128]) -> i128 {
    let inp = String::from(
        // !(A|B|C)&D&(E|!H) RUN
        "\
NOT A T
NOT B J
OR T J
NOT C T
OR T J
AND D J
NOT H T
NOT T T
OR E T
AND T J
RUN
",
    );
    runner(&inp, input)
}
