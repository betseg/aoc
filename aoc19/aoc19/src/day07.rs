use crate::intcode::IntcodeVM;
use permutohedron::Heap;
use std::{cmp::max, ops::Range, sync::mpsc, thread};

#[aoc_generator(day7)]
pub fn input_generator(input: &str) -> Vec<i128> {
    input.split(',').map(|i| i.parse().unwrap()).collect()
}

fn runner(input: &[i128], part: Range<i128>) -> i128 {
    let mut nums = part.collect::<Vec<_>>();
    let heap = Heap::new(&mut nums);

    let mut mx = 0;

    for perms in heap {
        let (tm0, rm0) = mpsc::channel();
        let (t01, r01) = mpsc::channel();
        let (t12, r12) = mpsc::channel();
        let (t23, r23) = mpsc::channel();
        let (t34, r34) = mpsc::channel();
        let (t4m, r4m) = mpsc::channel();
        tm0.send(perms[0]).unwrap();
        t01.send(perms[1]).unwrap();
        t12.send(perms[2]).unwrap();
        t23.send(perms[3]).unwrap();
        t34.send(perms[4]).unwrap();
        let inp = input.to_vec();
        thread::spawn(move || {
            IntcodeVM::with_io(&inp, Some(rm0), Some(t01)).run_prog()
        });
        let inp = input.to_vec();
        thread::spawn(move || {
            IntcodeVM::with_io(&inp, Some(r01), Some(t12)).run_prog()
        });
        let inp = input.to_vec();
        thread::spawn(move || {
            IntcodeVM::with_io(&inp, Some(r12), Some(t23)).run_prog()
        });
        let inp = input.to_vec();
        thread::spawn(move || {
            IntcodeVM::with_io(&inp, Some(r23), Some(t34)).run_prog()
        });
        let inp = input.to_vec();
        thread::spawn(move || {
            IntcodeVM::with_io(&inp, Some(r34), Some(t4m)).run_prog()
        });
        tm0.send(0).unwrap();
        let mut sig = 0;
        for rec in r4m {
            sig = rec;
            tm0.send(rec).ok();
        }
        mx = max(mx, sig);
    }
    mx
}

#[aoc(day7, part1)]
pub fn solve_part1(input: &[i128]) -> i128 {
    runner(input, 0..5)
}

#[aoc(day7, part2)]
pub fn solve_part2(input: &[i128]) -> i128 {
    runner(input, 5..10)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn examples1() {
        assert_eq!(
            solve_part1(&input_generator(
                "3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0"
            )),
            43210
        );
        assert_eq!(
            solve_part1(&input_generator(
                "3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0"
            )),
            54321
        );
        assert_eq!(
            solve_part1(&input_generator(
                "3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0"
            )),
            65210
        );
    }

    #[test]
    fn examples2() {
        assert_eq!(
            solve_part2(&input_generator(
                "3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5"
            )),
            139_629_729
        );
        assert_eq!(
            solve_part2(&input_generator(
                "3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10"
            )),
            18216
        );
    }
}
