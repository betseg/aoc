use std::collections::HashSet;

#[aoc_generator(day24)]
pub fn input_generator(input: &str) -> Vec<Vec<bool>> {
    input
        .lines()
        .map(|l| l.chars().map(|c| c == '#').collect::<Vec<_>>())
        .collect::<Vec<_>>()
}

#[aoc(day24, part1)]
pub fn solve_part1(input: &[Vec<bool>]) -> usize {
    let mut input = input.to_vec();
    let mut seen = HashSet::new();
    let mut chhash = hash(&input);

    while !seen.contains(&chhash) {
        seen.insert(chhash);
        input = tick(&input);
        chhash = hash(&input);
    }

    chhash
}

fn tick(field: &[Vec<bool>]) -> Vec<Vec<bool>> {
    let mut res = vec![vec![false; 5]; 5];
    for i in 0..5 {
        for j in 0..5 {
            let mut ctr = 0;
            for dy in 0..=2 {
                if i + dy != 0 && i + dy != 6 {
                    for dx in 0..=2 {
                        if (dy == 1 || dx == 1)
                            && dx != dy
                            && j + dx != 0
                            && j + dx != 6
                            && field[i + dy - 1][j + dx - 1]
                        {
                            ctr += 1;
                        }
                    }
                }
            }
            if field[i][j] {
                res[i][j] = ctr == 1;
            } else {
                res[i][j] = (ctr == 1) || (ctr == 2);
            }
        }
    }
    res
}

fn hash(field: &[Vec<bool>]) -> usize {
    let mut n = 0;
    let mut res = 0;
    for i in field.iter() {
        for j in i.iter() {
            if *j {
                res += 2usize.pow(n);
            }
            n += 1;
        }
    }
    res
}
