use std::collections::{HashMap, HashSet};

#[aoc_generator(day6)]
pub fn input_generator(input: &str) -> HashMap<String, String> {
    let input = input
        .lines()
        .map(|l| l.split(')').map(|s| s.to_owned()).collect())
        .collect::<Vec<Vec<_>>>();

    let mut orbits = HashMap::new();
    for pair in input {
        orbits.insert(pair[1].to_owned(), pair[0].to_owned());
    }
    orbits
}

#[aoc(day6, part1)]
pub fn solve_part1(input: &HashMap<String, String>) -> usize {
    let mut res = 0;
    for pair in input {
        res += 1;
        let mut smol: &String;
        let mut big = pair.1;
        while big != "COM" {
            res += 1;
            smol = &big;
            big = &input[smol];
        }
    }
    res
}

#[aoc(day6, part2)]
pub fn solve_part2(input: &HashMap<String, String>) -> usize {
    let mut parents = vec![Vec::new(); 2];
    for pair in input {
        let mut smol = pair.0;
        let mut big = pair.1;
        let flag = ["YOU", "SAN"].iter().position(|s| s == &&smol[..]);
        while big != "COM" {
            if let Some(ix) = flag {
                parents[ix].push(big);
            }
            smol = &big;
            big = &input[smol];
        }
    }
    parents[0]
        .iter()
        .collect::<HashSet<_>>()
        .symmetric_difference(&parents[1].iter().collect::<HashSet<_>>())
        .count()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn examples1() {
        assert_eq!(
            solve_part1(&input_generator(
                "COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L"
            )),
            42
        );
    }

    #[test]
    fn examples2() {
        assert_eq!(
            solve_part2(&input_generator(
                "COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L
K)YOU
I)SAN"
            )),
            4
        );
    }
}
