use num::integer::lcm;
use std::{collections::HashMap, ops::ControlFlow};

const INPUT: &str = include_str!("../input/day08.txt");

#[derive(Clone, Copy, Debug)]
enum Dir {
    L = 0,
    R,
}

#[derive(Clone, Debug)]
struct Map<'a> {
    dir: Vec<Dir>,
    graph: HashMap<&'a str, [&'a str; 2]>,
}

pub fn run() -> (usize, usize) {
    inner(INPUT)
}

fn inner(input: &str) -> (usize, usize) {
    let parsed = parse(input);
    let part1 = solve_part1(&parsed);
    let part2 = solve_part2(&parsed);
    (part1, part2)
}

fn parse(input: &str) -> Map {
    let (dir, graph) = input.split_once("\n\n").unwrap();

    let dir = dir
        .chars()
        .map(|c| match c {
            'L' => Dir::L,
            'R' => Dir::R,
            _ => unreachable!(),
        })
        .collect();

    let graph = graph
        .lines()
        .map(|l| (&l[0..3], [&l[7..10], &l[12..15]]))
        .collect();

    Map { dir, graph }
}

fn solve_part1(input: &Map) -> usize {
    steps(input, "AAA", 1)
}

fn solve_part2(input: &Map) -> usize {
    input
        .graph
        .keys()
        .filter_map(|k| {
            if k.ends_with('A') {
                Some(steps(input, k, 2))
            } else {
                None
            }
        })
        .fold(1, lcm)
}

fn steps(input: &Map, start: &str, part: u8) -> usize {
    if let ControlFlow::Break(v) = input.dir.iter().cycle().try_fold((0, start), |(s, c), d| {
        if part == 1 && c == "ZZZ" || part == 2 && c.ends_with('Z') {
            ControlFlow::Break(s)
        } else {
            ControlFlow::Continue((s + 1, input.graph[c][*d as usize]))
        }
    }) {
        v
    } else {
        unreachable!()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_test() {
        assert_eq!(
            solve_part1(&parse(
                "RL\n\
                 \n\
                 AAA = (BBB, CCC)\n\
                 BBB = (DDD, EEE)\n\
                 CCC = (ZZZ, GGG)\n\
                 DDD = (DDD, DDD)\n\
                 EEE = (EEE, EEE)\n\
                 GGG = (GGG, GGG)\n\
                 ZZZ = (ZZZ, ZZZ)"
            )),
            2
        );

        assert_eq!(
            solve_part1(&parse(
                "LLR\n\
                 \n\
                 AAA = (BBB, BBB)\n\
                 BBB = (AAA, ZZZ)\n\
                 ZZZ = (ZZZ, ZZZ)"
            )),
            6
        );

        assert_eq!(
            solve_part2(&parse(
                "LR\n\
                 \n\
                 11A = (11B, XXX)\n\
                 11B = (XXX, 11Z)\n\
                 11Z = (11B, XXX)\n\
                 22A = (22B, XXX)\n\
                 22B = (22C, 22C)\n\
                 22C = (22Z, 22Z)\n\
                 22Z = (22B, 22B)\n\
                 XXX = (XXX, XXX)"
            )),
            6
        );
    }

    #[test]
    fn test_my_input() {
        assert_eq!(run(), (19951, 16342438708751));
    }
}
