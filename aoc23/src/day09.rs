const INPUT: &str = include_str!("../input/day09.txt");

pub fn run() -> (usize, usize) {
    inner(INPUT)
}

fn inner(input: &str) -> (usize, usize) {
    let parsed = parse(input);
    let part1 = solve_part1(&parsed);
    let part2 = solve_part2(&parsed);
    (part1, part2)
}

fn parse(input: &str) -> Vec<Vec<isize>> {
    input
        .lines()
        .map(|l| {
            l.split_ascii_whitespace()
                .map(|n| n.parse().unwrap())
                .collect()
        })
        .collect()
}

fn solve_part1(input: &[Vec<isize>]) -> usize {
    input
        .iter()
        .map(|d| difference_engine(d.iter()))
        .sum::<isize>() as usize
}

fn solve_part2(input: &[Vec<isize>]) -> usize {
    input
        .iter()
        .map(|d| difference_engine(d.iter().rev()))
        .sum::<isize>() as usize
}

fn difference_engine<'a>(input: impl Iterator<Item = &'a isize>) -> isize {
    let mut diffs = vec![input.copied().collect::<Vec<_>>()];
    while !diffs.last().unwrap().iter().all(|n| *n == 0) {
        diffs.push(
            diffs
                .last()
                .unwrap()
                .windows(2)
                .map(|a| a[1] - a[0])
                .collect(),
        );
    }
    diffs.iter().fold(0, |a, v| a + v.last().unwrap())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_test() {
        assert_eq!(
            inner(
                "0 3 6 9 12 15\n\
                 1 3 6 10 15 21\n\
                 10 13 16 21 30 45
"
            ),
            (114, 2)
        );
    }

    #[test]
    fn test_my_input() {
        assert_eq!(run(), (1853145119, 923));
    }
}
