const INPUT: &str = include_str!("../input/day05.txt");

#[derive(Clone, Debug)]
struct Map {
    dest: usize,
    src: usize,
    len: usize,
}

pub fn run() -> (usize, usize) {
    inner(INPUT)
}

fn inner(input: &str) -> (usize, usize) {
    let (seeds, maps) = parse(input);
    let part1 = solve_part1(&seeds, &maps);
    let part2 = solve_part2(&seeds, &maps);
    (part1, part2)
}

fn parse(input: &str) -> (Vec<usize>, Vec<Vec<Map>>) {
    let (seeds, maps) = input.split_once("\n\n").unwrap();
    let seeds = seeds
        .split_ascii_whitespace()
        .filter_map(|n| n.parse().ok())
        .collect();
    let maps = maps
        .split("\n\n")
        .map(|m| {
            m.lines()
                .skip(1)
                .map(|l| {
                    let mut ns = l.split_ascii_whitespace();
                    Map {
                        dest: ns.next().unwrap().parse().unwrap(),
                        src: ns.next().unwrap().parse().unwrap(),
                        len: ns.next().unwrap().parse().unwrap(),
                    }
                })
                .collect()
        })
        .collect();
    (seeds, maps)
}

fn solve_part1(seeds: &[usize], maps: &[Vec<Map>]) -> usize {
    let mut seeds = seeds.to_owned();
    for map in maps {
        for i in seeds.iter_mut() {
            *i = map_p1(map, *i);
        }
    }
    *seeds.iter().min().unwrap()
}

fn solve_part2(seeds: &[usize], maps: &[Vec<Map>]) -> usize {
    let mut seed_ranges = seeds
        .chunks(2)
        .map(|c| (c[0]..c[0] + c[1]))
        .collect::<Vec<_>>();
    let mut maps = maps.to_owned();

    for map in maps.iter_mut() {
        map.sort_unstable_by_key(|m| m.src);
    }

    for map in maps {
        let mut new = vec![];
        for seed_range in seed_ranges.iter_mut() {
            let mut done = false;
            for map_range in &map {
                if (map_range.src..map_range.src + map_range.len).contains(&seed_range.start)
                    && (map_range.src..map_range.src + map_range.len).contains(&seed_range.end)
                {
                    done = true;
                    new.push(
                        seed_range.start - map_range.src + map_range.dest
                            ..seed_range.end - map_range.src + map_range.dest,
                    );
                } else if (map_range.src..map_range.src + map_range.len).contains(&seed_range.start)
                {
                    new.push(
                        seed_range.start - map_range.src + map_range.dest
                            ..map_range.dest + map_range.len,
                    );
                    seed_range.start = map_range.src + map_range.len;
                } else if seed_range.contains(&map_range.src)
                    && seed_range.contains(&(map_range.src + map_range.len))
                {
                    new.push(seed_range.start..map_range.src);
                    new.push(map_range.dest..map_range.dest + map_range.len);
                    seed_range.start = map_range.src + map_range.len;
                } else if seed_range.contains(&map_range.src) {
                    done = true;
                    new.push(seed_range.start..map_range.src);
                    new.push(map_range.dest..map_range.dest + seed_range.end - map_range.src);
                }
            }
            if !done {
                new.push(seed_range.to_owned());
            }
        }
        seed_ranges = new;
    }

    seed_ranges.iter().min_by_key(|r| r.start).unwrap().start
}

fn map_p1(map: &[Map], seed: usize) -> usize {
    for range in map {
        if (range.src..range.src + range.len).contains(&seed) {
            return seed - range.src + range.dest;
        }
    }
    seed
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_test() {
        assert_eq!(
            inner(
                "seeds: 79 14 55 13\n\
                 \n\
                 seed-to-soil map:\n\
                 50 98 2\n\
                 52 50 48\n\
                 \n\
                 soil-to-fertilizer map:\n\
                 0 15 37\n\
                 37 52 2\n\
                 39 0 15\n\
                 \n\
                 fertilizer-to-water map:\n\
                 49 53 8\n\
                 0 11 42\n\
                 42 0 7\n\
                 57 7 4\n\
                 \n\
                 water-to-light map:\n\
                 88 18 7\n\
                 18 25 70\n\
                 \n\
                 light-to-temperature map:\n\
                 45 77 23\n\
                 81 45 19\n\
                 68 64 13\n\
                 \n\
                 temperature-to-humidity map:\n\
                 0 69 1\n\
                 1 0 69\n\
                 \n\
                 humidity-to-location map:\n\
                 60 56 37\n\
                 56 93 4"
            ),
            (35, 46)
        );
    }

    #[test]
    fn test_my_input() {
        assert_eq!(run(), (424490994, 15290096));
    }
}
