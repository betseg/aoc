// idfk i spent hours debugging

const INPUT: &str = include_str!("../input/day01.txt");

pub fn run() -> (usize, usize) {
    inner(INPUT)
}

fn inner(input: &str) -> (usize, usize) {
    let part1 = solve_part1(input);
    let part2 = solve_part2(input);
    (part1 as usize, part2)
}

fn solve_part1(input: &str) -> u32 {
    input
        .lines()
        .map(|l| {
            let first = l
                .chars()
                .find(|c| c.is_ascii_digit())
                .unwrap_or('0')
                .to_digit(10)
                .unwrap();
            let last = l
                .chars()
                .rfind(|c| c.is_ascii_digit())
                .unwrap_or('0')
                .to_digit(10)
                .unwrap();
            first * 10 + last
        })
        .sum()
}

fn solve_part2(input: &str) -> usize {
    input.lines().map(find).sum()
}

fn find(input: &str) -> usize {
    let digits = [
        "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
    ];
    let spelled_first = digits
        .iter()
        .enumerate()
        .map(|d| (d, input.find(d.1)))
        .filter(|x| x.1.is_some())
        .min_by_key(|x| x.1)
        .map(|((a, _), c)| (c.unwrap(), a + 1));
    let spelled_last = digits
        .iter()
        .enumerate()
        .map(|d| (d, input.rfind(d.1)))
        .filter(|x| x.1.is_some())
        .max_by_key(|x| x.1)
        .map(|((a, _), c)| (c.unwrap(), a + 1));
    let digits_first = input
        .char_indices()
        .find(|c| c.1.is_ascii_digit())
        .map(|(a, b)| (a, b.to_digit(10).unwrap() as usize));
    let digits_last = input
        .char_indices()
        .rfind(|c| c.1.is_ascii_digit())
        .map(|(a, b)| (a, b.to_digit(10).unwrap() as usize));

    let first = spelled_first
        .unwrap_or((usize::MAX, 0))
        .min(digits_first.unwrap_or((usize::MAX, 0)))
        .1;
    let last = spelled_last
        .unwrap_or((0, 0))
        .max(digits_last.unwrap_or((0, 0)))
        .1;

    first * 10 + last
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_test() {
        assert_eq!(
            inner(
                "1abc2\n\
                 pqr3stu8vwx\n\
                 a1b2c3d4e5f\n\
                 treb7uchet"
            )
            .0,
            142
        );
        assert_eq!(
            inner(
                "two1nine\n\
                 eightwothree\n\
                 abcone2threexyz\n\
                 xtwone3four\n\
                 4nineeightseven2\n\
                 zoneight234\n\
                 7pqrstsixteen"
            )
            .1,
            281
        );
    }

    #[test]
    fn test_my_input() {
        assert_eq!(run(), (57346, 57345));
    }
}
