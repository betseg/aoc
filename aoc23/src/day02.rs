const INPUT: &str = include_str!("../input/day02.txt");

#[derive(Clone, Copy, Debug)]
struct Game {
    id: usize,
    rgb: (usize, usize, usize),
}

pub fn run() -> (usize, usize) {
    inner(INPUT)
}

fn inner(input: &str) -> (usize, usize) {
    let parsed = parse(input);
    let part1 = solve_part1(&parsed);
    let part2 = solve_part2(&parsed);
    (part1, part2)
}

fn parse(input: &str) -> Vec<Game> {
    input
        .lines()
        .map(|l| {
            let (id, balls) = l.split_once(':').unwrap();
            let id = id[5..].parse().unwrap();
            let rgb = balls.split(&[';', ',']).fold((0, 0, 0), |a, b| {
                let (cnt, clr) = b[1..].split_once(' ').unwrap();
                let cnt = cnt.parse().unwrap();
                match clr {
                    "red" => (a.0.max(cnt), a.1, a.2),
                    "green" => (a.0, a.1.max(cnt), a.2),
                    "blue" => (a.0, a.1, a.2.max(cnt)),
                    _ => unreachable!(),
                }
            });
            Game { id, rgb }
        })
        .collect()
}

fn solve_part1(input: &[Game]) -> usize {
    input
        .iter()
        .filter(|g| g.rgb.0 <= 12 && g.rgb.1 <= 13 && g.rgb.2 <= 14)
        .map(|g| g.id)
        .sum()
}

fn solve_part2(input: &[Game]) -> usize {
    input.iter().map(|g| g.rgb.0 * g.rgb.1 * g.rgb.2).sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_test() {
        assert_eq!(
            inner(
                "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green\n\
                 Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue\n\
                 Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red\n\
                 Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red\n\
                 Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green"
            ),
            (8, 2286)
        );
    }

    #[test]
    fn test_my_input() {
        assert_eq!(run(), (2278, 67953));
    }
}
