use std::collections::HashSet;

const INPUT: &str = include_str!("../input/day04.txt");

#[derive(Clone, Debug)]
struct Card {
    winners: HashSet<u8>,
    nums: HashSet<u8>,
}

pub fn run() -> (usize, usize) {
    inner(INPUT)
}

fn inner(input: &str) -> (usize, usize) {
    let parsed = parse(input);
    let part1 = solve_part1(&parsed);
    let part2 = solve_part2(&parsed);
    (part1, part2)
}

fn parse(input: &str) -> Vec<Card> {
    input
        .lines()
        .map(|l| {
            let (_, rest) = l.split_once(':').unwrap();
            let (winners, nums) = rest.split_once('|').unwrap();
            let winners = str_num_list_to_vec(winners);
            let nums = str_num_list_to_vec(nums);
            Card { winners, nums }
        })
        .collect()
}

fn solve_part1(input: &[Card]) -> usize {
    input
        .iter()
        .map(|c| 2usize.pow(c.winners.intersection(&c.nums).count() as u32) / 2)
        .sum()
}

fn solve_part2(input: &[Card]) -> usize {
    let mut mults = vec![1; input.len()];
    for (i, card) in input.iter().enumerate() {
        let matches = card.winners.intersection(&card.nums).count();
        for j in 1..=matches {
            mults[i + j] += mults[i];
        }
    }
    mults.iter().sum()
}

fn str_num_list_to_vec(input: &str) -> HashSet<u8> {
    input
        .split_ascii_whitespace()
        .map(|n| n.parse().unwrap())
        .collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_test() {
        assert_eq!(
            inner(
                "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53\n\
                 Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19\n\
                 Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1\n\
                 Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83\n\
                 Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36\n\
                 Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11"
            ),
            (13, 30)
        );
    }

    #[test]
    fn test_my_input() {
        assert_eq!(run(), (21919, 9881048));
    }
}
