use std::collections::HashMap;

use nom::character::complete::digit0;
use nom::error::Error;

const INPUT: &str = include_str!("../input/day03.txt");

#[derive(Clone, Copy, Debug)]
struct Number {
    n: usize,
    adjacent: (Option<u8>, (usize, usize)),
}

pub fn run() -> (usize, usize) {
    inner(INPUT)
}

fn inner(input: &str) -> (usize, usize) {
    let parsed = parse(input);
    let part1 = solve_part1(&parsed);
    let part2 = solve_part2(&parsed);
    (part1, part2)
}

fn parse(input: &str) -> Vec<Number> {
    let mut nums = vec![];
    for (l, line) in input.lines().enumerate() {
        let mut i = 0;
        while i < line.len() {
            if let Ok(n) = digit0::<_, Error<_>>(&line[i..])
                .unwrap()
                .1
                .parse::<usize>()
            {
                let len = n.ilog10() as usize;
                let bounds = (l, (i, i + len));
                let mut adjacent = (None, (i, i + len));
                for di in (bounds.0).saturating_sub(1)..=(bounds.0).saturating_add(1) {
                    for dj in (bounds.1 .0).saturating_sub(1)..=(bounds.1 .1).saturating_add(1) {
                        if let Some(line) = input.lines().nth(di) {
                            if let Some(c) = line.as_bytes().get(dj) {
                                if !c.is_ascii_digit() && *c != b'.' {
                                    adjacent = (Some(*c), (di, dj));
                                }
                            }
                        }
                    }
                }

                nums.push(Number { n, adjacent });
                i += len;
            }
            i += 1;
        }
    }
    nums
}

fn solve_part1(input: &[Number]) -> usize {
    input
        .iter()
        .filter(|n| n.adjacent.0.is_some())
        .map(|n| n.n)
        .sum()
}

fn solve_part2(input: &[Number]) -> usize {
    let mut map = HashMap::<(Option<u8>, (usize, usize)), Vec<&Number>>::new();
    for possible in input.iter().filter(|n| n.adjacent.0.is_some()) {
        map.entry(possible.adjacent)
            .and_modify(|v| v.push(possible))
            .or_insert(vec![possible]);
    }
    map.values()
        .filter(|v| v.len() == 2)
        .map(|v| v.iter().map(|n| n.n).product::<usize>())
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_test() {
        assert_eq!(
            inner(
                "467..114..\n\
                 ...*......\n\
                 ..35..633.\n\
                 ......#...\n\
                 617*......\n\
                 .....+.58.\n\
                 ..592.....\n\
                 ......755.\n\
                 ...$.*....\n\
                 .664.598.."
            ),
            (4361, 467835)
        );
    }

    #[test]
    fn test_my_input() {
        assert_eq!(run(), (556367, 89471771));
    }
}
