const INPUT: &str = "Time:        56     71     79     99\n\
                     Distance:   334   1135   1350   2430";

pub fn run() -> (usize, usize) {
    inner(INPUT)
}

fn inner(input: &str) -> (usize, usize) {
    let part1 = solve_part1(input);
    let part2 = solve_part2(input);
    (part1, part2)
}

fn solve_part1(input: &str) -> usize {
    let (time, dist) = input.split_once('\n').unwrap();
    time.split_ascii_whitespace()
        .skip(1)
        .zip(dist.split_ascii_whitespace().skip(1))
        .map(|(t, d)| calc(t.parse().unwrap(), d.parse().unwrap()))
        .product::<f64>() as usize
}

fn solve_part2(input: &str) -> usize {
    let mut nums = input.lines().map(|l| {
        l.split_ascii_whitespace()
            .skip(1)
            .collect::<String>()
            .parse::<usize>()
            .unwrap()
    });

    calc(nums.next().unwrap() as f64, nums.next().unwrap() as f64) as usize
}

fn calc(time: f64, dist: f64) -> f64 {
    let discr = f64::sqrt(time.powi(2) - 4. * dist);
    ((time + discr) / 2.).ceil() - ((time - discr) / 2.).floor() - 1.
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_test() {
        assert_eq!(
            inner(
                "Time:      7  15   30\n\
                 Distance:  9  40  200"
            ),
            (288, 71503)
        );
    }

    #[test]
    fn test_my_input() {
        assert_eq!(run(), (211904, 43364472));
    }
}
