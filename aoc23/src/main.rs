use std::hint::black_box;
use std::time::Instant;

use clap::Parser;

const PREPARE: u32 = 10;
const RUNS: u32 = 50;

#[derive(Parser)]
struct Opts {
    #[arg(short, long)]
    day: Option<usize>,
    #[arg(short, long)]
    benchmark: bool,
}

mod day01;
mod day02;
mod day03;
mod day04;
mod day05;
mod day06;
mod day07;
mod day08;
mod day09;

const SOLUTIONS: [&dyn Fn() -> (usize, usize); 9] = [
    &day01::run,
    &day02::run,
    &day03::run,
    &day04::run,
    &day05::run,
    &day06::run,
    &day07::run,
    &day08::run,
    &day09::run,
];

fn main() {
    let opts = Opts::parse();

    match (opts.benchmark, opts.day) {
        (true, Some(day)) => run(day, SOLUTIONS[day - 1]),
        (false, Some(day)) => println!("Day {:02}: {:?}", day, SOLUTIONS[day - 1]()),
        (true, None) => {
            for (i, f) in SOLUTIONS.iter().enumerate() {
                run(i + 1, f);
            }
        }
        (false, None) => {
            for (i, f) in SOLUTIONS.iter().enumerate() {
                println!("Day {:02}: {:?}", i + 1, f());
            }
        }
    }
}

fn run(day: usize, f: &dyn Fn() -> (usize, usize)) {
    for _ in 0..PREPARE {
        black_box(f());
    }
    let now = Instant::now();
    for _ in 0..RUNS {
        black_box(f());
    }
    let elapsed = now.elapsed();

    println!(
        "day {:02}: {:>9?}µs",
        day,
        (elapsed / RUNS).as_nanos() as f64 / 1000f64
    );
}
