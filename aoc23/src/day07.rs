const INPUT: &str = include_str!("../input/day07.txt");

#[derive(Clone, Debug)]
struct Hand {
    cards: Vec<u8>,
    raw: Vec<u8>,
    bid: usize,
}

pub fn run() -> (usize, usize) {
    inner(INPUT)
}

fn inner(input: &str) -> (usize, usize) {
    let parsed = parse(input);
    let part1 = solve_part1(&parsed);
    let part2 = solve_part2(&parsed);
    (part1, part2)
}

fn parse(input: &str) -> Vec<Hand> {
    input
        .lines()
        .map(|l| {
            let (cards, bid) = l.split_once(' ').unwrap();
            let raw = cards.chars().map(parse_card).collect();
            let cards = cards.chars().map(parse_card).fold(vec![0; 14], |a, c| {
                let mut a = a;
                a[c as usize] += 1;
                a
            });
            Hand {
                cards,
                raw,
                bid: bid.parse().unwrap(),
            }
        })
        .collect()
}

fn parse_card(card: char) -> u8 {
    (match card {
        '1'..='9' => card.to_digit(10).unwrap(),
        'T' => 10,
        'J' => 11,
        'Q' => 12,
        'K' => 13,
        'A' => 14,
        _ => unreachable!(),
    }) as u8
        - 1
}

fn solve_part1(input: &[Hand]) -> usize {
    let mut hands = input.to_owned();
    hands.sort_unstable_by(cmp_p1);
    hands.iter().enumerate().map(|(i, h)| (i + 1) * h.bid).sum()
}

fn cmp_p1(this: &Hand, other: &Hand) -> std::cmp::Ordering {
    hand_rank_p1(&this.cards)
        .cmp(&hand_rank_p1(&other.cards))
        .then(this.raw.cmp(&other.raw))
}

fn hand_rank_p1(hand: &[u8]) -> u8 {
    if hand.iter().any(|&n| n == 5) {
        6
    } else if hand.iter().any(|&n| n == 4) {
        5
    } else if hand.iter().any(|&n| n == 3) && hand.iter().any(|&n| n == 2) {
        4
    } else if hand.iter().any(|&n| n == 3) {
        3
    } else if hand.iter().filter(|&&n| n == 2).count() == 2 {
        2
    } else if hand.iter().any(|&n| n == 2) {
        1
    } else {
        0
    }
}

fn solve_part2(input: &[Hand]) -> usize {
    let mut hands = input.to_owned();
    for hand in hands.iter_mut() {
        hand.cards.swap(0, 10);
        for card in hand.raw.iter_mut() {
            if *card == 10 {
                *card = 0;
            }
        }
    }
    hands.sort_unstable_by(cmp_p2);
    hands.iter().enumerate().map(|(i, h)| (i + 1) * h.bid).sum()
}

fn cmp_p2(this: &Hand, other: &Hand) -> std::cmp::Ordering {
    hand_rank_p2(&this.cards)
        .cmp(&hand_rank_p2(&other.cards))
        .then(this.raw.cmp(&other.raw))
}

fn hand_rank_p2(hand: &[u8]) -> u8 {
    let jokers = hand[0];
    let fives = hand[1..].iter().any(|&n| n == 5);
    let fours = hand[1..].iter().any(|&n| n == 4);
    let threes = hand[1..].iter().any(|&n| n == 3);
    let twos = hand[1..].iter().filter(|&&n| n == 2).count();

    if fives
        || fours && jokers == 1
        || threes && jokers == 2
        || twos == 1 && jokers == 3
        || jokers >= 4
    {
        6
    } else if fours || threes && jokers == 1 || twos == 1 && jokers == 2 || jokers == 3 {
        5
    } else if threes && twos == 1 || twos == 2 && jokers == 1 || threes && jokers == 1 {
        4
    } else if threes || twos == 1 && jokers == 1 || jokers == 2 {
        3
    } else if twos == 2 || twos == 1 && jokers == 1 {
        2
    } else if twos == 1 || jokers == 1 {
        1
    } else {
        0
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_test() {
        assert_eq!(
            inner(
                "32T3K 765\n\
                 T55J5 684\n\
                 KK677 28\n\
                 KTJJT 220\n\
                 QQQJA 483"
            ),
            (6440, 5905)
        );
    }

    #[test]
    fn test_my_input() {
        assert_eq!(run(), (251058093, 249781879));
    }
}
