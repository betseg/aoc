use clap::Parser;

#[derive(Parser)]
struct Opts {
    #[arg(short, long)]
    day: Option<usize>,
}

mod day01;
mod day02;

const SOLUTIONS: [&dyn Fn() -> (usize, usize); 2] = [&day01::run, &day02::run];

fn main() {
    if let Some(day) = Opts::parse().day {
        println!("Day {:02}: {:?}", day, SOLUTIONS[day - 1]());
    } else {
        for (i, f) in SOLUTIONS.iter().enumerate() {
            println!("Day {:02}: {:?}", i + 1, f());
        }
    }
}
